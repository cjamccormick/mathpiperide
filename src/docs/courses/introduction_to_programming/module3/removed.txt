This version of the program contains an If()
procedure that does not use a code sequence as a
body:

%mathpiper,flowchart="false",image_scale="2.0",treeview="false"

number := 4;

If(number >? 5) 
{
    Echo(number, "is greater than 5.");
};

Echo("End of program.");

%/mathpiper