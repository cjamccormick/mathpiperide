%mathpiper,def="Sum;SumFunc"

/* Sums */


RulebaseHoldArguments("Sum",[sumvararg,sumfromarg,sumtoarg,sumbodyarg]);




10  # Sum(_sumvar,sumfrom_Number?,sumto_Number?,_sumbody)_(sumfrom>?sumto) <-- 0;

20 # Sum(_sumvar,sumfrom_Number?,sumto_Number?,_sumbody)_(sumto<?sumfrom) <--
     ApplyFast("Sum",[sumvar,sumto,sumfrom,sumbody]);
30 # Sum(_sumvar,sumfrom_Number?,sumto_Number?,_sumbody) <--
LocalSymbols(sumi,sumsum){
   Local(sumi,sumsum);
   sumsum:=0;
   For(sumi:=sumfrom,sumi<=?sumto,sumi++)
       {
        MacroLocal(sumvar);
        MacroAssign(sumvar,sumi);
        sumsum:=sumsum+Eval(sumbody);
       };
   sumsum;
};

UnFence("Sum",4);
HoldArgument("Sum",sumvararg);
HoldArgument("Sum",sumbodyarg);



40 # Sum([]) <-- 0;

50 # Sum(values_List?) <--
{
   Local(i, sum);
   sum:=0;
   ForEach(i, values) { sum := sum + i; };
   sum;
};





//============== SumFunc
LocalSymbols(c,d,expr,from,to,summand,sum,predicate,n,r,var,x) {

// Attempt to Sum series

Function() SumFunc(k,from,to,summand, sum, predicate );
Function() SumFunc(k,from,to,summand, sum);
HoldArgument(SumFunc,predicate);
HoldArgument(SumFunc,sum);
HoldArgument(SumFunc,summand);

// Difference code does not work
SumFunc(_sumvar,sumfrom_Integer?,_sumto,_sumbody,_sum) <--
{
        // Take the given answer and create 2 rules, one for an exact match
        // for sumfrom, and one which will catch sums starting at a different
        // index and subtract off the difference

        `(40 # Sum(@sumvar,@sumfrom,@sumto,@sumbody )        <-- Eval(@sum) );
        `(41 # Sum(@sumvar,p_Integer?,@sumto,@sumbody)_(p >? @sumfrom)
             <--
             {
                  Local(sub);
                  (sub := Eval(ListToFunction([Sum,sumvararg,@sumfrom,p-1,sumbodyarg])));
                  Simplify(Eval(@sum) - sub );
             });
};

SumFunc(_sumvar,sumfrom_Integer?,_sumto,_sumbody,_sum,_condition) <--
{

        `(40 # Sum(@sumvar,@sumfrom,@sumto,@sumbody)_(@condition)    <-- Eval(@sum) );
        `(41 # Sum(@sumvar,p_Integer?,@sumto,@sumbody )_(@condition And? p >? @sumfrom)
             <--
             {
                  Local(sub);
                  `(sub := Eval(ListToFunction([Sum,sumvararg,@sumfrom,p-1,sumbodyarg])));
                  Simplify(Eval(@sum) - sub );
             });
};

// Some type of canonical form is needed so that these match when
// given in a different order, like x^k/k! vs. (1/k!)*x^k
// works !
SumFunc(_k,1,_n,_c + _d,
  Eval(ListToFunction([Sum,sumvararg,1,n,c])) +
  Eval(ListToFunction([Sum,sumvararg,1,n,d]))
);
SumFunc(_k,1,_n,_c*_expr,Eval(c*ListToFunction([Sum,sumvararg,1,n,expr])), FreeOf?(k,c) );
SumFunc(_k,1,_n,_expr/_c,Eval(ListToFunction([Sum,sumvararg,1,n,expr])/c), FreeOf?(k,c) );

// this only works when the index=1
// If the limit of the general term is not zero, then the series diverges
// We need something like IsUndefined(term), because this croaks when limit return Undefined
//SumFunc(_k,1,Infinity,_expr,Infinity,Eval(Abs(ListToFunction([Limit,sumvararg,Infinity,expr])) >? 0));
SumFunc(_k,1,Infinity,1/_k,Infinity);

SumFunc(_k,1,_n,_c,c*n,FreeOf?(k,c) );
SumFunc(_k,1,_n,_k, n*(n+1)/2 );
//SumFunc(_k,1,_n,_k^2, n*(n+1)*(2*n+1)/6 );
//SumFunc(_k,1,_n,_k^3, (n*(n+1))^2 / 4 );
SumFunc(_k,1,_n,_k^_p,(Bernoulli(p+1,n+1) - Bernoulli(p+1))/(p+1), Integer?(p) );
SumFunc(_k,1,_n,2*_k-1, n^2 );
SumFunc(_k,1,_n,HarmonicNumber(_k),(n+1)*HarmonicNumber(n) - n );

// Geometric series! The simplest of them all ;-)
SumFunc(_k,0,_n,(r_FreeOf?(k))^(_k), (1-r^(n+1))/(1-r) );

// Infinite Series
// this allows Zeta a complex argument, which is not supported yet
SumFunc(_k,1,Infinity,1/(_k^_d), Zeta(d), FreeOf?(k,d) );
SumFunc(_k,1,Infinity,_k^(-_d), Zeta(d), FreeOf?(k,d) );

SumFunc(_k,0,Infinity,_x^(2*_k+1)/(2*_k+1)!,Sinh(x) );
SumFunc(_k,0,Infinity,(-1)^k*_x^(2*_k+1)/(2*_k+1)!,Sin(x) );
SumFunc(_k,0,Infinity,_x^(2*_k)/(2*_k)!,Cosh(x) );
SumFunc(_k,0,Infinity,(-1)^k*_x^(2*_k)/(2*_k)!,Cos(x) );
SumFunc(_k,0,Infinity,_x^(2*_k+1)/(2*_k+1),ArcTanh(x) );
SumFunc(_k,0,Infinity,1/(_k)!,Exp(1) );
SumFunc(_k,0,Infinity,_x^_k/(_k)!,Exp(x) );
40 # Sum(_var,_from,Infinity,_expr)_( `(Limit(@var,Infinity)(@expr)) =? Infinity) <-- Infinity;

SumFunc(_k,1,Infinity,1/BinomialCoefficient(2*_k,_k), (2*Pi*Sqrt(3)+9)/27 );
SumFunc(_k,1,Infinity,1/(_k*BinomialCoefficient(2*_k,_k)), (Pi*Sqrt(3))/9 );
SumFunc(_k,1,Infinity,1/(_k^2*BinomialCoefficient(2*_k,_k)), Zeta(2)/3 );
SumFunc(_k,1,Infinity,1/(_k^3*BinomialCoefficient(2*_k,_k)), 17*Zeta(4)/36 );
SumFunc(_k,1,Infinity,(-1)^(_k-1)/_k, Ln(2) );

};

%/mathpiper



%mathpiper_docs,name="Sum",categories="Mathematics Functions;Series"
*CMD Sum --- find sum of a sequence
*STD
*CALL
        Sum(list)
        Sum(var, from, to, body)

*PARMS

{list} -- list of values to sum

{var} -- variable to iterate over

{from} -- integer value to iterate from

{to} -- integer value to iterate up to

{body} -- expression to evaluate for each iteration

*DESC

The first form of the {Sum} command simply
adds all the entries in "list" and returns their sum.

If the second calling sequence is used, it finds the sum of
the sequence generated by an iterative formula. 
The expression "body" is
evaluated while the variable "var" ranges over all integers from
"from" up to "to", and the sum of all the results is
returned. Obviously, "to" should be greater than or equal to
"from".

Warning: {Sum} does not evaluate its arguments {var} and {body} until the actual loop is run.

*E.G.

In> Sum([1,2,3])
Result> 6

In> Sum(1 .. 10);
Result: 55;

In> Sum(i, 1, 3, i^2);
Result: 14;

*SEE Product
%/mathpiper_docs





%mathpiper,name="Sum",subtype="automatic_test"

Verify( Sum(k,1,n,k), n*(n+1)/2 );
Verify( Simplify(Sum(k,1,n,k^3)), Simplify( (n*(n+1))^2 / 4 ) );
Verify( Sum(k,1,Infinity,1/k^2), Zeta(2) );
Verify( Sum(k,1,Infinity,1/k), Infinity );
Verify( Sum(i,1,Infinity,1/i), Infinity );
Verify( Sum(k,1,Infinity,Sqrt(k)), Infinity );
Verify( Sum(k,2,Infinity,x^k/k!), Exp(x)-(x+1) );
Verify( Sum(k,1,n,Sin(a)+Sin(b)+p),(Sin(a)+Sin(b)+p)*n );

%/mathpiper