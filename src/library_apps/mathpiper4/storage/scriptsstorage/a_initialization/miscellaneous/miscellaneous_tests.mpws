%mathpiper,name="miscellaneous",subtype="automatic_test"

Testing("From: calculus.mpt");

Testing("UnaryFunctionInverses");
Verify(Sin(ArcSin(a)),a);
Verify(Cos(ArcCos(a)),a);
//TODO ??? Verify(Tan(ArcTan(a)),a);
//TODO ??? this is not always the correct answer! Verify(ArcTan(Tan(a)),a);
Verify(Tan(Pi/2),Infinity);
Verify(Tan(Pi),0);
Verify( Sin(x)/Cos(x), Tan(x) );
Verify( TrigSimpCombine(Sin(x)^2 + Cos(x)^2), 1 );
Verify( Sinh(x)-Cosh(x), Exp(-x));
Verify( Sinh(x)+Cosh(x), Exp(x) );
Verify( Sinh(x)/Cosh(x), Tanh(x) );
Verify( Sinh(Infinity), Infinity);
Verify( Sinh(x)*Csch(x), 1);
Verify( 1/Coth(x), Tanh(x) );
Verify(2+I*3,Complex(2,3));
Verify(Magnitude(I+1),Sqrt(2));
Verify(Re(2+I*3),2);
Verify(Im(2+I*3),3);
// Shouldn't these be in linalg.yts?
Verify(ZeroVector(3),[0,0,0]);
Verify(BaseVector(2,3),[0,1,0]);
Verify(Identity(3),[[1,0,0],[0,1,0],[0,0,1]]);

Testing("Checking comment syntax supported");
{
  Local(a);
  /* something here */
  a:= 3;
  // test 1

  // /* test2 */

  /* // test3 */

  //Echo([a, Nl()]);

  // Check parsing
  a==-b; // This would generate a parse error in Yacas versions 1.0.54 and earlier
};

Testing("Yacas tutorials and journal entries.");
Verify(1+1,2);
Verify("This text","This text");
Verify(2+3,5);
Verify(3*4,12);
Verify(-(3*4),-12);
Verify(2+3*4,14);
Verify(6/3,2);
Verify(1/3,1/3);
Verify(Number?(NM(1/3)),True);
Verify(Sin(Pi),0);
Verify(Minimum(5,1,3,-5,10),-5);
Verify(Sqrt(2),Sqrt(2));
Verify([1,2,3],[1,2,3]);
Verify([a,b,c][2],b);
Verify("abc"[2],"b");
Verify(x^(1/2),Sqrt(x));


// Yacas used to not simplify the following, due to Pi being
// considered constant. The expression was thus not expanded
// as a univariate polynomial in Pi
TestMathPiper(2*Pi/3,(Pi-Pi/3));
TestMathPiper(( a*(Sqrt(Pi))^2/2), (a*Pi)/2);
TestMathPiper(( 3*(Sqrt(Pi))^2/2), (3*Pi)/2);
TestMathPiper(( a*(Sqrt(b ))^2/2), (a*b)/2);

TestMathPiper(Sin(Pi-22),-Sin(22-Pi));
TestMathPiper(Cos(Pi-22), Cos(22-Pi));


Verify( Infinity + I, Complex(Infinity,1) );
Verify( Infinity - I, Complex(Infinity,-1) );
Verify( I - Infinity,Complex(-Infinity,1) );
Verify( I + Infinity, Complex(Infinity,1) );
Verify( I*Infinity,Complex(0,Infinity)); //Changed Ayal: I didn't like the old definition
Verify( -I*Infinity,Complex(0,-Infinity)); //Changed Ayal: I didn't like the old definition
Verify( Infinity*I,Complex(0,Infinity)); //Changed Ayal: I didn't like the old definition
Verify( Infinity^I,Undefined);//Changed Ayal: I didn't like the old definition (it is undefined, right?)
Verify( (2*I)^Infinity, Infinity );
Verify( Infinity/I,Infinity );
Verify( Sign(Infinity), 1 );
Verify( Sign(-Infinity), -1 );
// bugs with complex numbers
Verify((1+I)^0, 1);
Verify((-I)^0, 1);
Verify((2*I)^(-10), -1/1024);
Verify((-I)^(-10), -1);
Verify((1-I)^(-10), Complex(0,1/32));
Verify((1-I)^(+10), Complex(0,-32));
Verify((1+2*I)^(-10), Complex(237/9765625,3116/9765625));
Verify((1+2*I)^(+10), Complex(237,-3116));

// - and ! operators didn't get handled correctly in the
// parser/pretty printer (did you fix this, Serge?)
Verify(PipeToString()Write((-x)!),"(-x)!");


// some interesting interaction between the rules...
Verify(x*x*x,x^3,);
Verify(x+x+x,3*x);
Verify(x+x-x+x,2*x);


// numbers are too small because of wrong precision handling
Local(OriginalPrecision);
OriginalPrecision := BuiltinPrecisionGet();
BuiltinPrecisionSet(30);
Verify(0.00000000000000000005421010862 =? 0, False);        // 2^(-64)
Verify(0.00000000000000000005421010862 / 1 =? 0, False);
Verify(0.00000000000000000005421010862 / 2 =? 0, False);
Verify(0.00000000000000000001 =? 0, False);
Verify(0.00000000000000000001 / 2 =? 0, False);
Verify(0.00000000000000000000000000001 =? 0, False);
Verify(0.000000000000000000000000000001 =? 0, False);
Verify((0.0000000000000000000000000000000000000001 =? 0), False);
// I added another one, the code will currently say that 0.0000...00001=0 is True
// for a sufficient amount of zeroes, regardless of precision. Either that is good
// or that is bad, but the above tests didn't go far enough. This one makes it
// more explicit, unless we move over to a 128-bits system ;-)
Verify((0.0000000000000000000000000000000000000000000000001 =? 0), False);
BuiltinPrecisionSet(OriginalPrecision);


// With the changes in numerics, RoundTo seems to have been broken. This line demonstrates the problem.
// The last digit is suddenly rounded down (it used to be 4, correctly, and then gets rounded down to 3).
// KnownFailure(RoundTo(RoundTo(NM(Cot(2),9),9),NM(Cot(2),9),9)=0);


// Bug that was introduced when going to the new numeric setup where
// numbers were not converted to strings any more. In the situation
// -n*10^-m where n and m positive integers, the number got truncated
// prematurely, resulting in a wrong rounding.
{
  Local(n,m,nkeep,lcl);
  n:=7300 + 12*I;
  m:=2700 + 100*I;
  nkeep:=n;
  n:=m;
  m:=nkeep - m*Round(nkeep/m);
  lcl:=Re(NM(n/m))+0.5;
  Verify(FloorN(lcl),-3);
};


/* Bug reported by Adrian Vontobel. */
{
  Local(A1,A2);
  A1:=Pi*20^2; // 400*Pi
  A2:=Pi*18^2; // 324*Pi
  Verify(Minimum(A1,A2), 324*Pi);
  Verify(Maximum(A1,A2), 400*Pi);
};

//======================== The following tests are from sturm.mpt.

BuiltinPrecisionSet(6);

Testing("From: sturm.mpt");
/*
 TESTS:
 - random-test code for roots, be generating random roots and
   multiplicities.
 - find an example where bisection is needed, or better, a group
   of examples where bisection is needed, for tests
*/


VerifyZero(x) := (Abs(x)<?10^ -BuiltinPrecisionGet());

TryRandomPoly(deg,coefmin,coefmax):=
{
  Local(Coefs,monicCoefs,p,mp,roots,px);
  Coefs := BuildList(FloorN(coefmin+Random()*(coefmax-coefmin)),i,1,deg+1,1);
  p  := Add(Coefs*x^(0 .. deg));
  mp := Monic(p);
  monicCoefs := Coef(mp,x,0 .. deg);
  //Verify(Maximum(Abs(monicCoefs)) <=? MaximumBound(mp), True);
  //Verify(Minimum(Abs(monicCoefs)) >?  MinimumBound(mp), True);
  // HSO:  I don't think these two tests are useful, 
  //       and the second seems incorrect.
  roots:=FindRealRoots(mp);
  px := (mp Where x==roots);
  Verify(Dot(px, px) <? 0.01, True);
};
TryRandomPoly(5,5,1000);


TryRandomRoots(deg,coefmin,coefmax):=
{
  Local(coefs,p,roots,px,mult,sqf);
  coefs:=RemoveDuplicates(BuildList(FloorN(coefmin+Random()*(coefmax-coefmin)),i,1,deg+1,1));
  deg:=Length(coefs)-1;
  mult:=1+Abs(BuildList(FloorN(coefmin+Random()*(coefmax-coefmin)),i,1,deg+1,1));
  p:=Product((x-coefs)^mult);
  p:=Rationalize(p);
  sqf := SquareFree(p);
  roots:=FindRealRoots(sqf);
  //Verify(Length(roots) =? Length(coefs));
  //Verify(Length(RemoveDuplicates(roots)) =? Length(coefs));
  px := (p Where x==roots);
  Verify(Dot(px, px) <? 0.01, True);
  
};
TryRandomRoots(3,-10,10);



{
  Local(p);
  p := FindRealRoots((x+2)^9*(x-4)^5*(x-1)^4)-[-2.,1.,4.];
  Verify(VerifyZero(Dot(p,p)),True);
};

// Bounds on coefficients
Verify(MinimumBound(4),-Infinity);
Verify(MaximumBound(4),Infinity);

// RealRootsCount
Verify(RealRootsCount(x^2-1),2);
Verify(RealRootsCount(x^2+1),0);
Verify(FindRealRoots((x^2+20)*(x^2+10)),[]);
Verify(RealRootsCount((x^2+20)*(x^2+10)),0);
Verify(Difference(FindRealRoots(Expand((x*(x-10)^3*(x+2)^2))),[0,-2.,10.]),[]);

// Simple test on Squarefree
TestMathPiper(Monic(SquareFree((x-1)^2*(x-3)^3)),Monic((x-1)*(x-3)));

// Check the rare case where the bounds finder lands on
// exactly a root
{
  Local(p);
  p:=FindRealRoots((x+4)*(x-6),1,7)-[-4.,6.];
  Verify(VerifyZero(Dot(p, p)),True);
};

{
  Local(p);
  p:=Expand((x-3.1)*(x+6.23));
  p:=FindRealRoots(p)-[-6.23,3.1];
  Verify(VerifyZero(Dot(p, p)),True);
};

Verify(BuiltinPrecisionGet(),6);
{
  Local(res);
  res:=FindRealRoots(Expand((x-3.1)*(x+6.23)))-[-6.23,3.1];
  Verify(VerifyZero(Dot(res, res)) , True);
};

//====================== end of strum.mpt tests.

%/mathpiper

