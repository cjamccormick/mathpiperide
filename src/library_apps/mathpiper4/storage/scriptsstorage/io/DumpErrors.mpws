%mathpiper,def="DumpErrors"

/// print all errors and clear the tableau
DumpErrors() <--
{
        Local(errorobject, errorword);
        CheckErrorTableau();
        ForEach(errorobject, GetErrorTableau())
        {        // errorobject might be e.g. ["critical", ["bad bad", -1000]]
                Decide(
                        List?(errorobject),
                        {
                                Decide( // special case: error class "warning"
                                        Length(errorobject) >? 0 And? errorobject[1] =? "warning",
                                        {
                                                errorword := "Warning";
                                                errorobject[1] := "";        // dont print the word "warning" again
                                        },
                                        errorword := "Error: "        // important hack: insert ": " here but not after "Warning"
                                );

                                Decide(        // special case: ["errorclass", True]
                                        Length(errorobject)=?2 And? errorobject[2]=?True,
                                        Echo(errorword, errorobject[1]),
                                        {
                                                Echo(errorword, errorobject[1], ": ",
                                                        PrintList(Rest(errorobject)));
                                        }
                                );
                        },
                        // errorobject is not a list: just print it
                        Echo("Error: ", errorobject)
                );
        };
        ClearErrors();
};

%/mathpiper




%mathpiper_docs,name="DumpErrors",categories="Programming Functions;Error Reporting",access="private"
*CMD DumpErrors --- simple error handlers
*STD
*CALL
        DumpErrors()

*DESC

{DumpErrors} is a simple error handler for the global error reporting mechanism. 
It prints all errors posted using {Assert} and clears the error tableau.

*SEE Assert, Error?, ClearErrors

%/mathpiper_docs