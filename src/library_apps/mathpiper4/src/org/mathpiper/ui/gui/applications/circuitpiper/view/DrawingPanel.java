package org.mathpiper.ui.gui.applications.circuitpiper.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import javax.swing.AbstractAction;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import org.mathpiper.ui.gui.applications.circuitpiper.controller.MouseHandler;
import org.mathpiper.ui.gui.applications.circuitpiper.model.Terminal;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Ammeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.CapacitanceMeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Capacitor;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.CurrentIntegrator;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.InductanceMeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Meter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Ohmmeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.VoltageIntegrator;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Voltmeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.linearalgebra.SparseMatrix;

/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */
public final class DrawingPanel extends JPanel implements ViewPanel, Printable {

    CircuitPanel circuitPanel;
    MouseHandler eventHandler;

    public boolean updated;

    public double viewScale = 1.5;
    public int fontSize = 11;
    public boolean isShowBoardOutline = false;
    public double boardXPixels = 0;
    public double boardYPixels = 0;
    public boolean drawPlus = true;
    public boolean isDrawWireLabels = false;
    public boolean isDrawTerminalLabels = false;
    public boolean isDrawGrid = true;

    public DrawingPanel(final CircuitPanel parentCircuitEnginePanel) {
        super();
        circuitPanel = parentCircuitEnginePanel;
        eventHandler
                = new MouseHandler(parentCircuitEnginePanel);
        this.addMouseMotionListener(eventHandler);
        this.addMouseListener(eventHandler);
        this.setBackground(Color.WHITE);
        
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE,0), "Cancel");
        this.getActionMap().put("Cancel", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                circuitPanel.setIsDrawing(false);
                //circuitPanel.setHintStarting();
                circuitPanel.repaint();
            }
        });
        
    }


       

    public void updateGraphsAndMeters(SparseMatrix mainMatrix, double time) {
        for (Component ec : circuitPanel.circuit.electricComponents) {
            if (ec.getClass() == Voltmeter.class) {
                Voltmeter v = (Voltmeter) ec;
                v.voltageString = mainMatrix.getVoltageDeltaVString(ec, true);
                v.fullValue = mainMatrix.getVoltageDeltaVString(ec, false);
                v.calculatedValue = mainMatrix.getVoltageDeltaV(ec);
                if (ec.graphFrame != null && ec.graphFrame.plotBox != null) {
                    ec.graphFrame.plotBox.addPoint(0, time, mainMatrix.getVoltageDeltaV(ec), true);
                    ec.graphFrame.plotBox.fillPlot();
                }
            }
            if (ec.getClass() == Ammeter.class) {
                Ammeter a = (Ammeter) ec;
                a.ampString = mainMatrix.getCurrentString(ec, true);
                a.fullValue = mainMatrix.getCurrentString(ec, false);
                a.calculatedValue = mainMatrix.getCurrent(ec);

                if (ec.graphFrame != null && ec.graphFrame.plotBox != null) {
                    ec.graphFrame.plotBox.addPoint(0, time, mainMatrix.getCurrent(ec), true);
                    ec.graphFrame.plotBox.fillPlot();
                }
            }
            if (ec.getClass() == CurrentIntegrator.class) {
                CurrentIntegrator a = (CurrentIntegrator) ec;
                a.chargeString = Component.formatValue(a.secondaryValue) + "C";
                a.fullValue = a.secondaryValue + "C";
                a.calculatedValue = a.secondaryValue;
                if (ec.graphFrame != null && ec.graphFrame.plotBox != null) {
                    ec.graphFrame.plotBox.addPoint(0, time, ec.secondaryValue, true);
                    ec.graphFrame.plotBox.fillPlot();
                }
            }
            if (ec.getClass() == VoltageIntegrator.class) {
                VoltageIntegrator a = (VoltageIntegrator) ec;
                a.magneticFluxString = Component.formatValue(a.secondaryValue) + "Wb";
                a.fullValue = a.secondaryValue + "Wb";
                a.calculatedValue = a.secondaryValue;
                if (ec.graphFrame != null && ec.graphFrame.plotBox != null) {
                    ec.graphFrame.plotBox.addPoint(0, time, ec.secondaryValue, true);
                    ec.graphFrame.plotBox.fillPlot();
                }
            }
            if (ec.getClass() == Ohmmeter.class) {
                Ohmmeter a = (Ohmmeter) ec;
                a.ohmString = mainMatrix.getResistanceDeltaVString(ec, true);
                a.fullValue = mainMatrix.getResistanceDeltaVString(ec, false);
                a.calculatedValue = mainMatrix.getResistanceDeltaV(ec);
                if (ec.graphFrame != null && ec.graphFrame.plotBox != null) {
                    ec.graphFrame.plotBox.addPoint(0, time, mainMatrix.getResistanceDeltaV(ec), true);
                    ec.graphFrame.plotBox.fillPlot();
                }
            }
            if (ec.getClass() == CapacitanceMeter.class) {
                CapacitanceMeter a = (CapacitanceMeter) ec;
                a.capacitanceString = mainMatrix.getCapacitanceString(ec, true);
                a.fullValue = mainMatrix.getCapacitanceString(ec, false);
                a.calculatedValue = mainMatrix.getCapacitance(ec);
                if (ec.graphFrame != null && ec.graphFrame.plotBox != null) {
                    ec.graphFrame.plotBox.addPoint(0, time, mainMatrix.getCapacitance(ec), true);
                    ec.graphFrame.plotBox.fillPlot();
                }
            }
            if (ec.getClass() == InductanceMeter.class) {
                InductanceMeter a = (InductanceMeter) ec;
                a.inductanceString = mainMatrix.getInductanceDeltaVString(ec, true);
                a.fullValue = mainMatrix.getInductanceDeltaVString(ec, false);
                a.calculatedValue = mainMatrix.getInductanceDeltaV(ec);
                if (ec.graphFrame != null && ec.graphFrame.plotBox != null) {
                    ec.graphFrame.plotBox.addPoint(0, time, mainMatrix.getInductanceDeltaV(ec), true);
                    ec.graphFrame.plotBox.fillPlot();
                }
            }
        }
        for (PhasePlane phasePlane : circuitPanel.phasePlanes) {
            if (phasePlane.xComponent == null || phasePlane.yComponent == null
                    || !circuitPanel.circuit.electricComponents.contains(phasePlane.xComponent)
                    || !circuitPanel.circuit.electricComponents.contains(phasePlane.yComponent)) {
                continue;
            }
            phasePlane.pb.addPoint(0, phasePlane.xComponent.calculatedValue,
                    phasePlane.yComponent.calculatedValue, true);
            phasePlane.pb.fillPlot();
        }
    }






    /**
     * Draws the panel showing all the components. Draws grid points,
     * components, labels. Grid points are only shown near mouse cursor.
     * Components are sometimes drawn in a special color.
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        ScaledGraphics sg = new ScaledGraphics(g);
        sg.setViewScale(viewScale);
        sg.setFontSize(fontSize);
        
        drawTerminals(sg);
                
        drawGridPoints(sg);
        
    //updateCircuit();

        // now we know I, ie k1
        drawComponents(sg);

        for (Component ec : circuitPanel.circuit.electricComponents) {
            if (ec.getClass() == Capacitor.class && ec.primaryValue != null) {
                //System.out.println(ec.secondaryValue);
            }
        }
        if (circuitPanel.isDrawing()) {
            drawTempComponent(sg);
        }
        
        if(isShowBoardOutline)
        {
            drawBoard(sg);
        }

        
        //Iterator i;
        //i = myParentCircuitEnginePanel.myCircuit.electricComponents.iterator();
        // System.out.println(terminals.size());
        // boolean
        // isClose=movingState!=MOVING&drawingState!=DRAWING&isCloseToComponent(mouseLocation)&!isClose(new
        // Terminal(new Point(mouseLocation)), INNER_RADIUS);

    }
    
    private void drawBoard(ScaledGraphics sg)
    {
        double oldLineWidth = sg.getLineWidth();
        sg.setColor(Color.BLACK);
        sg.setLineWidth(2);
        sg.drawRectangle(0, 0, boardXPixels, boardYPixels); // 432 = 6*72
        sg.setLineWidth(oldLineWidth);
    }
    
    
    public void drawTerminals(ScaledGraphics sg)
    {
        /*
        // todo:tk:only show terminals that have 3 or more components attched to them.
        for (Terminal t : myParentCircuitEnginePanel.myCircuit.myTerminals.values()) {
            if (t.myConnectedTo.size() >= 3) {
                sg.setColor(Color.black);
                sg.fillOval(t.getX() - 3, t.getY() - 3, 6, 6);
            }
        }
        */
        
        
        // todo:tk:show all terminals.
        for (Terminal t : circuitPanel.circuit.myTerminals.values()) {
            
            if(t.myConnectedTo.size() == 1)
            {
                sg.setColor(Color.red);
            }
            else
            {
                sg.setColor(Color.black);
            }
            
            
            sg.fillOval(t.getX() - 3, t.getY() - 3, 6, 6);
            
            if(this.isDrawTerminalLabels)
            {
                sg.drawString(t.getID(), t.getX() + 4, t.getY() - 4);
            }
            
            sg.setColor(Color.black);
        }
    }

    public void drawGridPoints(final ScaledGraphics sg) {
         
        sg.setColor(Color.black);
        
        if(isDrawGrid)
        {
            int x1 = circuitPanel.leftSideOffsetPixels;
            double deltax = circuitPanel.terminalXSpacing;
            int y1 = circuitPanel.topSideYOffsetPixels;
            double deltay = circuitPanel.terminalYSpacing;
            int xDistanceBetweenTerminalsPixels = circuitPanel.xDistanceBetweenTerminalsPixels;
            int yDistanceBetweenTerminalsPixels = circuitPanel.yDistanceBetweenTerminalsPixels;
            for (double x = x1; x <= x1 + deltax * (xDistanceBetweenTerminalsPixels - 1.0); x += deltax) {
                for (double y = y1; y <= y1 + deltay * (yDistanceBetweenTerminalsPixels - 1.0); y += deltay) {
                    sg.fillOval(x - 1, y - 1, 3, 3);
                }
            }
        }
        
        sg.setColor(Color.LIGHT_GRAY);
        if (circuitPanel.getMouseEntered()) {
            
            /*
            for (Terminal t : myParentCircuitEnginePanel.myCircuit.myTerminals.values()) {
                sg.setColor(Color.black);
                sg.fillOval(t.getX() - 3, t.getY() - 3, 6, 6);
            }
            */
            
            
            // Draw large gray circle around nearest terminal.
            sg.setColor(Color.LIGHT_GRAY);
            if (!circuitPanel.isDrawing()
                    && !circuitPanel.isMovingPoint()
                    && !circuitPanel.isMovingComponent()) {
                
                Point nearestPoint = new Point(circuitPanel.nearestGridPointXPixels(), circuitPanel
                        .nearestGridPointYPixels());
                
                if (circuitPanel.circuit.myTerminals.containsKey(nearestPoint)
                        && circuitPanel.myNearestTerminal == circuitPanel.circuit.myTerminals
                        .get(nearestPoint)) {
                    sg.setColor(Color.blue);

                    Terminal nearTerminal = circuitPanel.circuit.myTerminals.get(nearestPoint);
                    this.circuitPanel.hintPanel.message("T" + nearTerminal.terminalNumber + ": " + nearTerminal.getX() + ", " + nearTerminal.getY());
                }
                
                sg.fillOval(circuitPanel.nearestGridPointXPixels() - 4, circuitPanel
                        .nearestGridPointYPixels() - 4, 8, 8);
            }
        }
    }

    public void drawComponents(final ScaledGraphics sg) {
        // System.out.println();
        // System.out.println(myParentCircuitEnginePanel.myGridPoints.values().size());
        sg.setColor(Color.black);
        
        sg.drawString("Time: " + circuitPanel.circuit.time /*String.format("%.2f", myParentCircuitEnginePanel.myCircuit.time)*/ + "s", 10, 10);
        sg.drawString("Step size: " + circuitPanel.circuit.stepSize + "s", 10, 25);

        for (Component ec : circuitPanel.circuit.electricComponents) {
            // System.out.println(e.head);
            // System.out.println(e.tail);
            if (!circuitPanel.isDrawing() && //!myParentCircuitEnginePanel.getIsMovingComponent()&&
                    !circuitPanel.isMovingPoint()
                    && !circuitPanel.isNearTerminal()
                    && circuitPanel.nearSwitch()
                    && ec == circuitPanel.myNearestSwitch) {
                sg.setColor(Color.green);
                ec.draw(sg);
                
            } else if (ec == circuitPanel.myNearestComponent) {
                sg.setColor(Color.blue);
                
                double oldLineWidth = sg.getLineWidth();
                sg.setLineWidth(2.0);
                ec.draw(sg);
                sg.setLineWidth(oldLineWidth);
                
                
                sg.fillOval(circuitPanel.myNearestTerminal.getX() - 4,
                        circuitPanel.myNearestTerminal.getY() - 4, 8, 8);
                
                if (ec.getClass() == Ammeter.class || ec.getClass() == CurrentIntegrator.class
                        || ec.getClass() == VoltageIntegrator.class || ec.getClass() == Voltmeter.class
                        || ec.getClass() == Ohmmeter.class || ec.getClass() == CapacitanceMeter.class
                        || ec.getClass() == InductanceMeter.class) {
                    sg.drawString(ec.fullValue, 10, 40);
                }

                /* todo:tk:experimental.
                if (myParentCircuitEnginePanel.isProbe) {
                    this.myParentCircuitEnginePanel.myHintPanel.message(myParentCircuitEnginePanel.myCircuit.myComponentNames.get(ec.getID()) + ", " + myParentCircuitEnginePanel.myCircuit.mainMatrix.getCurrent(ec));
                }
                */
            } else {
                sg.setColor(Color.black);
                ec.draw(sg);
            }

            int x1 = (int) ec.headTerminal.getX();
            int y1 = (int) ec.headTerminal.getY();
            int x2 = (int) ec.tailTerminal.getX();
            int y2 = (int) ec.tailTerminal.getY();

            if (ec.getClass() == Ammeter.class) {
                //System.out.println(((Ammeter)e).ampString);
                sg.drawString(((Ammeter) ec).ampString, (int) ((x1 + x2) * 0.5 + 5)
                        - Meter.METER_SIZE,
                        (int) (0.5 * (y1 + y2)));
            } else if (ec.getClass() == CurrentIntegrator.class) {
                sg.drawString(((CurrentIntegrator) ec).chargeString, (int) ((x1 + x2) * 0.5 + 5)
                        - Meter.METER_SIZE,
                        (int) (0.5 * (y1 + y2)));
            } else if (ec.getClass() == VoltageIntegrator.class) {
                sg.drawString(((VoltageIntegrator) ec).magneticFluxString, (int) ((x1 + x2) * 0.5 + 4) -//changed 5 to 4 on Dec 25 2008
                        Meter.METER_SIZE,
                        (int) (0.5 * (y1 + y2)));
            } else if (ec.getClass() == Voltmeter.class) {
                sg.drawString(((Voltmeter) ec).voltageString, (int) ((x1 + x2) * 0.5 + 5)
                        - Meter.METER_SIZE, (int) (0.5 * (y1 + y2)));
            } else if (ec.getClass() == Ohmmeter.class) {
                sg.drawString(((Ohmmeter) ec).ohmString, (int) ((x1 + x2) * 0.5 + 5)
                        - Meter.METER_SIZE, (int) (0.5 * (y1 + y2)));
            } else if (ec.getClass() == CapacitanceMeter.class) {
                sg.drawString(((CapacitanceMeter) ec).capacitanceString, (int) ((x1 + x2) * 0.5 + 5)
                        - Meter.METER_SIZE, (int) (0.5 * (y1 + y2)));
            } else if (ec.getClass() == InductanceMeter.class) {
                sg.drawString(((InductanceMeter) ec).inductanceString, (int) ((x1 + x2) * 0.5 + 5)
                        - Meter.METER_SIZE, (int) (0.5 * (y1 + y2)));
            }
        }
    }

    public void drawTempComponent(final ScaledGraphics sg) {
        sg.setColor(Color.GRAY);
        circuitPanel.myTempComponent.draw(sg);
    }
    
    
    public void setViewScale(double viewScale) {
        this.viewScale = viewScale;

        this.revalidate();
        this.repaint();
    }
    
    public Dimension getPreferredSize() {
        
        int largestX = circuitPanel.getSize().width;
        int largestY = circuitPanel.getSize().height;
                for (Terminal terminal : circuitPanel.circuit.myTerminals.values()){
                    
                    if(terminal.getX() * viewScale > largestX)
                    {
                        largestX = (int) (terminal.getX() * viewScale) + 150;
                    }
                    
                    if(terminal.getY() * viewScale > largestY)
                    {
                        largestY = (int) (terminal.getY() * viewScale) + 150;
                    }
                }
        return new Dimension(largestX, largestY);
    }
    
    
    public Dimension getMaximumSize() {
        return this.getPreferredSize();
    }

    public Dimension getMinimumSize() {
        return this.getPreferredSize();
    }
    
    
    
    
    public int print(Graphics g, PageFormat pageFormat, int page) throws PrinterException {
        // Original code obtained from https://stackoverflow.com/questions/17904518/fit-scale-jcomponent-to-page-being-printed
        Graphics2D g2d = (Graphics2D) g;

        // Move origin to printer area corner.
        g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

        //FontMetrics titleFontMetrics = g2d.getFontMetrics(titleFont);
        //FontMetrics fontMetrics = g2d.getFontMetrics(new Font("helvetica", Font.PLAIN, 10));

        //System.out.println(page);
        if (page > 0) {
            return Printable.NO_SUCH_PAGE;
        }
        
        //System.out.println(pageFormat.getImageableWidth()  + ", " + pageFormat.getImageableHeight());
        
/*
        // Get the preferred size of the component...
        Dimension componentSize = this.getPreferredSize();
        
        // Make sure we size to the preferred size
        this.setSize(componentSize);
        // Get the the print size
        Dimension printSize = new Dimension();
        printSize.setSize(pageFormat.getImageableWidth(), pageFormat.getImageableHeight());

        // Calculate the scale factor
        double scaleFactor = getScaleFactorToFit(componentSize, printSize);
        // Don't want to scale up, only want to scale down
        if (scaleFactor > 1d) {
            scaleFactor = 1d;
        }

        // Calcaulte the scaled size...
        double scaleWidth = componentSize.width * scaleFactor;
        double scaleHeight = componentSize.height * scaleFactor;

        // Create a clone of the graphics context.  This allows us to manipulate
        // the graphics context without begin worried about what effects
        // it might have once we're finished
        Graphics2D g2 = (Graphics2D) g.create();
        // Calculate the x/y position of the component, this will center
        // the result on the page if it can
        double x = ((pageFormat.getImageableWidth() - scaleWidth) / 2d) + pageFormat.getImageableX();
        double y = ((pageFormat.getImageableHeight() - scaleHeight) / 2d) + pageFormat.getImageableY();
        // Create a new AffineTransformation
        AffineTransform at = new AffineTransform();
        // Translate the offset to out "center" of page
        at.translate(x, y);
        // Set the scaling
        at.scale(scaleFactor, scaleFactor);
        // Apply the transformation
        g2.transform(at);
        // Print the component
        this.printAll(g2);
        // Dispose of the graphics context, freeing up memory and discarding
        // our changes
        g2.dispose();

        this.revalidate();
*/        
        
        
        //g2d.drawLine(0, 50, 432, 50);
        
        this.viewScale = 1.0;
        
        this.paintComponent(g);
        

        /*
        ScaledGraphics sg = new ScaledGraphics(g);
        sg.setViewScale(viewScale);
        sg.setFontSize(fontSize);

        drawGridPoints(sg);
    //updateCircuit();

        // now we know I, ie k1
        drawComponents(sg);

        for (Component ec : myParentCircuitEnginePanel.myCircuit.electricComponents) {
            if (ec.getClass() == Capacitor.class && ec.primaryValue != null) {
                //System.out.println(ec.secondaryValue);
            }
        }
        if (myParentCircuitEnginePanel.isDrawing()) {
            drawTempComponent(sg);
        } //*/
        

        return Printable.PAGE_EXISTS;
    }
    
    
    public static double getScaleFactorToFit(Dimension original, Dimension toFit) {

        double dScale = 1d;

        if (original != null && toFit != null) {

            double dScaleWidth = getScaleFactor(original.width, toFit.width);
            double dScaleHeight = getScaleFactor(original.height, toFit.height);

            dScale = Math.min(dScaleHeight, dScaleWidth);

        }

        return dScale;

    }

    public static double getScaleFactor(int iMasterSize, int iTargetSize) {

        double dScale = 1;
        if (iMasterSize > iTargetSize) {

            dScale = (double) iTargetSize / (double) iMasterSize;

        } else {

            dScale = (double) iTargetSize / (double) iMasterSize;

        }

        return dScale;

    }
    
    
/*
    // todo:tk:I am not sure what this code is used for.
    
    public HashSet<Component> union(HashSet<Component> a, HashSet<Component> b) {
        HashSet<Component> u = new HashSet<Component>(a);
        u.addAll(b);
        return u;
    }

    public void findConstantCaps() {
        for (Component ec : myParentCircuitEnginePanel.myCircuit.electricComponents) {
            if (ec.getClass() == Capacitor.class) {
                ec.isConstant = false;
                ec.isHeldConstant = false;
                HashSet<Component> seen = new HashSet<Component>();
                for (Component e : union(ec.headTerminal.in, ec.headTerminal.out)) {
                    if ((e.getClass() == Wire.class || e.getClass() == VoltageSource.class || e.getClass() == Ammeter.class
                            || ec.getClass() == CurrentIntegrator.class)) {
                        seen.add(e);
                        expand(ec, e, seen);
                    }
                }
                //(ec,ec,seen);
            } else {
                ec.isConstant = false;
                ec.isHeldConstant = false;
            }
        }
    }
    
        public void expand(Component e1, Component e2, HashSet<Component> seen) {
        if (e1.isConstant) {
            return;
        }
        for (Component ec : union(union(e2.headTerminal.in, e2.headTerminal.out), union(e2.tailTerminal.in, e2.tailTerminal.out))) {
            if ((ec.getClass() == Wire.class || ec.getClass() == VoltageSource.class || ec.getClass() == Ammeter.class
                    || ec.getClass() == CurrentIntegrator.class)
                    && !seen.contains(ec)) {
                if (ec.headTerminal == e1.tailTerminal || ec.tailTerminal == e1.tailTerminal) {
                    e1.isConstant = true;
                    e1.isHeldConstant = true;
                    System.out.println("is constant");
                    return;
                }
                seen.add(ec);
                expand(e1, ec, seen);
            }
        }
    }
*/

}
