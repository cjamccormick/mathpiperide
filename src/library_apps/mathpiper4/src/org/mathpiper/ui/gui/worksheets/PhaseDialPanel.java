package org.mathpiper.ui.gui.worksheets;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.RenderingHints;
import java.awt.geom.Arc2D;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;

// Code from https://www.developer.com/java/data/how-to-manipulate-complex-graphical-elements-in-java-with-the-2d-api.html

public class PhaseDialPanel extends JPanel {

    private static final long serialVersionUID = 1L;
    int xs, ys, xm, ym, xh, yh;

    public PhaseDialPanel() {
        //this.setDoubleBuffered(true);
        setAngle1(0);
    }

    public void setAngle1(double angleDegrees) {
        //this.setDoubleBuffered(true);
        xm = (int) (Math.cos(angleDegrees * Math.PI / 180) * 83 + 0);
        ym = (int) (Math.sin(angleDegrees * Math.PI / 180) * 83 + 0);
        repaint();

    }

    public void paintComponent(Graphics g) {
        //Graphics2D sg = g.;
        //sg.translate(getWidth() / 2, getHeight() / 2);
        //sg.translate(0, 400);
        //int side = getWidth() > getHeight() ? getHeight() : getWidth();
        ScaledGraphics sg = new ScaledGraphics(g);

        sg.setViewScale(1.5);

        sg.translate(getWidth() / 2, getHeight() / 2);
        //int side = getWidth() > getHeight() ? getHeight() : getWidth();
        //sg.scale(side / 250, side / 250);

        /*
        
        AffineTransform at = new AffineTransform();
        //at.translate(150,300);
        at.scale(.75, .75);
        at.translate(200, 300);

        sg.setTransform(at);
         */
        setAllRenderingHints(sg);
        drawFace(sg);
        drawNumbers(sg);
        drawHands(sg);
    }



    private void setAllRenderingHints(ScaledGraphics sg) {
        sg.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        sg.setRenderingHint(RenderingHints.KEY_DITHERING,
                RenderingHints.VALUE_DITHER_ENABLE);
        sg.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);
        sg.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        sg.setRenderingHint(RenderingHints.KEY_TEXT_LCD_CONTRAST, 100);
        sg.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS,
                RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        sg.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION,
                RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        sg.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
                RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        sg.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL,
                RenderingHints.VALUE_STROKE_PURE);

    }

    private void drawFace(ScaledGraphics sg) {

        sg.setPaint(Color.white);
        sg.fill(new Arc2D.Double(-110, -110, 220, 220, 0, 360, Arc2D.CHORD));
        sg.setColor(Color.darkGray);
        sg.setStroke(new BasicStroke(4.0f));
        sg.draw(new Arc2D.Double(-110, -110, 220, 220, 0, 360, Arc2D.CHORD));

        sg.setColor(Color.BLUE);
        sg.setStroke(new BasicStroke(2.0f));
        
        for (int i = 0; i < 360; i += 10) {
            //if ((i % 30) != 0) {
            //sg.setStroke(new BasicStroke(1.0f));
            //sg.setColor(Color.darkGray);
            //sg.drawLine(92, 0, 96, 0);
            //} else {
            //sg.setColor(new Color(255, 22, 10));
            //sg.setStroke(new BasicStroke(2.0f));
            if(i % 30 == 0)
            {
                sg.drawLine(87, 0, 96, 0);
            }
            else
            {
                sg.drawLine(92, 0, 96, 0);
            }

            //}
            sg.rotate((Math.PI / 180.0) * 10.0);
        }


    }
    
    private void drawNumbers(ScaledGraphics sg){
        
        sg.setColor(Color.BLACK);
        double numbersRadius = 70;
        double xOffset = -14;
        double yOffset = 5;
        for (int angle = 0; angle < 360; angle += 30) {
            sg.setFontSize(15);
            String number = "" + angle;
            if (number.length() == 1) {
                number = "  " + number;
            }
            if (number.length() == 2) {
                number = " " + number;
            }
            sg.drawString(number, xOffset + numbersRadius * Math.cos(angle * Math.PI / 180), yOffset + -numbersRadius * Math.sin(angle * Math.PI / 180));
        }
    }

    private void drawHands(ScaledGraphics sg) {
        //sg.setColor(new Color(220, 22, 10));
        
        //sg.setStroke(new BasicStroke(5.0f));
        //sg.drawLine(0, 0, xh, yh);
        
        sg.setColor(Color.RED);
        sg.setStroke(new BasicStroke(3.0f));
        sg.drawLine(0, 0, xm, ym);
        
        
        /*sg.setColor(Color.black);
        sg.setStroke(new BasicStroke(2.0f));
        sg.drawLine(0, 0, xs, ys);
        
        */
        sg.setColor(Color.black);
        sg.fillOval(-5, -5, 10, 10);
        //sg.setColor(Color.white);
        //sg.fillOval(-2, -2, 4, 4);
        
    }

    public static void main(String[] args) {
        
      JFrame f = new JFrame();
      f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      f.setSize(400, 400);
      PhaseDialPanel a = new PhaseDialPanel();
      f.add(a, BorderLayout.CENTER);
      f.setVisible(true);
         
    }

}
