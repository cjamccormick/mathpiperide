package org.mathpiper.ui.gui.applications.voscilloscope.oscilloscope;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * <p>Title: Virtual Oscilloscope.</p>
 * <p>Description: A Oscilloscope simulator</p>
 * <p>Copyright (C) 2003 José Manuel Gómez Soriano</p>
 * <h2>License</h2>
 * <p>
 This file is part of Virtual Oscilloscope.

 Virtual Oscilloscope is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Virtual Oscilloscope is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Virtual Oscilloscope; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 * </p>
 */

// Referenced classes of package es.upv.scope:
//            PTime, PVolt, PQuality, Screen

public class PanelBackground extends Panel
    implements MouseListener
{

    public static Color backgroundLevel1Color;
    public static Color backgroundLevel2Color;
    public static Color panelColor;
    public PanelTime pTime;
    public PanelVolt pVolt;
    public PanelQuality pQuality;
    public Screen screen;

    public PanelBackground()
    {
        backgroundLevel1Color = new Color(90, 90, 90);
        backgroundLevel2Color = new Color(120, 120, 120);
        panelColor = new Color(80, 80, 100);
        setBackground(Color.white);
        setForeground(panelColor);
        setLayout(null);
        pTime = new PanelTime();
        add(pTime);
        pTime.setLocation(373, 12);
        pVolt = new PanelVolt();
        add(pVolt);
        pVolt.setLocation(373, 189);
        pQuality = new PanelQuality();
        add(pQuality);
        pQuality.setLocation(15, 335);
        pQuality.BtnDigital.addMouseListener(this);
        screen = new Screen();
        add(screen);
        screen.setBounds(38, 20, 300, 300); //todo:tk:the size of the scope display can be adjusted here.
        addMouseListener(this);
    }

    public void addNotify()
    {
        super.addNotify();
    }

    public Dimension getMinimumSize()
    {
        return new Dimension(780, 400);
    }

    public Dimension getPreferredSize()
    {
        return getMinimumSize();
    }

    public void setLocation(int x, int y)
    {
        super.setLocation(x, y);
        setSize(getMinimumSize());
    }

    public void paint(Graphics g)
    {
        drawBackground(g);
    }

    public void mouseClicked(MouseEvent e)
    {
        if(e.getSource() == pQuality.BtnDigital)
        {boolean xx = pQuality.BtnDigital.getValue();
            pVolt.BtnCiclica1.setVisible(!pQuality.BtnDigital.getValue());
            pVolt.BtnCiclica2.setVisible(!pQuality.BtnDigital.getValue());
            pTime.btnXPos.setValue(19F);
            pTime.btnXPos.setVisible(!pQuality.BtnDigital.getValue());
            pTime.btnHoldOff.setVisible(!pQuality.BtnDigital.getValue());
            pTime.btnMoreLess.setVisible(!pQuality.BtnDigital.getValue());
            pTime.btnLevel.setVisible(!pQuality.BtnDigital.getValue());
            pTime.btnAutomaticManual.setVisible(!pQuality.BtnDigital.getValue());
            pTime.btnTimePosition.setVisible(pQuality.BtnDigital.getValue());
            pTime.labelTimePosition.setVisible(pQuality.BtnDigital.getValue());
        }
    }

    public void mouseEntered(MouseEvent mouseevent)
    {
    }

    public void mouseExited(MouseEvent mouseevent)
    {
    }

    public void mousePressed(MouseEvent mouseevent)
    {
    }

    public void mouseReleased(MouseEvent mouseevent)
    {
    }

    public void drawBackground(Graphics g)
    {
        g.setColor(backgroundLevel1Color);
        g.fillRoundRect(0, 0, 780, 400, 45, 45);
        g.setColor(backgroundLevel2Color);
        g.fillRoundRect(4, 4, 772, 392, 45, 45);
        g.setColor(backgroundLevel1Color);
        g.fillRoundRect(25, 7, 327, 327, 20, 20);
        int x1[] = {
            14, 25, 25
        };
        int y1[] = {
            172, 17, 324
        };
        g.fillPolygon(x1, y1, 3);
        int x2[] = {
            363, 352, 352
        };
        int y2[] = {
            172, 17, 324
        };
        g.fillPolygon(x2, y2, 3);
        g.setColor(backgroundLevel2Color);
        g.fillRoundRect(29, 11, 319, 319, 20, 20);
        g.setColor(Color.black);
        g.fillRoundRect(33, 15, 310, 310, 15, 15);
    }

    public void update(Graphics g)
    {
        paint(g);
    }
}
