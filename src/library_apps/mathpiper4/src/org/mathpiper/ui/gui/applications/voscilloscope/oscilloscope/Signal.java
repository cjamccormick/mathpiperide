package org.mathpiper.ui.gui.applications.voscilloscope.oscilloscope;

import org.mathpiper.ui.gui.applications.voscilloscope.simulator.Tokenizer;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * <p>Title: Virtual Oscilloscope.</p>
 * <p>Description: A Oscilloscope simulator</p>
 * <p>Copyright (C) 2003 José Manuel Gómez Soriano</p>
 * <h2>License</h2>
 * <p>
 This file is part of Virtual Oscilloscope.

 Virtual Oscilloscope is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Virtual Oscilloscope is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Virtual Oscilloscope; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 * </p>
 */

// Referenced classes of package es.upv.scope:
//            OperatorSignalException, DlgOpAdq, ClienteSignal, DlgFile

public class Signal
{

    public static final int AC = 1;
    public static final int DC = 2;
    public static final int GND = 0;
    public static final int MAX12BITS = 4096;
    private boolean ciclica;
    private boolean invert;
    private boolean visible;
    private int yPos;
    private double vDiv;
    private int samples[];
    private int nSamples;
    private double posSample;
    private int signalType;
    private int levelContinuous;
    private double samplingFrequency;
    private double valueMax;
    private double valueMin;

    public synchronized void rewind()
    {
        posSample = 0.0F;
    }

    public synchronized double getVoltageMax()
    {
        return valueMax;
    }

    public synchronized void setVoltageMax(double voltageMax)
    {
        valueMax = voltageMax;
    }

    public synchronized double getVoltageMin()
    {
        return valueMin;
    }

    public synchronized void setVoltageMin(double voltageMin)
    {
        valueMax = voltageMin;
    }

    public synchronized double getVoltage(int pos)
    {
        if(pos >= nSamples || pos < 0)
        {
            return 0.0F;
        }
        double value = ((double)samples[pos] * (valueMax - valueMin)) / (double)4096 + (valueMax + valueMin) / (double)2;
        if(signalType == 1)
        {
            value -= getContinuous();
        }
        return value;
    }

    public synchronized void setVoltage(double value, int pos)
    {
        samples[pos] = (int)(((double)4096 * (value - valueMin)) / (valueMax - valueMin) - (double)2048);
    }

    public synchronized double sigSample(double time)
    {
        double value;
        if(nSamples == 0 || signalType == 0 || posSample >= (double)nSamples)
        {
            value = 0.0F;
        } else
        {
            int x = (int)posSample;
            double ini = getVoltage(x);
            double fin = getVoltage(ciclica ? (x + 1) % nSamples : x + 1);
            value = (fin - ini) * (posSample - (double)x) + ini;
            double Inc = samplingFrequency * (double)10 * time;
            advance(Inc);
        }
        return (invert ? value : -value) + (double)yPos;
    }

    public Signal()
    {
        yPos = 0;
        ciclica = false;
        invert = visible = false;
        vDiv = 2.0F;
        nSamples = 0;
        posSample = 0.0F;
        signalType = 2;
        levelContinuous = 0;
        samplingFrequency = 0.0F;
        valueMax = 20F;
    }

    public Signal(InputStream is)
    {
        this();
        Signal.this.setSamples(is);
    }

    public void setSamples(InputStream fSample)
    {
        nSamples = 0;
        posSample = 0.0F;
        samplingFrequency = 6000F;
        try
        {
            Tokenizer tok = new Tokenizer(fSample);
            tok.whitespaceChars(58, 255);
            tok.wordChars(48, 57);
            valueMax = tok.readDouble();
            valueMin = tok.readDouble();
            nSamples = tok.readInt();
            samplingFrequency = tok.readInt();
            int length = nSamples * 2;
            byte buf[] = new byte[length];
            fSample.skip(1L);
            int num = fSample.read(buf, 0, length);
            if(num >= 0)
            {
                nSamples = num / 2;
            }
            samples = new int[nSamples];
            System.out.println("valueMax:" + valueMax);
            System.out.println("valueMin:" + valueMin);
            for(int i = 0; i < nSamples; i++)
            {
                samples[i] = (buf[i * 2] + 128 | (buf[i * 2 + 1] + 128 & 0xff) << 8) - 2048;
            }

            fSample.close();
        }
        catch(IOException e)
        {
            System.out.println("Error en el flujo de entrada:" + e);
        }
    }

    public void setSamples(byte bsamples[])
    {
        ByteArrayInputStream bais = new ByteArrayInputStream(bsamples);
        Signal.this.setSamples(((InputStream) (bais)));
    }

    public synchronized int getLength()
    {
        return signalType != 0 ? nSamples : 0;
    }

    public synchronized double getFastSampling()
    {
        return samplingFrequency;
    }

    public synchronized void sinContinuous()
    {
        signalType = 1;
    }

    public synchronized void conContinuous()
    {
        signalType = 2;
    }

    public synchronized void signalGND()
    {
        signalType = 0;
    }

    public synchronized int getTipoSignal()
    {
        return signalType;
    }

    private static int calculaContinuous(Signal sig)
    {
        int cont = 0;
        for(int i = 0; i < sig.nSamples; i++)
        {
            cont += sig.samples[i];
        }

        if(sig.nSamples > 0)
        {
            cont /= sig.nSamples;
        }
        return cont;
    }

    private static double calculaValueMax(double voltagees[])
    {
        double max = (-1.0F / 0.0F);
        for(int i = 0; i < voltagees.length; i++)
        {
            if(max < voltagees[i])
            {
                max = voltagees[i];
            }
        }

        return max;
    }

    private static double calculaValueMin(double voltagees[])
    {
        double min = (1.0F / 0.0F);
        for(int i = 0; i < voltagees.length; i++)
        {
            if(min > voltagees[i])
            {
                min = voltagees[i];
            }
        }

        return min;
    }

    public synchronized void advance(double inc)
    {
        posSample = ciclica ? (posSample + inc) % (double)nSamples : posSample + inc;
    }

    public void connectSignal(URL dir, SignalClient cs)
    {
        DlgOpAdq dlgOp = DlgOpAdq.makeDialog();
        int op;
        if(cs.hasSignal())
        {
            op = dlgOp.showModal(new Signal(new ByteArrayInputStream(cs.getData())));
        } else
        {
            op = dlgOp.showModal();
        }
        if(op != DlgOpAdq.CANCELAR)
        {
            if(dlgOp.getOpcion() == DlgOpAdq.ARCHIVO)
            {
                FileDialog dlgFile = FileDialog.makeDialog(dir);
                try
                {
                    if(dlgFile.showModal())
                    {
                        Signal.this.setSamples((new URL(dir + dlgFile.getFile())).openStream());
                    }
                }
                catch(MalformedURLException e)
                {
                    System.out.println("Mal formada URL:" + e);
                }
                catch(IOException e)
                {
                    System.out.println("Error de E/S:" + e);
                }
            } else
            {
                synchronized(cs)
                {
                    if(cs.hasSignal())
                    {
                        Signal.this.setSamples(new ByteArrayInputStream(cs.getData()));
                    } else
                    {
                        System.out.println("No signal through port 5454");
                    }
                }
            }
        }
        levelContinuous = calculaContinuous(this);
    }

    public void conectaSignal(byte data[])
    {
        //DlgOpAdq dlgOp = DlgOpAdq.makeDialog();
        Signal.this.setSamples(new ByteArrayInputStream(data));
        levelContinuous = calculaContinuous(this);
    }

    public synchronized double getPosShow()
    {
        return posSample;
    }

    public synchronized void setPosShow(double pos)
    {
        posSample = pos;
    }

    public synchronized boolean isInvert()
    {
        return invert;
    }

    public synchronized void setInvert(boolean inv)
    {
        invert = inv;
    }

    public synchronized int getYPos()
    {
        return yPos;
    }

    public synchronized void setYPos(int pos)
    {
        yPos = pos;
    }

    public synchronized boolean isVisible()
    {
        return visible;
    }

    public synchronized void setVisible(boolean visible)
    {
        this.visible = visible;
    }

    public synchronized void setCiclica(boolean ciclica)
    {
        this.ciclica = ciclica;
    }

    public synchronized boolean isCiclica()
    {
        return ciclica;
    }

    public synchronized void setVDiv(double vdiv)
    {
        vDiv = vdiv;
    }

    public synchronized double getVDiv()
    {
        return vDiv;
    }

    public synchronized double getContinuous()
    {
        return ((valueMax - valueMin) * (double)levelContinuous) / (double)4096 + (valueMax + valueMin) / (double)2;
    }

    public static Signal sum(Signal a, Signal b)
        throws OperatorSignalException
    {
        Signal resul = new Signal();
        if(a.nSamples != b.nSamples && a.samplingFrequency != b.samplingFrequency)
        {
            throw new OperatorSignalException("The signals do not have the same number of samples or the same sampling frequency.");
        }
        resul.nSamples = a.nSamples;
        resul.samplingFrequency = a.samplingFrequency;
        resul.samples = new int[resul.nSamples];
        double voltagees[] = new double[resul.nSamples];
        for(int i = 0; i < resul.nSamples; i++)
        {
            voltagees[i] = a.getVoltage(i) + b.getVoltage(i);
        }

        resul.valueMax = calculaValueMax(voltagees);
        resul.valueMin = calculaValueMin(voltagees);
        for(int i = 0; i < resul.nSamples; i++)
        {
            resul.setVoltage(voltagees[i], i);
        }

        resul.levelContinuous = calculaContinuous(resul);
        return resul;
    }

    public static Signal difference(Signal a, Signal b)
        throws OperatorSignalException
    {
        Signal resul = new Signal();
        if(a.nSamples != b.nSamples && a.samplingFrequency != b.samplingFrequency)
        {
            throw new OperatorSignalException("The signals do not have the same number of samples or the same sampling frequency.");
        }
        resul.nSamples = a.nSamples;
        resul.samplingFrequency = a.samplingFrequency;
        resul.samples = new int[resul.nSamples];
        double voltagees[] = new double[resul.nSamples];
        for(int i = 0; i < resul.nSamples; i++)
        {
            voltagees[i] = a.getVoltage(i) - b.getVoltage(i);
        }

        resul.valueMax = calculaValueMax(voltagees);
        resul.valueMin = calculaValueMin(voltagees);
        for(int i = 0; i < resul.nSamples; i++)
        {
            resul.setVoltage(voltagees[i], i);
        }

        resul.levelContinuous = calculaContinuous(resul);
        return resul;
    }

    public static Signal multiply(Signal a, Signal b)
        throws OperatorSignalException
    {
        Signal signal = new Signal();
        if(a.nSamples != b.nSamples && a.samplingFrequency != b.samplingFrequency)
        {
            throw new OperatorSignalException("The signals do not have the same number of samples or the same sampling frequency.");
        }
        signal.nSamples = a.nSamples;
        signal.samplingFrequency = a.samplingFrequency;
        signal.samples = new int[signal.nSamples];
        double voltagees[] = new double[signal.nSamples];
        for(int i = 0; i < signal.nSamples; i++)
        {
            voltagees[i] = a.getVoltage(i) * b.getVoltage(i);
        }

        signal.valueMax = calculaValueMax(voltagees);
        signal.valueMin = calculaValueMin(voltagees);
        for(int i = 0; i < signal.nSamples; i++)
        {
            signal.setVoltage(voltagees[i], i);
        }

        signal.levelContinuous = calculaContinuous(signal);
        return signal;
    }

    public static Signal generateSinusodial(double vPeak, double offset, int numberOfSamples, double signalFrequency, double numberOfCyclesToGenerate, double phaseShift)
    {
        Signal signal = new Signal();
        signal.nSamples = numberOfSamples;
        signal.samplingFrequency = (signalFrequency * (double)numberOfSamples) / numberOfCyclesToGenerate;
        signal.samples = new int[numberOfSamples];
        signal.valueMax = vPeak + offset;
        signal.valueMin = -vPeak + offset;
        for(int i = 0; i < numberOfSamples; i++)
        {
            signal.setVoltage((double) Math.sin( ((double)(i * 2) * 3.1415926535897931D * (double)numberOfCyclesToGenerate) / (double)numberOfSamples + phaseShift * 3.1415926535897931D/180.0) * vPeak + offset, i);
        }

        signal.levelContinuous = calculaContinuous(signal);
        return signal;
    }

    public static Signal generateSquare(double nHigh, double nLow, double nDisp, int numberOfSamples, double signalFrequency, double numberOfCyclesToGenerate, double phaseShift)
    {
        Signal signal = new Signal();
        signal.nSamples = numberOfSamples;
        signal.samplingFrequency = signalFrequency;
        signal.samples = new int[numberOfSamples];
        signal.valueMax = nHigh;
        signal.valueMin = nLow;
        for(int i = 0; i < numberOfSamples; i++)
        {
            double voltage = Math.sin(((double)i * numberOfCyclesToGenerate) / (double)numberOfSamples + phaseShift) * (double)(nHigh - nLow) + (double)(nHigh + nLow) < (double)nDisp ? nLow : nHigh;
            System.out.println(voltage);
            signal.setVoltage(voltage, i);
        }

        signal.levelContinuous = calculaContinuous(signal);
        return signal;
    }

    public void sendSignal(OutputStream fSamples)
        throws IOException
    {
        fSamples.write((Double.valueOf(valueMax)).toString().getBytes());
        fSamples.write(58);
        fSamples.write((Double.valueOf(valueMin)).toString().getBytes());
        fSamples.write(58);
        fSamples.write((Integer.valueOf(nSamples)).toString().getBytes());
        fSamples.write(58);
        fSamples.write((Double.valueOf(samplingFrequency)).toString().getBytes());
        fSamples.write(58);
        fSamples.write(13);
        for(int i = 0; i < nSamples; i++)
        {
            fSamples.write(samples[i] - 128);
            fSamples.write((samples[i] + 2048 >> 8) - 128);
        }

    }
}
