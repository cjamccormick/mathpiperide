package org.mathpiper.ui.gui.applications.circuitpiper.model.components;

import java.awt.Color;
import java.awt.RenderingHints;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.mathpiper.ui.gui.applications.circuitpiper.model.Terminal;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Wire;
import org.mathpiper.ui.gui.applications.circuitpiper.view.CircuitPanel;
import org.mathpiper.ui.gui.applications.circuitpiper.view.GraphFrame;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;
/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */

abstract public class Component  {
    
    public static final Map<String, Double> siToValue;

    static
    {
        siToValue = new HashMap<String, Double>();

        siToValue.put("Y", Math.pow(10, 24));
        siToValue.put("Z", Math.pow(10, 21));
        siToValue.put("E", Math.pow(10, 18));
        siToValue.put("P", Math.pow(10, 15));
        siToValue.put("T", Math.pow(10, 12));
        siToValue.put("G", Math.pow(10, 9));
        siToValue.put("M", Math.pow(10, 6));
        siToValue.put("k", Math.pow(10, 3));
        siToValue.put("h", Math.pow(10, 2));
        siToValue.put("da", Math.pow(10, 1));
        siToValue.put("", Math.pow(10, 0));
        siToValue.put("d", Math.pow(10, -1));
        siToValue.put("c", Math.pow(10, -2));
        siToValue.put("m", Math.pow(10, -3));
        siToValue.put("μ", Math.pow(10, -6));
        siToValue.put("n", Math.pow(10, -9));
        siToValue.put("p", Math.pow(10, -12));
        siToValue.put("f", Math.pow(10, -15));
        siToValue.put("a", Math.pow(10, -18));
        siToValue.put("z", Math.pow(10, -21));
        siToValue.put("y", Math.pow(10, -24));

    }

    public String componentUID;
    public Color color;
    public Terminal headTerminal;
    public Terminal tailTerminal;
    public String siPrefix = "";
    private static String micro = "\u03BC";
    private static String[] siPrefixes = {"Y", "Z", "E", "P", "T", "G", "M", "k", "", "m", micro, "n", "p", "f", "a", "z", "y"};
    public String primary;
    public String secondary;
    public String primaryUnit;
    public String primaryUnitPlural;
    public String primarySymbol;
    public String primaryUnitSymbol;
    public String secondaryUnitSymbol;
    public String preselectedPrimaryPrefix;
    public String preselectedFrequencyPrefix;
    public String preselectedPhaseShiftPrefix;
    public String enteredPrimaryValue;
    public String selectedPrimaryPrefix;
    public String selectedFrequencyPrefix;
    public String selectedPhaseShiftPrefix;
    public String frequencySymbol;
    public Double primaryValue;//changed to double from Double on Dec 29 2008
    public Double frequency;
    public Double phaseShift;
    public Double calculatedValue;
    public String enteredFrequency;
    public String enteredPhaseShift;
    public double secondaryValue = 0.0;
    public boolean isConstant, isHeldConstant;
    public String fullValue = "";
    public GraphFrame graphFrame;
    protected CircuitPanel circuitPanel;
    //static{
    //  System.out.println(formatValue(1));
    //  System.out.println(formatValue(10));
    //  System.out.println(formatValue(0.99999999));
    //  System.out.println(formatValue(9.999999999999999));
    //}
    public double y1, yj, zj, k1, k2, k3, k4, k5, k6, I1, y2, y3, originalValue, twoStepValue, oneStepValue, originalCurrent, delta1;

    
    public Component(final int x, final int y, CircuitPanel circuitPanel) {
        this.circuitPanel = circuitPanel;
        headTerminal = new Terminal(x, y, circuitPanel);
        tailTerminal = new Terminal(x+1, y+1, circuitPanel);
        connectHeadAndTail();
    }
    
    public String getID()
    {
        return primarySymbol + componentUID;
    }
    
    //import java.text.DecimalFormat;
    public static String formatValue(double d) {
        if (Double.isInfinite(d) || Double.isNaN(d)) {
            return "Error ";
        }
        String sign = "";
        if (d == 0.0) {
            return "0.00 ";
        }
        if (d < 0) {
            sign = "-";
        }
        double absd = Math.abs(d);
        int p = (int) Math.floor(Math.log(absd) / Math.log(1000));
        double mantissa = absd / Math.pow(1000, p);
        //System.out.println(mantissa+" "+p*3);
        //System.out.println((-3.5));
        DecimalFormat format;
        if (mantissa < 10) {
            format = new DecimalFormat("0.00");
            //mantissa=((mantissa*100))/100; 
        } else if (mantissa < 100) {
            format = new DecimalFormat("00.0");
            //mantissa=((mantissa*10))/10;
        } else {
            format = new DecimalFormat("000");
        }
        if (absd < 10e-24 || absd >= 10e27 || 8 - p < 0 || 8 - p >= siPrefixes.length) {
            return sign + format.format(mantissa) + "E" + 3 * p + "";
        }
        //int i=8-p;

        return sign + format.format(mantissa) + siPrefixes[8 - p] + "";
    }


    public void moveTail(final int x, final int y) {
        tailTerminal.setX(x);
        tailTerminal.setY(y);
        connectHeadAndTail();
    }

    public void setTail(Terminal theTerminal) {
        disconnectHeadAndTail();
        tailTerminal = theTerminal;
        connectHeadAndTail();
    }

    public void setHead(Terminal theTerminal) {
        disconnectHeadAndTail();
        headTerminal = theTerminal;
        connectHeadAndTail();
    }

    public void connectHeadAndTail() {
        headTerminal.myConnectedTo.add(tailTerminal);
        tailTerminal.myConnectedTo.add(headTerminal);
        headTerminal.in.add(this);
        tailTerminal.out.add(this);
    }

    public void disconnectHeadAndTail() {
        headTerminal.myConnectedTo.remove(tailTerminal);
        tailTerminal.myConnectedTo.remove(headTerminal);
        headTerminal.in.remove(this);
        tailTerminal.out.remove(this);
    }

    public boolean badSize() {
        return headTerminal.getX() == tailTerminal.getX() && headTerminal.getY() == tailTerminal.getY();
    }

    public void draw(ScaledGraphics sg) {
        
        sg.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);

        if(((this instanceof Wire) && circuitPanel.drawingPanel.isDrawWireLabels) || !(this instanceof Wire))
        {
            drawLabel(sg);
        }

    }

    public double sqr(double x) {
        return x * x;
    }
    
    public int getLabelDistance()
    {
        return 25;
    }

    public void drawLabel(ScaledGraphics sg) {
        String unit;
        
        if (this.primaryValue != null && this.primaryValue == 1) {
            unit = this.primaryUnitSymbol;//primaryUnit;
        } else {
            unit = this.primaryUnitSymbol;//Plural;
        }
        
        int distanceFromSymbol = getLabelDistance();

        int x1 =  this.headTerminal.getX();
        int y1 =  this.headTerminal.getY();
        int x2 =  this.tailTerminal.getX();
        int y2 =  this.tailTerminal.getY();
        int run = x2 - x1;
        int rise = y2 - y1;
        double d = Math.sqrt(sqr(run / 2.0) + sqr(rise / 2.0));
        
        double textYOffset = sg.getScaledTextHeight(getID());
        //textYOffset = 10; //todo:tk:font metrics don't seem to be working duirng printing.
        
        if (rise * run > 0) {
            sg.drawString(getID(),
                     (x1 + run / 2.0 + Math.abs(rise) / (2.0 * d) * distanceFromSymbol),
                     (y1 + rise / 2.0 - Math.abs(run) / (2.0 * d) * distanceFromSymbol)
            );
        } else {
            sg.drawString(getID(),  
                    (x1 + run / 2.0 + Math.abs(rise) / 2.0 / d * distanceFromSymbol),
                    (y1 + rise / 2.0 + Math.abs(run) / 2.0 / d * distanceFromSymbol));
        }

        if (this.primaryValue != null) {

            if (rise * run > 0) {
                sg.drawString(formatValue(this.primaryValue) + unit,
                         (x1 + run / 2.0 + Math.abs(rise) / (2.0 * d) * distanceFromSymbol),
                         (y1 + textYOffset + rise / 2.0 - Math.abs(run) / (2.0 * d) * distanceFromSymbol)
                );
            } else {

                sg.drawString(formatValue(this.primaryValue) + unit,  
                        (x1 + run / 2.0 + Math.abs(rise) / 2.0 / d * distanceFromSymbol),
                        (y1 + textYOffset + rise / 2.0 + Math.abs(run) / 2.0 / d * distanceFromSymbol));
            }
        }
    }

    public void reverse() {
        disconnectHeadAndTail();
        Terminal oldHead = headTerminal;
        this.headTerminal = this.tailTerminal;
        this.tailTerminal = oldHead;
        connectHeadAndTail();
    }
    
    
    /*
     g the graphics component.
     x1 x-position of first point.
     y1 y-position of first point.
     x2 x-position of second point.
     y2 y-position of second point.
     d  the width of the arrow.
     h  the height of the arrow.

        Obatined from https://stackoverflow.com/questions/2027613/how-to-draw-a-directed-arrow-line-in-java
     */
    public void drawArrowLine(ScaledGraphics sg, double x1, double y1, double x2, double y2, double d, double h) {

        double dx = x2 - x1;
        double dy = y2 - y1;

        double D = Math.sqrt(dx*dx + dy*dy);
        double xm = D - d;
        double xn = xm;
        double ym = h;
        double yn = -h;
        double x;

        double sin = dy / D, cos = dx / D;

        x = xm*cos - ym*sin + x1;
        ym = xm*sin + ym*cos + y1;
        xm = x;

        x = xn*cos - yn*sin + x1;
        yn = xn*sin + yn*cos + y1;
        xn = x;

        int[] xpoints = {(int) Math.round(x2), (int) Math.round(xm), (int) Math.round(xn)};
        int[] ypoints = {(int) Math.round(y2), (int) Math.round(ym), (int) Math.round(yn)};

        sg.drawLine(x1, y1, x2, y2);
        sg.fillPolygon(xpoints, ypoints, 3);
    }

    
    protected void handleRLCAttribute(Stack<String> attributes) throws Exception
    {
        if(attributes != null && attributes.size() == 1)
        {
            String attribute = attributes.pop();
            try {
                double value = Double.parseDouble(attribute);
                
                primaryValue = value;
                enteredPrimaryValue = "" + 1/siToValue.get(siPrefix) * primaryValue;
            }
            catch (NumberFormatException nfe)
            {
                primaryValue = null;
                enteredPrimaryValue = "";
            }
        }
        else
        {
            throw new Exception("Wrong number of attibutes.");
        }
    }
    
    protected void handleACSourceAttributes(Stack<String> attributes) throws Exception
    {
        if(attributes != null && attributes.size() == 3)
        {
            try {
                String attribute = attributes.pop();
                double value = Double.parseDouble(attribute);
                primaryValue = value;
                enteredPrimaryValue = "" + 1/siToValue.get(siPrefix) * primaryValue;
                
                attribute = attributes.pop();
                value = Double.parseDouble(attribute);
                frequency = value;
                enteredFrequency = "" + frequency;
                
                attribute = attributes.pop();
                value = Double.parseDouble(attribute);
                phaseShift = value;
                enteredPhaseShift = "" + phaseShift;
            }
            catch (NumberFormatException nfe)
            {
                primaryValue = frequency = phaseShift = null;
                enteredPrimaryValue = enteredFrequency = enteredPhaseShift = "";
            }
        }
        else
        {
            throw new Exception("Wrong number of attibutes.");
        }
    }

    public String toString()
    {
        return this.getClass().getSimpleName() + "_" + componentUID + " " + this.headTerminal.getX() + " " + this.headTerminal.getY()+ " " + this.tailTerminal.getX() + " " + this.tailTerminal.getY();
    }
}
