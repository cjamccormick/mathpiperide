package org.mathpiper.ui.gui.worksheets;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.mathpiper.interpreters.EvaluationResponse;
import org.mathpiper.interpreters.ResponseListener;
import org.mathpiper.interpreters.ResponseProvider;
import org.mathpiper.lisp.Environment;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;
import org.mathpiper.ui.gui.applications.voscilloscope.oscilloscope.Signal;

import org.mathpiper.ui.gui.worksheets.symbolboxes.Bounds;

public class PlotPanel extends JPanel implements ViewPanel, MouseListener, MouseWheelListener, MouseMotionListener, ResponseProvider
{
    PhaseDialPanel vectorViewer;
    protected double viewScale = 1;
    private boolean paintedOnce = false;
    private int largestX = 0;
    private int largestY = 0;
    private Map options;
    private boolean metaData = false;
    private Map<String, GraphicObject> graphicObjects = new HashMap<String, GraphicObject>();
    private int fontSize = 6;

    private int boundsWidthPixels = 600;
    private int boundsHeightPixels = 400;

    GraphicObject pressedGraphicObject = null;

    //private String testPlot = "pensize 2.0  pencolor 64 64 128  lines2d 25 -5 25  -4.583333333 21.0069444413889  -4.1666666665 17.3611111097222  -3.750000000 14.0625000000000  -3.333333333 11.1111111088889  -2.916666666 8.50694444055556  -2.4999999995 6.24999999750000  -2.083333333 4.34027777638889  -1.666666666 2.77777777555556  -1.249999999 1.56249999750000  -0.8333333325 0.694444443055556  -0.4166666658 0.173611110388889  0.000000001 0.000000000000000001  0.4166666678 0.173611112055556  0.8333333345 0.694444446388889  1.250000001 1.56250000250000  1.666666668 2.77777778222222  2.083333335 4.34027778472222  2.5000000015 6.25000000750000  2.916666668 8.50694445222222  3.333333335 11.1111111222222  3.750000002 14.0625000150000  4.1666666685 17.3611111263889  4.583333335 21.0069444597222  5.000000002 25.0000000200000 ";

    private List<ResponseListener> responseListeners;

    private boolean isMouseClickedHandlerStillProcessing = false;


    private double xGraphMin;
    private double yGraphMin;
    private double xGraphMax;
    private double yGraphMax;
    private double xGraphMaxInitial;
    private double xGraphMinInitial;
    private double yGraphMaxInitial;
    private double yGraphMinInitial;

    private double channel1Phase = 0;

    private String iCallList;
    private String execList;
    private String token;
    private int xLeftPixels = -1;
    private int yTopPixels = -1;
    private int widthPixels = -1;
    private int heightPixels = -1;
    private int axesFontHeight = 12;
    private FontMetrics fontMetrics = null;
    private int exampleWidthPixels = 20; //48; // todo:tk.

    // Can control the space above and to the left of the graph with these variables.
    private int xLeftPixelsPadding = 0;
    private int yTopPixelsPadding = 0;

    private Point mousePt;

    private boolean isSnapToInteger = false;

    private boolean isShowSignals = false;
    private Signal channe1;
    private Signal channe2;
    
    private File img;
    private Image customCursor;
    private Cursor gripHand;
    

    public void defaultXY()
    {
        // Controls the grid.
        xGraphMin = -.25;
        yGraphMin = -.25;

        xGraphMax = 10.0;
        yGraphMax = 10.0 * boundsHeightPixels / boundsWidthPixels;

        xGraphMaxInitial = xGraphMax;
        xGraphMinInitial = xGraphMin;

        yGraphMaxInitial = yGraphMax;
        yGraphMinInitial = yGraphMin;
    }


    public PlotPanel(Environment aEnvironment, int aStackTop, double viewScale, Map options)
    {
        super();

        //iCallList = testPlot;

        responseListeners = new ArrayList<ResponseListener>();

        this.options = options;

        if (options.containsKey("MetaData"))
        {
            metaData = (Boolean) options.get("MetaData");
        }

        if (options.containsKey("Width"))
        {
            boundsWidthPixels = ((Double) options.get("Width")).intValue();
        }

        if (options.containsKey("Height"))
        {
            boundsHeightPixels = ((Double) options.get("Width")).intValue();
        }

        if (options.containsKey("SnapToInteger?"))
        {
            isSnapToInteger = ((boolean) options.get("SnapToInteger?"));
        }

        if (options.containsKey("ShowSignalPhase?") && ((boolean) options.get("ShowSignalPhase?")) == true)
        {
            //channe1 = Signal.generateSinusodial(5, 0, 500, 100, 1, 0);
            channe1 = Signal.generateSinusodial(5, 0, 200, 100, 2, channel1Phase);
            channe2 = Signal.generateSinusodial(5, 0, 200, 100, 2, 0);
            JFrame f = new JFrame();
            f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            f.setSize(400, 400);
            vectorViewer = new PhaseDialPanel();
            f.add(vectorViewer, BorderLayout.CENTER);
            f.setVisible(true);
            isShowSignals = true;
        }

        /*
        xGraphMin = 1e200;
        yGraphMin = 1e200;
        xGraphMax = -xGraphMin;
        yGraphMax = -yGraphMin;
        */
        MediaTracker tracker = new MediaTracker(this);
        
        try
        {
            customCursor = ImageIO.read(getClass().getClassLoader().getResourceAsStream("org/mathpiper/ui/gui/cursors/grip_cursor.png"));
            tracker.waitForAll();
            gripHand = Toolkit.getDefaultToolkit().createCustomCursor(customCursor, new Point(0,0), "gripHand");
        }
        catch(IOException | InterruptedException ex)
        {
            System.out.println(ex);
            gripHand = new Cursor(Cursor.MOVE_CURSOR);
        }
        
        defaultXY();

        runCallList(null);

        this.setOpaque(true);
        this.viewScale = viewScale;
        this.setBackground(Color.white);

        //this.plotPoint("a1", 0.0, 0.0);
        //this.plotPoint("a2", 5.0, 5.0);
        
        
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        this.addMouseWheelListener(this);
    }

    public void plotMeta(String commands)
    {
        iCallList = commands;
        this.repaint();
    }

    public void paint(Graphics g)
    {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;

        /*
        int xInset = getInsets().left;
        int yInset = getInsets().top;
        int boundsWidthPixels = getWidth() - getInsets().left - getInsets().right;
        int boundsHeightPixels = getHeight() - getInsets().top - getInsets().bottom;
        */

        g2d.setColor(Color.white);
        g2d.fillRect(0, 0, getWidth(), getHeight());

        //Uncomment to draw a line around the perimiter of the panel.
        //g2d.setColor(Color.red);
        //g2d.drawRect(0, 0, getWidth() - 1, getHeight() - 1);

        g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));

        g2d.setStroke(new BasicStroke((float) (2), BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        g2d.setColor(Color.black);
        g2d.setBackground(Color.white);

        Rectangle r = g2d.getClipBounds();



        ScaledGraphics sg = new ScaledGraphics(g2d);
        sg.setLineWidth(0);
        sg.setViewScale(viewScale);
        sg.setFontSize(fontSize);



        //grapher.paint(sg, 0, 0, new Dimension(boundsWidthPixels, boundsHeightPixels));
        //-------------------


        Dimension d = new Dimension(boundsWidthPixels, boundsHeightPixels);

        determineGraphBounds(xLeftPixelsPadding, yTopPixelsPadding, d);

        Color grey = new Color(164, 164, 164);

        double x, y;

        // X Axis.

        PlotRange xRange = new PlotRange(xGraphMin, xGraphMax, d.width / ((3 * exampleWidthPixels) / 2));

        int xtick = ((int) (xGraphMin / xRange.tickSize() ));

        if (xRange.tickSize() * xtick < xGraphMin)
        {
            xtick = xtick + 1;
        }

        double xstart = xRange.tickSize() * xtick;

        sg.setColor(Color.black);

        for (x = xstart; x <= xGraphMax; x += xRange.tickSize())
        {
            double xPix = (xLeftPixels + widthPixels * (x - xGraphMin) / (xGraphMax - xGraphMin));
            sg.setColor(grey);
            sg.drawLine(xPix, yTopPixels, xPix, yTopPixels + heightPixels);
            sg.setColor(Color.black);
            String num = xRange.format(xtick);
            //int numWidth = fontMetrics.stringWidth(num);
            double numWidth = sg.getScaledTextWidth(num);
            //g.drawString(num, xPix - numWidth / 2, graphy + graphHeight + fontMetrics.getAscent());
            sg.drawString(num, xPix - numWidth / 2, yTopPixels + heightPixels + sg.getAscent());
            xtick++;
        }

        // Y Axis.

        PlotRange yRange = new PlotRange(yGraphMin, yGraphMax, d.height / (axesFontHeight * 2));

        int ytick = ((int) (yGraphMin / yRange.tickSize()));

        if (yRange.tickSize() * ytick < yGraphMin)
        {
            ytick = ytick + 1;
        }

        double ystart = yRange.tickSize() * ytick;

        for (y = ystart; y <= yGraphMax; y += yRange.tickSize())
        {
            double yPix = (yTopPixels + heightPixels * (yGraphMax - y) / (yGraphMax - yGraphMin));
            sg.setColor(grey);
            sg.drawLine(xLeftPixels, yPix, xLeftPixels + widthPixels, yPix);
            sg.setColor(Color.black);
            String num = yRange.format(ytick);
            //int numWidth = fontMetrics.stringWidth(num);
            double numWidth = sg.getScaledTextWidth(num);
            //g.drawString(num, graphx - numWidth - 8, yPix + fontMetrics.getAscent() - (axesFontHeight) / 2);
            sg.drawString(num, xLeftPixels - numWidth - 8.0, yPix + sg.getAscent() - (axesFontHeight) / 2);
            ytick++;
        }


        sg.setColor(Color.blue);
        sg.drawRectangle(xLeftPixels, yTopPixels, widthPixels, heightPixels);
        sg.setColor(Color.BLACK);

        sg.clip(xLeftPixels, yTopPixels, widthPixels, heightPixels);

        // Origin axes.
        sg.setColor(Color.black);
        double currentLineWidth = sg.getLineWidth();
        sg.setLineWidth(1.5);

        double xOrigin = (xLeftPixels + widthPixels * (0 - xGraphMin) / (xGraphMax - xGraphMin));
        if(xOrigin > xLeftPixels && xOrigin < xLeftPixels + widthPixels)
        {
            sg.drawLine(xOrigin, yTopPixels, xOrigin, yTopPixels + heightPixels);
        }

        double yOrigin = (yTopPixels + heightPixels * (yGraphMax - 0) / (yGraphMax - yGraphMin));
        if(yOrigin > yTopPixels && yOrigin < yTopPixels + heightPixels)
        {
            sg.drawLine(xLeftPixels, yOrigin, xLeftPixels + widthPixels, yOrigin);
        }

        sg.setLineWidth(currentLineWidth);



        runCallList(sg);


        Iterator it = graphicObjects.entrySet().iterator();
        while (it.hasNext())
        {
            Map.Entry pair = (Map.Entry) it.next();

            GraphicObject go = (GraphicObject) pair.getValue();

            go.paint(sg, this);
        }


        if(isShowSignals == true)
        {
            double lineWidth = sg.getLineWidth();
            sg.setLineWidth(1.5);

            sg.setColor(Color.blue);
            for(int index = 0; index < this.channe2.getLength()-1; index++)
            {
                sg.drawLine(toPixelsX(index), toPixelsY(channe2.getVoltage(index)), toPixelsX(index+1), toPixelsY(channe2.getVoltage(index+1)));
            }

            sg.setColor(Color.red);
            for(int index = 0; index < this.channe1.getLength()-1; index++)
            {
                sg.drawLine(toPixelsX(index), toPixelsY(channe1.getVoltage(index)), toPixelsX(index+1), toPixelsY(channe1.getVoltage(index+1)));
            }

            sg.setColor(Color.black);
            sg.setLineWidth(lineWidth);
        }

        g2d.setClip(null);


        if (paintedOnce == false)
        {
            super.revalidate();

            paintedOnce = true;
        }

    }

    public Cursor getGripCursor()
    {
        return gripHand;
    }


    public Dimension getPreferredSize()
    {
        if (paintedOnce)
        {
            Bounds maxBounds = new Bounds(0, boundsHeightPixels, 0, boundsWidthPixels);

            Dimension scaledDimension = maxBounds.getScaledDimension(this.viewScale);

            return scaledDimension;
        }
        else
        {
            return new Dimension(boundsHeightPixels, boundsWidthPixels);
        }

    }

    public void setViewScale(double viewScale)
    {
        this.viewScale = viewScale;
        this.revalidate();
        this.repaint();
    }


    public GraphicObject getObject(String label)
    {
        return graphicObjects.get(label);
    }

    public List getSelectedObjects()
    {
        List<String> selectedList = new ArrayList<String>();

        Iterator it = graphicObjects.entrySet().iterator();
        while (it.hasNext())
        {
            Map.Entry pair = (Map.Entry) it.next();

            GraphicObject go = (GraphicObject) pair.getValue();

            if(go.isSelected())
            {
                selectedList.add("\"" + go.getLabel() + "\"");
            }

        }

        return selectedList;
    }


    public void setColor(String label, int r, int g, int b)
    {
        GraphicObject go = this.getObject(label);

        if(go != null)
        {
            go.setColor(new Color(r,g,b));
            repaint();
        }
    }


    public void showLabel(String label, boolean isLabel)
    {
        GraphicObject go = this.getObject(label);

        if(go != null)
        {
            go.showLabel(isLabel);
            repaint();
        }
    }

    public void setLabel(String oldLabel, String newLabel)
    {
        GraphicObject go = this.getObject(oldLabel);

        if(go != null)
        {
            go.setLabel(newLabel);
            this.graphicObjects.remove(oldLabel);
            this.graphicObjects.put(newLabel, go);
            repaint();
        }
    }


    public void plotPoint(String label, double x, double y)
    {
        GraphicObject go = this.getObject(label);

        if(go == null)
        {
            go = new GraphicObject(label, x, y);
        }
        else
        {
            go.setXCoordinate(x);
            go.setYCoordinate(y);
        }

        graphicObjects.put(label, go);

        this.repaint();
    }



    public void deleteObject(String label)
    {
        GraphicObject go = this.getObject(label);

        if(go != null)
        {
            graphicObjects.remove(label);
            this.repaint();
        }
    }


    public void selectObject(String label, boolean isSelect)
    {
        GraphicObject go = this.getObject(label);

        if(go != null)
        {
            go.select(isSelect);
            this.repaint();
        }
    }


    public void selectObject(int x, int y)
    {
        Iterator it = graphicObjects.entrySet().iterator();
        while (it.hasNext())
        {
            Map.Entry pair = (Map.Entry) it.next();

            GraphicObject go = (GraphicObject) pair.getValue();

            if(go.getXCoordinate() == x && go.getYCoordinate() == y)
            {
                go.select(true);
                this.repaint();
            }
        }
    }


    public void clear()
    {
        graphicObjects.clear();
        this.repaint();
    }

    public double toPixelsX(double xLocationGraph)
    {
        //Scaling is handled in the ScaledGraphics class.
        return (xLeftPixels + widthPixels * (xLocationGraph - xGraphMin) / (xGraphMax - xGraphMin));
    }

    public double toPixelsY(double yLocationGraph)
    {
        //Scaling is handled in the ScaledGraphics class.
        return (yTopPixels + heightPixels * (1.0 - (yLocationGraph - yGraphMin) / (yGraphMax - yGraphMin)));
    }

    public double toGraphX(double xLocationPixels)
    {
        double scaledXLocationPixels = xLocationPixels/viewScale;
        double xLocationGraph = (widthPixels*xGraphMin + scaledXLocationPixels*xGraphMax - scaledXLocationPixels*xGraphMin - xGraphMax*xLeftPixels + xGraphMin*xLeftPixels)/widthPixels;
        return xLocationGraph;
    }

    public double toGraphY(double yLocationPixels)
    {
        double scaledYLocationPixels = yLocationPixels/viewScale;
        double yLocationGraph = (heightPixels*yGraphMax - yGraphMax*scaledYLocationPixels + yGraphMax*yTopPixels + yGraphMin*scaledYLocationPixels - yGraphMin*yTopPixels)/heightPixels;
        return yLocationGraph;
    }



        void nextToken()
    {
        int startPos = 0;
        while (startPos < execList.length() && execList.charAt(startPos) == ' ')
        {
            startPos++;
        }
        int endPos = startPos;
        while (endPos < execList.length() && execList.charAt(endPos) != ' ')
        {
            endPos++;
        }
        token = execList.substring(startPos, endPos);
        execList = execList.substring(endPos);
    }

    void runCallList(ScaledGraphics g)
    {
        try
        {

            /*
             Graphics2D g2d = null;
             if (g != null) {
             if (g instanceof Graphics2D) {
             g2d = (Graphics2D) g;
             }
             }
             */
            execList = iCallList;
            nextToken();
            while (token.length() > 0)
            {
                if (token.equals("lines2d"))
                {
                    int i;

                    nextToken();

                    int numberOfPoints = Integer.parseInt(token);

                    nextToken();

                    double x2, y2 = 0;

                    x2 = Float.parseFloat(token);

                    nextToken();

                    y2 = Float.parseFloat(token);

                    if (g == null)
                    {
                        if (xGraphMin > x2)
                        {
                            xGraphMin = x2;
                        }
                        if (xGraphMax < x2)
                        {
                            xGraphMax = x2;
                        }
                        if (yGraphMin > y2)
                        {
                            yGraphMin = y2;
                        }
                        if (yGraphMax < y2)
                        {
                            yGraphMax = y2;
                        }
                    }

                    double x1, y1;

                    for (i = 1; i < numberOfPoints; i++)
                    {
                        x1 = x2;
                        y1 = y2;
                        nextToken();
                        x2 = Float.parseFloat(token);
                        nextToken();
                        y2 = Float.parseFloat(token);
                        if (g == null)
                        {
                            if (xGraphMin > x2)
                            {
                                xGraphMin = x2;
                            }
                            if (xGraphMax < x2)
                            {
                                xGraphMax = x2;
                            }
                            if (yGraphMin > y2)
                            {
                                yGraphMin = y2;
                            }
                            if (yGraphMax < y2)
                            {
                                yGraphMax = y2;
                            }
                        }
                        if (g != null)
                        {
                            int xPix1 = (int) (xLeftPixels + widthPixels * (x1 - xGraphMin) / (xGraphMax - xGraphMin));
                            int yPix1 = (int) (yTopPixels + heightPixels * (1.0 - (y1 - yGraphMin) / (yGraphMax - yGraphMin)));
                            int xPix2 = (int) (xLeftPixels + widthPixels * (x2 - xGraphMin) / (xGraphMax - xGraphMin));
                            int yPix2 = (int) (yTopPixels + heightPixels * (1.0 - (y2 - yGraphMin) / (yGraphMax - yGraphMin)));
                            g.drawLine(xPix1, yPix1, xPix2, yPix2);
                        }
                    }
                }
                else if (token.equals("pencolor"))
                {
                    nextToken();
                    int red = Integer.parseInt(token);
                    nextToken();
                    int green = Integer.parseInt(token);
                    nextToken();
                    int blue = Integer.parseInt(token);
                    if (g != null)
                    {
                        g.setColor(new Color(red, green, blue));
                    }
                }
                else if (token.equals("pensize"))
                {
                    nextToken();
                    float width = Float.parseFloat(token);
                    g.setLineWidth(width);
                }
                else
                {
                    //TODO raise an exception here
                    return;
                }
                nextToken();
            }
        } catch (Throwable e)
        {
            //TODO handle exception here
        }
    }




    void determineGraphBounds(int xleft, int ytop, Dimension d)
    {
        if (fontMetrics != null)
        {
            exampleWidthPixels = fontMetrics.stringWidth("100000");
        }
        xLeftPixels = xleft + exampleWidthPixels;
        yTopPixels = ytop + axesFontHeight;
        widthPixels = d.width - (3 * exampleWidthPixels) / 2;
        heightPixels = d.height - 3 * axesFontHeight;
    }


    private GraphicObject getPressedGraphicObject(MouseEvent me)
    {
        GraphicObject pressedGraphicObject = null;

        int x = me.getX();
        int y = me.getY();

        double mouseXGraph = toGraphX(x);

        double mouseYGraph = toGraphY(y);

        double xPointPerPixel = toGraphX(1) - toGraphX(0);
        double yPointPerPixel = toGraphY(0) - toGraphY(1);

        Iterator it = graphicObjects.entrySet().iterator();

        while (it.hasNext())
        {
            Map.Entry<String, GraphicObject> pair = (Map.Entry) it.next();

            GraphicObject graphicObject = pair.getValue();

            double objectX = graphicObject.getXCoordinate();
            double objectY = graphicObject.getYCoordinate();

            double xSelectRadius = xPointPerPixel * graphicObject.getDiameter() / 2 * viewScale;
            double ySelectRadius = yPointPerPixel * graphicObject.getDiameter() / 2 * viewScale;


            //System.out.println("OBJ: " + graphicObject.getLabel() + ", " + objectX + ", " + objectY + ", " + (mouseXGraph - xPointPerPixel) + ", " + (mouseXGraph + xPointPerPixel));

            //System.out.println("AAA: " + mouseXGraph + ", " + mouseYGraph);

            if (objectX >= mouseXGraph - xSelectRadius && objectX <= mouseXGraph + xSelectRadius && objectY >= mouseYGraph - ySelectRadius && objectY <= mouseYGraph + ySelectRadius) {
                pressedGraphicObject = graphicObject;
                break;
            }
        }

        return pressedGraphicObject;
    }



    public void mouseClicked(MouseEvent me)
    {

        if (isMouseClickedHandlerStillProcessing)
        {
            return;
        }

        isMouseClickedHandlerStillProcessing = true;


        GraphicObject clickedGraphicObject = getPressedGraphicObject(me);

        if(clickedGraphicObject != null)
        {
            clickedGraphicObject.toggleSelected();
            repaint();
        }

        isMouseClickedHandlerStillProcessing = false;
    }

    public void mousePressed(MouseEvent me)
    {
        mousePt = me.getPoint();
        pressedGraphicObject = getPressedGraphicObject(me);
        
        this.setCursor(getGripCursor());
    }

    public void mouseReleased(MouseEvent me)
    {
        this.setCursor(Cursor.getDefaultCursor());
        
        if(pressedGraphicObject != null && this.isSnapToInteger)
        {
            pressedGraphicObject.snapToInteger();
            repaint();
        }

        pressedGraphicObject = null;
    }

    public void mouseEntered(MouseEvent me)
    {
    }

    public void mouseExited(MouseEvent me)
    {
    }

    public void addResponseListener(ResponseListener listener)
    {
        responseListeners.add(listener);
    }

    public void removeResponseListener(ResponseListener listener)
    {
        responseListeners.remove(listener);
    }

    protected void notifyListeners(EvaluationResponse response)
    {

        for (ResponseListener listener : responseListeners)
        {
            listener.response(response);
        }//end for.

    }



    /*
     * Determine the ticks of the graph. The calling routine
     * should first determine the minimum and maximum values, and
     * the number of steps (based on size of the axis to draw
     * relative to font size).
     *
     * Steps will always be m*10^n, for some suitable n, with m either 1, 2 or 5.
     */
    class PlotRange
    {
        private double iMinValue;
        private double iMaxValue;
        private int iMaxSteps;

        private int iN;
        private int iStep;

        public PlotRange(double aMinValue, double aMaxValue, int aMaxSteps)
        {
            iMinValue = aMinValue;
            iMaxValue = aMaxValue;
            iMaxSteps = aMaxSteps;
            
            //customCursor = toolkit.getImage("../Cursors/grip_cursor.png");
            //gripHand = toolkit.createCustomCursor(customCursor, new Point(0,0), "gripHand");

            //TODO handle zero length range
            double range = aMaxValue - aMinValue;

            iN = (int) (Math.log(range) / Math.log(10) - 1);

            iN = iN - 1;

            iStep = 1;

            for (;;)
            {
                double tickSize = tickSize();
                int nrSteps = (int) (range / tickSize);
                if (nrSteps <= aMaxSteps)
                {
                    break;
                }
                switch (iStep)
                {
                    case 1:
                        iStep = 2; // 2; // todo:tk.
                        break;
                    case 2:
                        iStep = 5;
                        break;
                    case 5:
                        iN++;
                        iStep = 1;
                        break;
                }
            }
            
            
        }
        
        public Cursor getCursor()
        {
            return gripHand;
        }

        public double tickSize()
        {
            return iStep * Math.pow(10, iN);
        }

        public String format(int tick)
        {
            String result = "";

            int fct = tick * iStep;

            if (iN >= 0 && iN < 3)
            {
                if (iN > 0)
                {
                    fct = fct * 10;
                }

                if (iN > 1)
                {
                    fct = fct * 10;
                }

                result = "" + fct;
            }
            else
            {
                int n = iN;
                if (fct == 10 * (fct / 10))
                {
                    fct /= 10;
                    n += 1;
                }
                String ex = "";
                if (n != 0 && tick != 0)
                {
                    ex = "e" + n;
                }
                result = "" + fct + ex;
            }
            return result;
        }

    }


    public void mouseWheelMoved(MouseWheelEvent e) {
       int notches = e.getWheelRotation();
       
       if(notches > 0)
       {
           xGraphMin -= .5;
           xGraphMax += .5;
           yGraphMin -= .5;
           yGraphMax += .5;
       }
       else
       {
           xGraphMin += .5;
           xGraphMax -= .5;
           yGraphMin += .5;
           yGraphMax -= .5;
       }
       this.repaint();
    }

    public void mouseMoved(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
        
        int dxPixels = e.getX() - mousePt.x;
        int dyPixels = e.getY() - mousePt.y;

        double dxGraph = (dxPixels * ((xGraphMax - xGraphMin) / widthPixels)) / viewScale;
        double dyGraph = (dyPixels * ((yGraphMax - yGraphMin) / heightPixels)) / viewScale;

        double dxGraphInitial = (dxPixels * ((xGraphMaxInitial - xGraphMinInitial) / widthPixels)) / viewScale;
        double dyGraphInitial = (dyPixels * ((yGraphMaxInitial - yGraphMinInitial) / heightPixels)) / viewScale;

        if(pressedGraphicObject != null)
        {
            pressedGraphicObject.move(dxGraph, -dyGraph);
        }
        else
        {
            xGraphMin -= dxGraph;
            xGraphMax -= dxGraph;
            this.xGraphMaxInitial -= dxGraphInitial;
            this.xGraphMinInitial -= dxGraphInitial;

            yGraphMin += dyGraph;
            yGraphMax += dyGraph;
            this.yGraphMaxInitial += dyGraphInitial;
            this.yGraphMinInitial += dyGraphInitial;
        }

        mousePt = e.getPoint();
        repaint();
    }

    public double getXGraphMin() {
        return xGraphMin;
    }

    public void setXGraphMin(double xGraphMin) {
        this.xGraphMin = xGraphMin;
    }

    public double getYGraphMin() {
        return yGraphMin;
    }

    public void setYGraphMin(double yGraphMin) {
        this.yGraphMin = yGraphMin;
    }

    public double getXGraphMax() {
        return xGraphMax;
    }

    public void setXGraphMax(double xGraphMax) {
        this.xGraphMax = xGraphMax;
    }

    public double getYGraphMax() {
        return yGraphMax;
    }

    public void setYGraphMax(double yGraphMax) {
        this.yGraphMax = yGraphMax;
    }

    public double getXGraphMaxInitial() {
        return xGraphMaxInitial;
    }

    public void setXGraphMaxInitial(double xGraphMaxInitial) {
        this.xGraphMaxInitial = xGraphMaxInitial;
    }

    public double getXGraphMinInitial() {
        return xGraphMinInitial;
    }

    public void setXGraphMinInitial(double xGraphMinInitial) {
        this.xGraphMinInitial = xGraphMinInitial;
    }

    public double getYGraphMaxInitial() {
        return yGraphMaxInitial;
    }

    public void setYGraphMaxInitial(double yGraphMaxInitial) {
        this.yGraphMaxInitial = yGraphMaxInitial;
    }

    public double getYGraphMinInitial() {
        return yGraphMinInitial;
    }

    public void setYGraphMinInitial(double yGraphMinInitial) {
        this.yGraphMinInitial = yGraphMinInitial;
    }

    public void setChannel1Phase(double newPhase)
    {
        channel1Phase = newPhase;
        channe1 = Signal.generateSinusodial(5, 0, 200, 100, 2, channel1Phase);
        if(vectorViewer != null)
        {
            this.vectorViewer.setAngle1(-newPhase);
        }
        repaint();
    }

    public void resetGraph()
    {
        defaultXY();
        this.setXGraphMin(xGraphMinInitial);
        this.setXGraphMax(xGraphMaxInitial);
        this.setYGraphMin(yGraphMinInitial);
        this.setYGraphMax(yGraphMaxInitial);
    }

    public void getCenter()
    {
        boolean isInit = false;

        double  xMax = 0.0,
                yMax = 0.0,
                xMin = 0.0,
                yMin = 0.0;

        Iterator it = graphicObjects.entrySet().iterator();
        while(it.hasNext())
        {
            Map.Entry<String, GraphicObject> pair = (Map.Entry) it.next();

            GraphicObject graphicObject = pair.getValue();

            double tmpx = graphicObject.getXCoordinate(),
                   tmpy = graphicObject.getYCoordinate();

            if(!isInit)
            {
                xMax = tmpx;
                xMin = tmpx;
                yMax = tmpy;
                yMin = tmpy;
                isInit = true;
            }

            if(xMax < tmpx)
                xMax = tmpx;
            else if(xMin > tmpx)
                xMin = tmpx;

            if(yMax < tmpy)
                yMax = tmpy;
            else if(yMin > tmpy)
                yMin = tmpy;
        }

        this.setXGraphMin(xMin - 1.0);
        this.setXGraphMax(xMax + 1.0);
        this.setYGraphMin(yMin - 1.0);
        this.setYGraphMax(yMax + 1.0);
    }


    public static void main(String[] args)
    {
            Map options = new HashMap();
            options.put("metaData", false);
            options.put("Scale", 1.5);

            double viewScale = ((Double) options.get("Scale")).doubleValue();

            JFrame frame = new JFrame();
            Container contentPane = frame.getContentPane();
            frame.setBackground(Color.WHITE);
            contentPane.setBackground(Color.WHITE);

            final org.mathpiper.ui.gui.worksheets.PlotPanel plotPanel = new org.mathpiper.ui.gui.worksheets.PlotPanel(null, -1, viewScale, options);

            GraphPanelController mathPanelScaler = new GraphPanelController(plotPanel, viewScale);
            mathPanelScaler.removeScaleSlider();


            JScrollPane scrollPane = new JScrollPane(plotPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
            scrollPane.getVerticalScrollBar().setUnitIncrement(16);

            JPanel mathControllerPanel = new JPanel();
            mathControllerPanel.setLayout(new BorderLayout());
            mathControllerPanel.add(scrollPane);
            mathControllerPanel.add(mathPanelScaler, BorderLayout.NORTH);

            contentPane.add(mathControllerPanel);

            JMenu fileMenu = new JMenu("File");
            JMenuItem saveAsImageAction = new JMenuItem();
            saveAsImageAction.setText("Save As Image");
            saveAsImageAction.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent ae)
                {
                    org.mathpiper.ui.gui.Utility.saveImageOfComponent(plotPanel);
                }
            });
            fileMenu.add(saveAsImageAction);
            JMenuBar menuBar = new JMenuBar();
            menuBar.add(fileMenu);
            frame.setJMenuBar(menuBar);

            plotPanel.plotPoint("A", 1, 5);



            //frame.setAlwaysOnTop(false);
            frame.setTitle("Plot Viewer");
            frame.setResizable(true);
            frame.pack();
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int height = screenSize.height;
            int width = screenSize.width;
            frame.setSize(width/2, height/2);
            frame.setLocationRelativeTo(null);
            
            frame.addComponentListener(new ComponentAdapter() 
            {
                    public void componentResized(ComponentEvent e) {
                        Dimension dim = ((JFrame) e.getSource()).getSize();
                        Double scale = (double)dim.height;
                        plotPanel.setViewScale(scale / 500);
                    }
            });

            frame.setVisible(true);
            frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
    }

}
