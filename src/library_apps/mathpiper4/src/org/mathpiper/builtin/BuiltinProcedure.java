/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin;

import org.mathpiper.builtin.procedures.core.*;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.unparsers.MathPiperUnparser;

import org.mathpiper.lisp.cons.Cons;

public abstract class BuiltinProcedure {

    protected String functionName = "";

    public abstract void evaluate(Environment aEnvironment, int aStackTop) throws Throwable;

    public static Cons getTopOfStack(Environment aEnvironment, int aStackTop) throws Throwable {
        return aEnvironment.iArgumentStack.getElement(aStackTop, aStackTop, aEnvironment);
    }

    public static void setTopOfStack(Environment aEnvironment, int aStackTop, Cons cons) throws Throwable {
        aEnvironment.iArgumentStack.setElement(aStackTop, aStackTop, aEnvironment, cons);
    }

    public static Cons getArgument(Environment aEnvironment, int aStackTop, int argumentPosition) throws Throwable {
        return aEnvironment.iArgumentStack.getElement(aStackTop + argumentPosition, aStackTop, aEnvironment);
    }

    public static Cons getArgument(Environment aEnvironment, int aStackTop, Cons cur, int n) throws Throwable {
        if(n < 0)LispError.lispAssert(aEnvironment, aStackTop);

        Cons loop = cur;
        while (n != 0) {
            n--;
            loop = loop.cdr();
        }
        return loop;
    }

    public void plugIn(Environment aEnvironment) throws Throwable {
    }//end method.

    public static void addCoreFunctions(Environment aEnvironment) throws Throwable {
        String functionNameInit = "";


        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "While");
        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "DoWhile");
        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "If");
        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "For");
        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "Until");
        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "RuleHoldArguments");
        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "RuleEvaluateArguments");
        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "RulePatternHoldArguments");
        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "RulePatternEvaluateArguments");
        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "PipeFromFile");
        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "PipeFromString");
        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "PipeToFile");
        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "PipeToString");
        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "PipeToStdout");
        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "TraceRule");
        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "Substitute");
        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "LocalSymbols");
        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "BackQuote");
        
        aEnvironment.iPrefixOperators.setOperator(0, "`", " ", "");
        aEnvironment.iPrefixOperators.setOperator(0, "'", " ", "");
        aEnvironment.iPrefixOperators.setOperator(21000, "''", " ", "");
        aEnvironment.iPrefixOperators.setOperator(0, "@");
        aEnvironment.iInfixOperators.setOperator(21000, "::", "", "");

        functionNameInit = "AbsN"; aEnvironment.iBuiltinFunctions.put( functionNameInit, new BuiltinProcedureEvaluator(new Abs(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "AddN"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Add(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        BuiltinProcedureEvaluator function = new BuiltinProcedureEvaluator(new And_(functionNameInit), 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments); //Alias.
        functionNameInit = "And?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, function); //Alias.
        functionNameInit = "&?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, function); //Alias.
        functionNameInit = "ApplyFast"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ApplyFast(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ArcCosD"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ArcCosD(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ArcSinD"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ArcSinD(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ArcTanD"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ArcTanD(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ArrayCreate"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ArrayCreate(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ArrayGet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ArrayGet(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ArraySet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ArraySet(functionNameInit), 3, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ArraySize"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ArraySize(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Assign"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Assign(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "AssignGlobalLazy"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new AssignGlobalLazy(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "Atom?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Atom_(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "`"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new BackQuote(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "BitAnd"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new BitAnd(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "BitCount"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new BitCount(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "BitNot"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new BitNot(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "BitOr"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new BitOr(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "BitXor"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new BitXor(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "BitsToDigits"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new BitsToDigits(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Bodied"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Bodied(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Bodied?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Bodied_(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Assigned?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Assigned_(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "Break"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Break(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "BuiltinAssociation"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new BuiltinAssociation(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "BuiltinPrecisionGet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new BuiltinPrecisionGet(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "BuiltinPrecisionSet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new BuiltinPrecisionSet(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Car"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Car(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Cdr"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Cdr(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "CeilN"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Ceil(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Check"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Check(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "CommonLispTokenizer"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new CommonLispTokenizer(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Concat"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Concatenate(functionNameInit), 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ConcatStrings"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ConcatenateStrings(functionNameInit), 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Constant"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Constant(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Continue"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Continue(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "CosD"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new CosD(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "CurrentFile"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new CurrentFile(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "CurrentLine"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new CurrentLine(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "CustomEval"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new CustomEval(functionNameInit), 4, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "CustomEvalExpression"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new CustomEvalExpression(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "CustomEvalLocals"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new CustomEvalLocals(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "CustomEvalResult"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new CustomEvalResult(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "CustomEvalStop"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new CustomEvalStop(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "DebugFile"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new DebugFile(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "DebugLine"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new DebugLine(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "DebugMode?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new DebugMode_(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Decide"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Decide(functionNameInit), 2, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "Decimal?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Decimal_(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "MacroRulebaseHoldArguments"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new MacroRulebaseHoldArguments(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "MacroRulebaseListedHoldArguments"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new MacroRulebaseListedHoldArguments(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "DefaultDirectory"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new DefaultDirectory(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "DefaultTokenizer"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new DefaultTokenizer(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "DefinedConstant?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new DefinedConstant_(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Delete"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Delete(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Delete!"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Delete__(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Insert!"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Insert__(functionNameInit), 3, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Replace!"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Replace__(functionNameInit), 3, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Reverse!"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Reverse__(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "DigitsToBits"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new DigitsToBits(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "DivideN"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Divide(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "DoWhile"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new DoWhile(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "DumpNumber"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new DumpNumber(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "EditDistance"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new EditDistance(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Equal?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Equal_(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "=?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Equal_(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments)); //Alias.
        function = new BuiltinProcedureEvaluator(new Equivales_(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments);
        functionNameInit = "Equivales?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, function);
        functionNameInit = "==?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, function);
        functionNameInit = "Eval"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Eval(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ExceptionCatch"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ExceptionCatch(functionNameInit), 3, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "ExceptionGet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ExceptionGet(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ExceptionThrow"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ExceptionThrow(functionNameInit), 2, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "IsExitRequested"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ExitRequested(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "MathFac"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Factorial(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "FastIsPrime"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new FastIsPrime(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "FastLog"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new FastLog(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "FastPower"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new FastPower(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "FindProcedure"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new FindProcedure(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "First"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new First(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "FlatCopy"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new FlatCopy(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "FloorN"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Floor(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "For"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new For(functionNameInit), 4, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "FromBase"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new FromBase(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "GcdN"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Gcd(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "GenericTypeName"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new GenericTypeName(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Generic?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Generic_(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "GetExactBitsN"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new GetExactBits(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "GreaterThan?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new GreaterThan_(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "HistorySize"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new HistorySize(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Hold"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Hold(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "'"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Hold(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "''"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Freeze(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "HoldArgument"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new HoldArgument(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        function = new BuiltinProcedureEvaluator(new Implies_(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments);
        functionNameInit = "If"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new If(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "Implies?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, function);
        functionNameInit = "->?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, function);
        functionNameInit = "Infix"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Infix(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Infix?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Infix_(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Insert"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Insert(functionNameInit), 3, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Integer?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Integer_(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "LeftPrecedenceGet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new LeftPrecedenceGet(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "LeftPrecedenceSet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new LeftPrecedenceSet(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Length"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Length(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "LessThan?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new LessThan_(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "LibraryLock"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new LibraryLock(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "List"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new List(functionNameInit), 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "ListToProcedure"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ListToProcedure(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ListToStringInternal"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ListToStringInternal(functionNameInit, aEnvironment), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "List?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new List_(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "LoadScript"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new LoadScript(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "LoadLibraryProcedure"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new LoadLibraryProcedure(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Local"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Local(functionNameInit), 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "LocalSymbols"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new LocalSymbols(functionNameInit), 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "Log10D"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Log10D(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "MacroLocal"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Local(functionNameInit), 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "MacroAssign"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new MacroAssign(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "ParseMathPiper"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ParseMathPiper(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "RuleEvaluateArguments"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new RuleEvaluateArguments(functionNameInit), 5, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "RulePatternEvaluateArguments"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new RulePatternEvaluateArguments(functionNameInit), 5, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "RulebaseEvaluateArguments"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new RulebaseEvaluateArguments(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "RulebaseListedEvaluateArguments"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new RulebaseListedEvaluateArguments(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "MathIsSmall"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new MathIsSmall(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "MathNegate"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new MathNegate(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "MathSign"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new MathSign(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "MaxEvalDepth"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new MaxEvalDepth(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "MetaClear"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new MetaClear(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "MetaEntries"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new MetaEntries(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "MetaGet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new MetaGet(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "MetaKeys"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new MetaKeys(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "MetaSet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new MetaSet(functionNameInit), 3, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "MetaValues"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new MetaValues(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ModuloN"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Modulo(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "MultiplyN"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Multiply(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        function = new BuiltinProcedureEvaluator(new Not_(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments);
        functionNameInit = "Not?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, function); //Alias.
        functionNameInit = "!?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, function); //Alias.
        functionNameInit = "MathNth"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Nth(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Number?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Number_(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        function = new BuiltinProcedureEvaluator(new Or_(functionNameInit), 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments); //Alias.
        functionNameInit = "OperatorSpacesSet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new OperatorSpacesSet(functionNameInit), 4, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Or?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, function); //Alias.
        functionNameInit = "|?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, function); //Alias.
        functionNameInit = "ParseLisp"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ParseLisp(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ParseLispListed"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ParseLispListed(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ParserGet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ParserGet(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ParserSet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ParserSet(functionNameInit), 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "PatchString"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new PatchString(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "PatternCreate"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new PatternCreate(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "PatternMatch?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new PatternMatch_(functionNameInit), 3, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "PipeFromString"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new PipeFromString(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "PipeToStdout"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new PipeToStdout(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "PipeToString"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new PipeToString(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "Postfix"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Postfix(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Postfix?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Postfix_(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "PrecedenceGet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new PrecedenceGet(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Prefix"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Prefix(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Prefix?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Prefix_(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Prepend"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Prepend(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Prepend!"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Prepend__(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Procedure?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Procedure_(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ProcedureToList"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ProcedureToList(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "UnparserGet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new UnparserGet(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "UnparserSet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new UnparserSet(functionNameInit), 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "PromptShown?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new PromptShown_(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "QuotientN"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Quotient(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ParseMathPiperToken"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ParseMathPiperToken(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Replace"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Replace(functionNameInit), 3, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Rest"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Rest(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Retract"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Retract(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "RightAssociativeSet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new RightAssociativeSet(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "RightPrecedenceGet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new RightPrecedenceGet(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "RightPrecedenceSet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new RightPrecedenceSet(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "RoundToN"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new RoundToN(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "RuleHoldArguments"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new RuleHoldArguments(functionNameInit), 5, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "RulePatternHoldArguments"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new RulePatternHoldArguments(functionNameInit), 5, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "RulebaseHoldArguments"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new RulebaseHoldArguments(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "RulebaseArgumentsList"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new RulebaseArgumentsList(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "RulebaseDefined"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new RulebaseDefined(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "RulebaseDump"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new RulebaseDump(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "RulebaseListedHoldArguments"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new RulebaseListedHoldArguments(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "Sequence"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Sequence(functionNameInit), 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "Secure"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Secure(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "SetExactBitsN"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new SetExactBits(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ShiftLeft"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ShiftLeft(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ShiftRight"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ShiftRight(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "SinD"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new SinD(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "SqrtD"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new SqrtD(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "StaSiz"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new StackSize(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "StackTrace"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new StackTrace(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "StackTraceOff"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new StackTraceOff(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "StackTraceOn"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new StackTraceOn(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "State"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new State(functionNameInit), 0, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "StringCompareNatural"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new StringCompareNatural(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "StringMidGet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new StringMidGet(functionNameInit), 3, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "StringMidSet"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new StringMidSet(functionNameInit), 3, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "StringToUnicode"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new StringToUnicode(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "String?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new String_(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Substitute"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Substitute(functionNameInit), 3, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "SubtractN"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Subtract(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Symbols"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Symbols(functionNameInit), 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "MetaToObject"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new MetaToObject(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "SysOut"; aEnvironment.iBuiltinFunctions.put( functionNameInit, new BuiltinProcedureEvaluator(new SysOut(functionNameInit), 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "SystemTimer"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new SystemTimer(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "TanD"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new TanD(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Time"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Time(aEnvironment, functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "ToAtom"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ToAtom(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ToBase"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ToBase(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "ToString"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ToString(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "TraceExcept"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new TraceExcept(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, functionNameInit);
        functionNameInit = "TraceOff"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new TraceOff(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "TraceOn"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new TraceOn(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "TraceRule"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new TraceRule(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "TraceSome"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new TraceSome(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, functionNameInit);
        functionNameInit = "TraceStack"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new TraceStack(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "TreeProcess"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new TreeProcess(functionNameInit), 3, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Type"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new Type(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "UnFence"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new UnFence(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Unassign"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new Unassign(functionNameInit), 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "MacroUnassign"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new Unassign(functionNameInit), 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "UnderscoreConstant?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new UnderscoreConstant_(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "UnicodeToString"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new UnicodeToString(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "UnparseLisp"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new UnparseLisp(functionNameInit), 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "UnparseMathPiper"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new UnparseMathPiper(functionNameInit), 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Until"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new Until(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "ObjectToMeta"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new ObjectToMeta(functionNameInit), 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Variable?"; aEnvironment.iBuiltinFunctions.put(functionNameInit, new BuiltinProcedureEvaluator(new Variable_(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "Version"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new Version(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "While"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new While(functionNameInit), 2, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.HoldArguments));
        functionNameInit = "Write"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new Write(functionNameInit), 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "WriteString"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new WriteString(functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "XmlExplodeTag"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new XmlExplodeTag(aEnvironment, functionNameInit), 1, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
        functionNameInit = "XmlTokenizer"; aEnvironment.getBuiltinFunctions().put(functionNameInit, new BuiltinProcedureEvaluator(new XmlTokenizer(functionNameInit), 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));


        /*name = "Return"; aEnvironment.getBuiltinFunctions().put(new BuiltinFunctionEvaluator(new org.mathpiper.builtin.functions.core.Return(), 1, BuiltinFunctionEvaluator.Fixed | BuiltinFunctionEvaluator.Function), name);*/



    }//end method.
}//end class.

