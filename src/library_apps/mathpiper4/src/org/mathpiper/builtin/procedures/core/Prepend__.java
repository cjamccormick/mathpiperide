/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;


public class Prepend__ extends BuiltinProcedure
{

    private Prepend__()
    {
    }

    public Prepend__(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Cons list = getArgument(aEnvironment, aStackTop, 1);
        
        if(! Utility.isList(list)) LispError.throwError(aEnvironment, aStackTop, "The first argument must be a list");
        
        Cons listAtomCons = (Cons) list.car();
        
        Cons element = getArgument(aEnvironment, aStackTop, 2);
        
        element.setCdr(listAtomCons.cdr());
        
        listAtomCons.setCdr(element);
        
        setTopOfStack(aEnvironment, aStackTop, list);
    }
}



/*
%mathpiper_docs,name="Prepend!",categories="Programming Procedures,Lists (Operations)"
*CMD Prepend! --- destructively prepend an expression to a list

*CALL
        Prepend!(list, expr)

*PARMS

{list} -- list to prepend "expr" to

{expr} -- expression to prepend to the list

*DESC

This procedure prepends a value to a list.

Destructive commands run faster than their nondestructive counterparts
because the latter copy the list before they alter it.

*E.G.

In> lst := [_a,_b,_c,_d];
Result: [_a,_b,_c,_d]

In> Prepend!(lst, 1);
Result: [1,_a,_b,_c,_d]

In> lst;
Result: [1,_a,_b,_c,_d]

*SEE Prepend, Append!
%/mathpiper_docs





%mathpiper,name="Prepend!",subtype="automatic_test"

LocalSymbols(list1, list2)
{
    list1 := [_a,_b,_c,_d];
    list2 := Prepend!(list1, 1);
    Verify(list1 =? list2, True);
    Verify(list2, [1,_a,_b,_c,_d]);
}

%/mathpiper
 */
