/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.Operator;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.cons.Cons;

/**
 *
 *  
 */
public class OperatorSpacesSet extends BuiltinProcedure
{

    private OperatorSpacesSet()
    {
    }

    public OperatorSpacesSet(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {              
        Cons arg2 = BuiltinProcedure.getArgument(aEnvironment, aStackTop, 2);
        if( arg2 == null) LispError.checkArgument(aEnvironment, aStackTop, 2);
        LispError.checkIsString(aEnvironment, aStackTop, arg2, aStackTop);
        String xfix = (String) arg2.car();
        if( xfix == null) LispError.checkArgument(aEnvironment, aStackTop, 2);
        xfix = Utility.stripEndQuotesIfPresent(xfix);
        
        Cons arg3 = BuiltinProcedure.getArgument(aEnvironment, aStackTop, 3);
        if( arg3 == null) LispError.checkArgument(aEnvironment, aStackTop, 3);
        LispError.checkIsString(aEnvironment, aStackTop, arg3, aStackTop);
        String leftSpaces = (String) arg3.car();
        if( leftSpaces == null) LispError.checkArgument(aEnvironment, aStackTop, 3);
        
        Cons arg4 = BuiltinProcedure.getArgument(aEnvironment, aStackTop, 4);
        if( arg4 == null) LispError.checkArgument(aEnvironment, aStackTop, 4);
        LispError.checkIsString(aEnvironment, aStackTop, arg4, aStackTop);
        String rightSpaces = (String) arg4.car();
        if( rightSpaces == null) LispError.checkArgument(aEnvironment, aStackTop, 4);
        
        Operator op = null;
        
        if(xfix.equals("Prefix"))
        {
            op = Utility.operatorInfo(aEnvironment, aStackTop, aEnvironment.iPrefixOperators, 1);
            
            if (op == null)
            {
                LispError.throwError(aEnvironment, aStackTop, "The operator < " + op + " > is is not a prefix operator.");
            }
        }
        else if (xfix.equals("Infix"))
        {
            op = Utility.operatorInfo(aEnvironment, aStackTop, aEnvironment.iInfixOperators, 1);
            
            if (op == null)
            {
                LispError.throwError(aEnvironment, aStackTop, "The operator < " + op + " > is is not an infix operator.");
            }          
        }
        else if (xfix.equals("Postfix"))
        {
            op = Utility.operatorInfo(aEnvironment, aStackTop, aEnvironment.iPostfixOperators, 1);
            
            if (op == null)
            {
                LispError.throwError(aEnvironment, aStackTop, "The operator < " + op + " > is is not a postfix operator.");
            }
        }
        else
        {
            LispError.throwError(aEnvironment, aStackTop, "The argument < " + xfix + " > is invalid.");
        }
        

        op.setSpaceLeft(Utility.stripEndQuotesIfPresent(leftSpaces));
        
        op.setSpaceRight(Utility.stripEndQuotesIfPresent(rightSpaces));
            
        setTopOfStack(aEnvironment, aStackTop, Utility.getTrueAtom(aEnvironment));
    }
}



/*
%mathpiper_docs,name="PrecedenceGet",categories="Programming Procedures,Miscellaneous,Built In"
*CMD PrecedenceGet --- get operator precedence
*CORE
*CALL
	PrecedenceGet("op")

*PARMS

{"op"} -- string, the name of a function

*DESC

Returns the precedence of the function named "op" which should have been declared as a bodied function or an infix, postfix, or prefix operator. Generates an error message if the string str does not represent a type of function that can have precedence.

For infix operators, right precedence can differ from left precedence. Bodied functions and prefix operators cannot have left precedence, while postfix operators cannot have right precedence; for these operators, there is only one value of precedence.

*E.G.
In> PrecedenceGet("+")
Result: 6;

*SEE LeftPrecedenceGet,RightPrecedenceGet,LeftPrecedenceSet,RightPrecedenceSet,RightAssociativeSet
%/mathpiper_docs
*/