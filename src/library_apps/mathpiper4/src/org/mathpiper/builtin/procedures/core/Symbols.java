/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.Cons;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;

/**
 *
 *  
 */
public class Symbols extends BuiltinProcedure
{

    private Symbols()
    {
    }

    public Symbols(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        if (getArgument(aEnvironment, aStackTop, 1).car() instanceof Cons) {

            Cons subList = (Cons) getArgument(aEnvironment, aStackTop, 1).car();
            
            Cons consTraverser = subList;
            consTraverser = consTraverser.cdr();

            int nr = 1;
            while (consTraverser != null)
            {
                String variable = (String) consTraverser.car();
                if(variable == null) LispError.checkArgument(aEnvironment, aStackTop, nr);
                // printf("Variable %s\n",variable.String());
                aEnvironment.setLocalOrGlobalVariable(aStackTop, variable, AtomCons.getInstance(10, variable), false, false, false);
                consTraverser = consTraverser.cdr();
                nr++;
            }
        }
         setTopOfStack(aEnvironment, aStackTop, Utility.getTrueAtom(aEnvironment));
    }
}



/*
%mathpiper_docs,name="Symbols",categories="Programming Procedures,Variables,Built In"
*CMD Symbols --- declare variables to be symbols that evaluate to themselves
*CORE
*CALL
	Symbols(var, ...)

*PARMS

{var} -- name of variable to be declared a symbol

*DESC

When unassigned variables are evaluated, an exception is thrown.
It is useful for various procedures such as {Solve} and {SolveSteps}
to accept expressions that contain variables which evaluate to themselves
to represent unspecified values instead of using underscore constants
for this purpose.

Each variable that is passed to this procedure is assigned itself
as a value. This is equivalent to evaluating {var := 'var} for
each of these variables.

*E.G.

In> Unassign(a)
Result: True

In> a
Result: Exception
Exception: Error: The variable <a> does not have a value assigned to it. Starting at index 0.

In> Symbols(a)
Result: True

In> a
Result: a

In> Unassign(a)
Result: True

In> a
Result: Exception
Exception: Error: The variable <a> does not have a value assigned to it. Starting at index 0.


In> Symbols(a,b,c,x)
Result: True

In> a*x^2 + b*x + c
Result: (a*x^2 + b*x) + c


*SEE Unassign
%/mathpiper_docs
*/