/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;

/**
 *
 *  
 */
public class Unassign extends BuiltinProcedure
{

    private Unassign()
    {
    }

    public Unassign(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        if (getArgument(aEnvironment, aStackTop, 1).car() instanceof Cons) {

            Cons subList = (Cons) getArgument(aEnvironment, aStackTop, 1).car();
            
            Cons  consTraverser = subList;
            consTraverser = consTraverser.cdr();
            int nr = 1;
            while (consTraverser != null)
            {
                String variableName;
                variableName =  (String) consTraverser.car();
                if( variableName == null) LispError.checkArgument(aEnvironment, aStackTop, nr);
                aEnvironment.unassignVariable(aStackTop, variableName);
                consTraverser = consTraverser.cdr();
                nr++;
            }
        }
        setTopOfStack(aEnvironment, aStackTop, Utility.getTrueAtom(aEnvironment));
    }
}



/*
%mathpiper_docs,name="Unassign",categories="Programming Procedures,Variables,Built In"
*CMD Unassign --- undo an assignment
*CORE
*CALL
	Unassign(var, ...)

*PARMS

{var} -- name of variable to be unbound

*DESC

All assignments made to the variables listed as arguments are
undone. From now on, all these variables remain unevaluated (until a
subsequent assignment is made). Also unsets any metadata that may have
been set in an unbound variable.  If the constant 'All' is passed in
as the variable name, all global variables that don't start with a '?'
character are unassigned.

*E.G.
In> a := 5;
Result> 5;

In> a^2;
Result> 25;

In> Unassign(a);
Result> True;

In> a^2;
Result> a^2;

In> Unassign(All)
Result> True

*SEE Assign, :=
%/mathpiper_docs




%mathpiper_docs,name="MacroUnassign",categories="Programming Procedures,Miscellaneous,Built In"
*CMD MacroUnassign --- define rules in procedures
*CORE
*DESC

This procedure has the same effect as its non-macro counterpart, except
that its arguments are evaluated before the required action is performed.
This is useful in macro-like procedures or in procedures that need to define new
rules based on parameters.

Make sure that the arguments of {Macro}... commands evaluate to expressions that would normally be used in the non-macro version!

*SEE Unassign
%/mathpiper_docs
*/


