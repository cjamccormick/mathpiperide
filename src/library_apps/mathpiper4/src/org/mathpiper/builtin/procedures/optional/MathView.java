/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.optional;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.interpreters.EvaluationResponse;
import org.mathpiper.interpreters.ResponseListener;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.BuiltinObjectCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.ui.gui.worksheets.MathPanel;
import org.mathpiper.ui.gui.worksheets.MathPanelController;
import org.mathpiper.ui.gui.worksheets.SymbolNode;
import org.mathpiper.ui.gui.worksheets.TreePanelCons;
import org.mathpiper.ui.gui.worksheets.symbolboxes.InfixOperator;
import org.mathpiper.ui.gui.worksheets.symbolboxes.SymbolBox;
import org.mathpiper.ui.gui.worksheets.symbolboxes.SymbolName;
import org.scilab.forge.mp.jlatexmath.DefaultTeXFont;
import org.scilab.forge.mp.jlatexmath.cyrillic.CyrillicRegistration;
import org.scilab.forge.mp.jlatexmath.greek.GreekRegistration;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;

/**
 *
 *
 */
public class MathView extends BuiltinProcedure implements ResponseListener {

    private Map defaultOptions;
    private List<ResponseListener> responseListeners = new ArrayList<ResponseListener>();
    
    public void plugIn(Environment aEnvironment)  throws Throwable
    {
	this.functionName = "MathView";
	
        aEnvironment.getBuiltinFunctions().put("MathView", new BuiltinProcedureEvaluator(this, 1, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));

        defaultOptions = new HashMap();
        defaultOptions.put("Scale", 1.0);
        defaultOptions.put("Resizable", false);

    }//end method.

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {
	
        Cons arguments = getArgument(aEnvironment, aStackTop, 1);

        if(! Utility.isSublist(arguments)) LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "ToDo");

        arguments = (Cons) arguments.car(); //Go to sub list.

        arguments = arguments.cdr(); //Strip List tag.
        
        Cons expressionCons = arguments;

        SymbolBox sBoxExpression = null;
        Map treeOptionsMap = new HashMap();
        treeOptionsMap.put("Code", true);
        treeOptionsMap.put("WordWrap", 0);
        SymbolNode mainRootNode;
        int fontSize = 11;
        double viewScaleTree = 1;
        
        
        if (expressionCons != null) {
            mainRootNode = new SymbolNode();
            mainRootNode.setFontSize(viewScaleTree * fontSize);
            mainRootNode.setViewScale(viewScaleTree);

            try {
                String operator;

                if (expressionCons.car() instanceof Cons) {
                    operator = (String) Cons.caar(expressionCons);
                } else {
                    operator = (String) expressionCons.car();
                }

                mainRootNode.setOperator(operator, (Boolean) treeOptionsMap.get("Code"), (int) treeOptionsMap.get("WordWrap"));

                TreePanelCons.handleSublistCons(mainRootNode, expressionCons, null, null, "", treeOptionsMap);

            } catch (Throwable e) {
                e.printStackTrace();
            }
            
            sBoxExpression = nodeToBox(mainRootNode);
        }


        Cons options = arguments.cdr();
        Map userOptions = Utility.optionsListToJavaMap(aEnvironment, aStackTop, options, defaultOptions);
        Double viewScale = ((Double)userOptions.get("Scale")).doubleValue();
        

/*
        MathPanel mathPanel = new MathPanel(sBoxExpression, viewScale);
        MathPanelController mathPanelScaler = new MathPanelController(mathPanel, viewScale);
        JScrollPane mathPanelScrollPane = new JScrollPane(mathPanel,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
*/
        JPanel mathControllerPanel = new JPanel();
        mathControllerPanel.setLayout(new BorderLayout());
        MathPanel mathPanel = new MathPanel(sBoxExpression, viewScale);
        mathPanel.addResponseListener(this);
        MathPanelController mathPanelScaler = new MathPanelController(mathPanel, viewScale);
        JScrollPane scrollPane = new JScrollPane(mathPanel,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.getVerticalScrollBar().setUnitIncrement(16);
        mathControllerPanel.add(scrollPane);

        boolean includeSlider = (Boolean) userOptions.get("Resizable");
        if(includeSlider)
        {
            mathControllerPanel.add(mathPanelScaler, BorderLayout.NORTH);
        }

        //Box box = Box.createVerticalBox();

	//box.add(mathControllerPanel);
        
        
        List responseList = new ArrayList();
        responseList.add(mathControllerPanel);
        responseList.add(mathPanel);
        JavaObject response = new JavaObject(responseList);

        setTopOfStack(aEnvironment, aStackTop, BuiltinObjectCons.getInstance(aEnvironment, aStackTop, response));


    }//end method.
    
    
    
    private SymbolBox nodeToBox(SymbolNode node)
    {
        SymbolBox box = null;
        
        if("+-*/".contains(node.getOperator()))
        {
            SymbolNode[] children = node.getChildren();
            SymbolName operatorBox = new SymbolName(node.getOperator());
            operatorBox.setPosition(node.getPosition());
            box = new InfixOperator(nodeToBox(children[0]), operatorBox, nodeToBox(children[1]));
        }
        else
        {
            box = new SymbolName(node.getOperator());
            box.setPosition(node.getPosition());
        }
        
        return box;
    }
    
    
    public void response(EvaluationResponse response) {
        notifyListeners(response);
    }
    
    public boolean remove()
    {
        return false;
    }
    
    public void addResponseListener(ResponseListener listener) {
        responseListeners.add(listener);
    }

    public void removeResponseListener(ResponseListener listener) {
        responseListeners.remove(listener);
    }

    protected void notifyListeners(EvaluationResponse response) {
        for (ResponseListener listener : responseListeners) {
            listener.response(response);
        }//end for.
    }

}//end class.





/*
%mathpiper_docs,name="MathView",categories="Mathematics Procedures,Visualization"
*CMD MathView --- display rendered Latex code

*CALL
    MathView(expression, option, option, option...)

*PARMS
{expression} -- a mathematical expression to display

{Options:}

{Scale} -- a value that sets the initial size of the tree

{Resizable} -- if set to True, a resizing slider is displayed

*DESC
Display a mathematical expression

 
*E.G.
In> 

%/mathpiper_docs
*/



