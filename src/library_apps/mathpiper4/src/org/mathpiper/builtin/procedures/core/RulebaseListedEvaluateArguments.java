/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.Environment;

/**
 *
 *  
 */
public class RulebaseListedEvaluateArguments extends BuiltinProcedure
{

    private RulebaseListedEvaluateArguments()
    {
    }

    public RulebaseListedEvaluateArguments(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {
        org.mathpiper.lisp.Utility.defineRulebase(aEnvironment, aStackTop, true);
    }

}



/*
%mathpiper_docs,name="RulebaseListedEvaluateArguments",categories="Programming Procedures,Miscellaneous,Built In"
*CMD RulebaseListedEvaluateArguments --- define rules in procedures
*CORE
*DESC

This procedure has the same effect as its non-macro counterpart, except
that its arguments are evaluated before the required action is performed.
This is useful in macro-like procedures or in procedures that need to define new
rules based on parameters.

Make sure that the arguments of {Macro}... commands evaluate to expressions that would normally be used in the non-macro version!

*SEE Assign, Unassign, Local, RulebaseHoldArguments, RuleHoldArguments, `, MacroAssign, MacroUnassign, MacroLocal, MacroRulebaseHoldArguments, RuleEvaluateArguments
%/mathpiper_docs
*/
