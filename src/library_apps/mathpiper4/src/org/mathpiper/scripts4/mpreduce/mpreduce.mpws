%mathpiper,def="Factor;Solve;SolveInequality;Expand;Differentiate;Integrate;Equivalent?"

RulebaseHoldArguments("Log",["x"]);

RulebaseHoldArguments("Arbint",["x"]);


LocalSymbols(mpreduce, HandleFactor)
{


If(ExceptionCatch(mpreduce, "", "Exception") =? "Exception")
{
    mpreduce := JavaCall("org.mathpiper.mpreduce.Interpreter2", "getInstance");
    
    JavaCall(mpreduce, "evaluate", "load_package ineq;");
}


/*
Reduce procedures:
ACOS ACOSH ACOT ACOTH ACSC ACSCH ASEC ASECH ASIN ASINH
ATAN ATANH ATAN2 COS COSH COT COTH CSC CSCH DILOG EI EXP
HYPOT LN LOG LOGB LOG10 SEC SECH SIN SINH SQRT TAN TANH
*/




HandleFactor(factorList) :=
{
    Local(result);
    
    If(factorList[2] =? 1)
    {
        result := factorList[1];
    }
    Else
    {
        result :=  ListToProcedure([ToAtom("^"),factorList[1], factorList[2]]);
    }

    result;

}


Factor(expression) :=
{
    Local(result, expressionString, list, length);
    
    expressionString := MathPiperToReduce(expression);

    result := JavaAccess(mpreduce, "evaluate","Factorize(" + expressionString + ");");
    
    list := ReduceToMathPiper(result);

    length := Length(list);
    
    If(length =? 0)
    {
        result := 1;
    }
    Else If(length =? 1)
    {
        result := HandleFactor(list[1]);  
    }
    Else
    {
      result := HandleFactor(First(list));
      
      ForEach(item,Rest(list))
      {
        result := ListToProcedure([ToAtom("*"),result,HandleFactor(item)]);
      }

    }
    
    
     
    ReduceToMathPiper(result);

}



Solve(expression) :=
{
    Local(result);

    result := JavaAccess(mpreduce, "evaluate","Solve(" + MathPiperToReduce(expression) + ");"); 
    
    result := JavaAccess(JavaNew("java.lang.String", result), "replaceAll", "Unknown:.*\\n", "");

    ReduceToMathPiper(result);
}

Solve(expression, unknown) :=         
{         
     Local(result);
     
     If(List?(expression) |? List?(unknown))
     {
        Check(List?(expression) &? List?(unknown), "The arguments must be either a procedure and a constant or both lists.");
        Check(Length(expression) =? Length(unknown), "If the arguments are both lists, they must be the same length.");
     }
     
     JavaAccess(mpreduce,"evaluate","off factor"); 
     
     If(!? List?(expression))
     {
         result := JavaAccess(mpreduce, "evaluate","Solve(" + MathPiperToReduce(expression) + ", " + MathPiperToReduce(unknown) + ");");         
         result := JavaAccess(JavaNew("java.lang.String", result), "replaceAll", "Unknown:.*\\n", "");

         result := ReduceToMathPiper(result);
     }
     Else // Handle a system of equations.
     {
        result := JavaAccess(mpreduce, "evaluate", "Solve(" + MathPiperToReduce(expression) + ","  + MathPiperToReduce(unknown) + ");"); 
        
        // Echo(ReduceToMathPiper("NotZeroAssumptions: " + JavaAccess(mpreduce, "evaluate", "assumptions;"))); //todo:tk.
        
        // Solutions to system of equation problems are returned
        // in a matrix by Reduce.
        result := First(ReduceToMathPiper(result));     
     }     

     result;
}



SolveInequality(expression) :=
{
    Local(result);

    result := JavaAccess(mpreduce, "evaluate","ineq_solve(" + MathPiperToReduce(expression) + ");"); 
    
    result := JavaAccess(JavaNew("java.lang.String", result), "replaceAll", "Unknown:.*\\n", "");

    ReduceToMathPiper(result);
}



SolveInequality(expression, unknown) :=         
{         
     Local(result);         
             
     result := JavaAccess(mpreduce, "evaluate","ineq_solve(" + MathPiperToReduce(expression) + ", " + MathPiperToReduce(unknown) + ");");         
             
     result := JavaAccess(JavaNew("java.lang.String", result), "replaceAll", "Unknown:.*\\n", "");         
             
     ReduceToMathPiper(result);         
}



Expand(expression) :=
{
    Local(result);
    
    JavaCall(mpreduce, "evaluate","off factor;");

    result := JavaAccess(mpreduce, "evaluate",MathPiperToReduce(expression) + ";");
    
    JavaCall(mpreduce, "evaluate","on factor;");
    
    ReduceToMathPiper(result);
}



Differentiate(expression, variable) :=
{
    Local(result);

    result := JavaAccess(mpreduce, "evaluate","DF(" + MathPiperToReduce(expression) + ", " + MathPiperToReduce(variable) + ");"); 
    
    result := JavaAccess(JavaNew("java.lang.String", result), "replaceAll", "Unknown:.*\\n", "");
    
    result := JavaAccess(JavaNew("java.lang.String", result), "replace", "=", "==");

    ReduceToMathPiper(result);
}


Limit(expression, variable, where) :=
{
    Local(result);

    result := JavaAccess(mpreduce, "evaluate","LIMIT(" + MathPiperToReduce(expression) + ", " + MathPiperToReduce(variable) + ", " + MathPiperToReduce(where)+ ");"); 
    
    result := JavaAccess(JavaNew("java.lang.String", result), "replaceAll", "Unknown:.*\\n", "");
    
    result := JavaAccess(JavaNew("java.lang.String", result), "replace", "=", "==");

    ReduceToMathPiper(result);
}



Integrate(expression, variable) :=
{
    Local(result);

    result := JavaAccess(mpreduce, "evaluate","INT(" + MathPiperToReduce(expression) + ", " + MathPiperToReduce(variable) + ");"); 
    
    result := JavaAccess(JavaNew("java.lang.String", result), "replaceAll", "Unknown:.*\\n", "");
    
    result := JavaAccess(JavaNew("java.lang.String", result), "replace", "=", "==");

    ReduceToMathPiper(result);
}



Integrate(expression, variable, lowerValue, higherValue) :=
{
    Local(indefiniteForm);
    
    indefiniteForm := MetaToObject(Integrate(expression, variable));
    
    Apply(`Lambda([@variable], @indefiniteForm), [higherValue]) - Apply(`Lambda([x], @indefiniteForm), [lowerValue]);
}



Equivalent?(expression1, expression2) :=
{
    Local(result);

    result := JavaAccess(mpreduce, "evaluate","(" + MathPiperToReduce(expression1) + ") - (" + MathPiperToReduce(expression2) + ");"); 
    
    result := JavaAccess(JavaNew("java.lang.String", result), "replaceAll", "Unknown:.*\\n", "");
    
    result := JavaAccess(JavaNew("java.lang.String", result), "replace", "=", "==");
    
    result := ReduceToMathPiper(result);
    
    If(Verbose?())
    {
        Echo(result);
    }

    Decide(result =? 0, True, False);
}



} // End LocalSymbols.
%/mathpiper






%mathpiper_docs,name="Factor",categories="Mathematics Procedures,Polynomials (Operations),Number Theory"
*CMD Factor -- Factorization of almost anything factorable
*CALL
Factor( expr )
*PARMS
{expr} -- An Integer, Rational number, Gaussian Integer, Polynomial, or Rational Function
*DESC
This procedure factors integers and symbolic polynomials.
    
*E.G.

In> Factor(16940)
Result: 2^2*5*7*11^2


In> Factor('(5*_x^7-20*_x^6+25*_x^5-20*_x^4+25*_x^3-20*_x^2+20*_x))
Result: (((5*((_x^2 + _x) + 1))*((_x^2 - _x) + 1))*_x)*(_x - 2)^2

 
*SEE Expand

%/mathpiper_docs





%mathpiper_docs,name="Solve",categories="Mathematics Procedures,Solvers (Symbolic)"
*CMD Solve --- solve an equation
*STD
*CALL
        Solve(equ, var)
        Solve([equ1, equ2, ...], [var1, var2])

*PARMS

{equ} -- equation to solve

{var} -- variable to solve for

*DESC

Solves equations.

*E.G.

In> Solve(_x^2+_x,_x)
Result: [_x == 0,_x ==  -1]

In> Solve([2 * _x + _y == 5, -_x + _y == 2], [_x, _y])
Result: [_x == 1,_y == 3]

*SEE Where, AddTo, ==, SolveInequality
%/mathpiper_docs





%mathpiper_docs,name="SolveInequality",categories="Mathematics Procedures,Solvers (Symbolic)"
*CMD SolveInequality --- solve an inequality
*STD
*CALL
        SolveInequality(ineq, var)

*PARMS

{ineq} -- inequality to solve

{var} -- variable to solve for

*DESC

Solves inequalities.

*E.G.

In> SolveInequality('( (-3*(3*_y-2)^2 + 1) * (-3*_y^2 - 2*_y) <=? 0 ), _y)
Result: [_y == (-2/3 .. 0),_y == (0.474217 .. 0.859117)]

*SEE Where, AddTo, ==, Solve
%/mathpiper_docs





%mathpiper_docs,name="Expand",categories="Mathematics Procedures,Polynomials (Operations)"
*CMD Expand --- transform a polynomial to an expanded form
*STD
*CALL
    Expand(expr)

*PARMS

{expr} -- a polynomial expression

*DESC

This command brings a polynomial in expanded form, in which
polynomials are represented in the form 
$c0 + c1*x + c2*x^2 + ... + c[n]*x^n$. In this form, it is easier to test whether a polynomial is
zero, namely by testing whether all coefficients are zero.



*E.G.
In> Expand((1+_x)^5);
Result: ((((_x^5 + 5*_x^4) + 10*_x^3) + 10*_x^2) + 5*_x) + 1

%/mathpiper_docs





%mathpiper_docs,name="Differentiate",categories="Mathematics Procedures,Calculus Related (Symbolic)"
*CMD Differentiate --- take derivative of expression with respect to variable
*STD
*CALL
    Differentiate(expression,variable)

*PARMS

{expression} -- expression to take derivatives of

{variable} -- variable


*DESC

This procedure calculates the derivative of the expression {expr} with
respect to the variable {var}

*E.G.

In> In> Differentiate(_x^2,_x)
Result: 2*_x

*SEE Integrate
%/mathpiper_docs





%mathpiper_docs,name="Limit",categories="Mathematics Procedures,Calculus Related (Symbolic)"
*CMD Limit --- (try) to compute a limit
*STD
*CALL
    Limit(expression,variable,where)

*PARMS

{expression} -- expression to take a the limit, for  variable -> where

{variable} -- variable

{where} -- value  


*DESC

This procedure calculates the limit of the expression {expr} with
respect to the variable {var} var  to where (number).

*E.G.

In> ForEach( i ,[0,1,3,4,Infinity]) {tmp := "lim(x*Sin(1/x),x," +  ToString(i) + ") = ";Echo(tmp,Limit(Sin(1/_x)*_x,_x,i));} 
Result: True
Side Effects:
lim(x*Sin(1/x),x,0) = 0 
lim(x*Sin(1/x),x,1) = Sin(1) 
lim(x*Sin(1/x),x,3) = 3*Sin(1/3) 
lim(x*Sin(1/x),x,4) = 4*Sin(1/4) 
lim(x*Sin(1/x),x,Infinity) = 1 

*SEE ForEach, 
%/mathpiper_docs





%mathpiper_docs,name="Integrate",categories="Mathematics Procedures,Calculus Related (Symbolic)"
*CMD Integrate --- integration

*STD
*CALL
    Integrate(expression, variable)
    Integrate(expression, variable, lowerValue, higherValue)

*PARMS

{expr} -- expression to integrate

{variable} -- atom, variable to integrate over

{lowerValue} -- lower value of definite integration

{higherValue} -- higher value of definite integration


*DESC

This procedure integrates the expression {expression} with respect to the
variable {variable}.

*E.G.
In> Integrate(_x^2,_x)
Result: _x^3/3

In> Integrate(_x^2,_x, 1, 2)
Result: 7/3

*SEE Differentiate
%/mathpiper_docs





%mathpiper_docs,name="Equivalent?",categories="Mathematics Procedures,Polynomials (Operations)"
*CMD Equivalent? --- determine if one polynomial expression equals another one
*STD
*CALL
    Equivalent?(expr1, expr2)

*PARMS

{expr1} -- a polynomial expression

{expr2} -- a polynomial expression

*DESC
Determine if one polynomial expression equals another one. If the procedure is called
in verbose mode, the difference between the expressions is printed.


*E.G.
In> Equivalent?(2*_a, 2*_a)
Result: True

In> Equivalent?(2*_a, 3*_a)
Result: False

In> Verbose(Equivalent?(2*_a, 3*_a))
Result: False
Side Effects:
 -_a
%/mathpiper_docs
