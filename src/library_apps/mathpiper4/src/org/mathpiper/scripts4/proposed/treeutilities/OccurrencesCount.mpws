%mathpiper,def="OccurrencesCount"

OccurrencesCount(expression, subexpression) :=
{
    If(UnderscoreConstant?(subexpression))
    {
        // The code in the Else would also handle this case, but
        // this code is significantly faster.
        Count(UnderscoreConstantsAll(expression), subexpression);
    }
    Else
    {
        Length(PositionsPattern(expression, subexpression));
    }
}

%/mathpiper





%mathpiper_docs,name="OccurrencesCount",categories="Programming Procedures,Expression Trees"
*CMD OccurrencesCount --- counts the number of occurrences of a subexpression that are in an expression

*CALL
    OccurrencesCount(expression, subexpression)

*PARMS

{expression} -- an expression

{subexpression} -- a subexpression

*DESC

Counts the number of occurrences of a subexpression that are in an expression.

*E.G.
In> OccurrencesCount('(a + _b*c / _b - c), _b)
Result: 2

*SEE Contains?
%/mathpiper_docs





%mathpiper,name="OccurrencesCount",subtype="automatic_test"

Verify(OccurrencesCount('(a + _b*c / _b - c), _b), 2);
Verify(OccurrencesCount('(a + _b*c / _b - c*c), 'c), 3);
Verify(OccurrencesCount('(a + (_b*c) / _b - _b*c), '(_b*c)), 2);

%/mathpiper
