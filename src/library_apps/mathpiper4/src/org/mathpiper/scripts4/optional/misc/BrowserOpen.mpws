%mathpiper,def="BrowserOpen"

BrowserOpen(uriString) :=
{
    Local(uri, desktop);
        Check(String?(uriString), "The argument must be a string.");
    uri := JavaNew("java.net.URI", uriString);
    desktop := JavaCall("java.awt.Desktop", "getDesktop");
    JavaCall(desktop, "browse", uri);
        True;
}

%/mathpiper





%mathpiper_docs,name="BrowserOpen",categories="Programming Procedures,Input/Output",access="experimental"
*CMD BrowserOpen --- opens a URI in the system's default browser

*CALL
        BrowserOpen(uri)

*PARMS

{uri} -- the URI of a resource to open

*DESC

This procedure opens a URI in the system's default browser.

*E.G.

In> BrowserOpen("http://www.mathpiper.org") -> True
Result: True

In> BrowserOpen("mailto:example@example.com")
Result: True

%/mathpiper_docs


Note: this procedure can't be automatically tested with
the existing testing system because it opens a browser.