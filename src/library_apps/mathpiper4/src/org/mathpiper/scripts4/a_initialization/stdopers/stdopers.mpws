%mathpiper,def="StandardOperatorsLoad"
RulebaseHoldArguments(StandardOperatorsLoad,[]);
RuleHoldArguments(StandardOperatorsLoad, 0, 1, True)
{
    /* stdopers is loaded immediately after MathPiper is started. It contains
     * the definitions of the infix operators, so the parser can already
     * parse expressions containing these operators, even though the
     * procedure hasn't been defined yet.
     */

    Infix("=?",90);
    Infix("&?",1000);
    RightAssociativeSet("&?");
    Infix("|?", 1010);
    Prefix("!?", 100);
    Infix("<?",90);
    Infix(">?",90);
    Infix("<=?",90);
    Infix(">=?",90);
    Infix("!=?",90);

    Infix(":=",26000);
    RightAssociativeSet(":=");

    Infix("+:=",26000);
    RightAssociativeSet("+:=");

    Infix("-:=",26000);
    RightAssociativeSet("-:=");
    
    RulebaseHoldArguments(":",["left", "right"]);
    HoldArgument(":","left");
    Infix(":", 5);
    OperatorSpacesSet(":", "Infix", "", "");
    RightAssociativeSet(":");
    
    RulebaseHoldArguments("->",["left", "right"]);
    HoldArgument("->","left");
    Infix("->",950);

    Infix("^",20);
    LeftPrecedenceSet("^",19); //Added to make expressions like x^n^2 unambiguous.
    RightAssociativeSet("^");
    OperatorSpacesSet("^", "Infix", "", "");

    Prefix("+",25);
    OperatorSpacesSet("+", "Prefix", " ", "");
    Prefix("-",25);
    OperatorSpacesSet("-", "Prefix", " ", "");

    Infix("/",30);
    Infix("*",40);
    OperatorSpacesSet("/", "Infix", "", "");
    OperatorSpacesSet("*", "Infix", "", "");

    Infix("+",70);
    OperatorSpacesSet("+", "Infix", " ", " ");
    Infix("-",70);
    OperatorSpacesSet("-", "Infix", " ", " ");
    RightPrecedenceSet("-",40);

    Postfix("++",5);
    OperatorSpacesSet("++", "Postfix", "", " ");
    Postfix("--",5);
    OperatorSpacesSet("--", "Postfix", "", "");
    Bodied("ForEach",60000);
    Infix("<<",10);
    Infix(">>",10);
    //Bodied("Differentiate",60000);
    //Bodied("Deriv",60000);
    Infix("X",30);
    Infix(".",30);
    Infix("o",30);
    Infix("***", 50);
    
    Postfix("hex", 30);
    Postfix("bin", 30);
    
    //Bodied("Integrate",60000);

    //Bodied("Limit",60000);

    Bodied("EchoTime", 60000);

    Bodied("Repeat", 60000);
    
    Bodied("FoldGrade", 10000);


    /* Procedural operators */

    //Infix("@",600);
    Infix("/@",600);
    Infix("..",600);

    Bodied("Taylor",60000);
    Bodied("Taylor1",60000);
    Bodied("Taylor2",60000);
    Bodied("Taylor3",60000);
    Bodied("InverseTaylor",60000);

    Infix("<--",22000);

    Infix("##",21900);
    RulebaseHoldArguments("##",["left", "right"]);


    Bodied("TSum",60000);
    Bodied("TExplicitSum",60000);
    Bodied("TD",5);  /* Tell the MathPiper interpreter that TD is to be used as TD(i)f */

    /* Operator to be used for non-evaluating comparisons */
    Infix("==",90);
    Infix("!==",90);

    /* Operators needed for propositional logic theorem prover */
    Infix("->?",10000); /* implication. */
    Infix("==?",10000); /* equivalence. */


    Bodied("If",5);
    Infix("Else",60000);
    RightAssociativeSet("Else");
    /* Bitwise operations we REALLY need. Perhaps we should define them
       also as MathPiper operators?
     */


    /* local pattern replacement operators */
    Infix("/:",25000);
    Infix("/::",25000);
    Infix("<-",22000);

    Prefix("@@", 90);
    Prefix("``", 21000);

    /* Operators used for manual layout */
    Infix("<>", PrecedenceGet("=?"));
    Infix("<=>", PrecedenceGet("=?"));

    /* Operators for Solve: Where and AddTo */
    Infix("Where", 11000);
    Infix("AddTo", 2000);

    Bodied("Procedure",60000);
    Bodied("Macro",60000);

    Bodied("Assert", 60000);

    // Defining very simple procedures, in scripts that can be converted to plugin.
    Bodied("Defun",0);

    Bodied("TemplateProcedure",60000);

    Bodied("Taylor3TaylorCoefs",0);

    Infix(":*:",3);

    Bodied("ProblemSolution", 10000);
    Bodied("Formulas", 9100);
    Bodied("Givens", 9100);
    
    // Units Operators.
    Infix("~", 3); // Set to 50 to have 1 ~ J/s == 1 ~ (J/s)
    OperatorSpacesSet("~", "Infix", "", "");
    Infix("#", 10000);

    Infix("∠", 15);
    OperatorSpacesSet("∠", "Infix", " ", " ");
    
    Infix("||", 85);
    OperatorSpacesSet("||", "Infix", " ", " ");

    Infix("~~", 80);
    OperatorSpacesSet("~~", "Infix", " ", " ");

    /*
        Symbolic operators. 
        The arguments of these operators cannot be held because this will
        prevent the bodies of rules to be evaluated correctly.

        The precedence of these operators is internally set to the precedence 
        of their counterparts.
    */

    RulebaseHoldArguments("+$", ["lhs", "rhs"]);
    RulebaseHoldArguments("-$", ["lhs", "rhs"]);
    RulebaseHoldArguments("/$", ["lhs", "rhs"]);
    RulebaseHoldArguments("*$", ["lhs", "rhs"]);
    RulebaseHoldArguments("^$", ["lhs", "rhs"]);
    RulebaseHoldArguments("+$", ["operand"]);
    RulebaseHoldArguments("-$", ["operand"]);
    RulebaseHoldArguments("==$", ["lhs", "rhs"]);
    RulebaseHoldArguments("=?$", ["lhs", "rhs"]);
    RulebaseHoldArguments("<$", ["lhs", "rhs"]);
    RulebaseHoldArguments(">$", ["lhs", "rhs"]);
    RulebaseHoldArguments("<=?$", ["lhs", "rhs"]);
    RulebaseHoldArguments(">=?$", ["lhs", "rhs"]);
    RulebaseHoldArguments("!=?$", ["lhs", "rhs"]);
    RulebaseHoldArguments("&?$", ["lhs", "rhs"]);
    RulebaseHoldArguments("|?$", ["lhs", "rhs"]);
    RulebaseHoldArguments("!?$", ["rhs"]);
    RulebaseHoldArguments(":=$", ["lhs", "rhs"]);
    RulebaseHoldArguments("++$", ["lhs"]);
    RulebaseHoldArguments("+:=$", ["lhs", "rhs"]);
    RulebaseHoldArguments("--$", ["lhs"]);
    RulebaseHoldArguments("-:=$", ["lhs", "rhs"]);
    RulebaseHoldArguments("~$", ["lhs", "rhs"]);
    OperatorSpacesSet("~$", "Infix", "", ""); //todo:tk:find a way to have the precedence of '$' operators be set to there normal equivalents.
    RulebaseHoldArguments("∠$", ["lhs", "rhs"]);
    RulebaseHoldArguments("||$", ["a", "b"]);
    RulebaseHoldArguments("~~$", ["a", "b"]);

}


%/mathpiper