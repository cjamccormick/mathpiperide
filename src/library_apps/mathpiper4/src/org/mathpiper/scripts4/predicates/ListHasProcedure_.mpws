%mathpiper,def="ListHasProcedure?"

/// ListHasProcedure? --- test for one of the elements of a list to contain a procedure
/// this is mainly useful to test whether a list has nested lists, 
/// i.e. ListHasProcedure?([1,2,3], List)=False and ListHasProcedure?([1,2,[3]], List)=True.
// need to exclude the List atom itself, so don't use ProcedureToList
10 ## ListHasProcedure?([], atom_) <-- False;
15 ## ListHasProcedure?(expr_List?, string_String?) <-- ListHasProcedure?(expr, ToAtom(string));
20 ## ListHasProcedure?(expr_List?, atom_Atom?) <-- HasProcedure?(First(expr), atom) |? ListHasProcedure?(Rest(expr), atom);

%/mathpiper




%mathpiper_docs,name="ListHasProcedure?",categories="Programming Procedures,Predicates"
*CMD ListHasProcedure? --- test for one of the elements of a list to contain a procedure
*STD
*CALL
        ListHasProcedure?( list, func )

*PARMS

{list} - a list of expressions

{func} - a procedure atom to be found

*DESC

Given a list of expressions and and a procedure name, this command returns 
{True} if the list contains the procedure name.

*E.G.
In> lst := [Ln(x),Sinh(a*x),ArcTan(3/x)]
Result: [Ln(x),Sinh(a*x),ArcTan(3/x)]

In> ListHasProcedure?(lst,Sqrt)
Result: False

In> ListHasProcedure?(lst,Ln)
Result: True

In> ListHasProcedure?(lst,Sinh)
Result: True

In> ListHasProcedure?(lst,ArcTan)
Result: True
*SEE HasProcedure?
%/mathpiper_docs


