%mathpiper,def="EvaluateFormula"

EvaluateFormula(formulaProcedure) := EvaluateFormula(formulaProcedure, None);

EvaluateFormula(formulaProcedure, unit) :=
{
    Local(conversions, finalFormula, exp3, formulaHelper, formulaAssociationList, result);
    
    formulaHelper := Substitute('Formula, 'FormulaHelper) formulaProcedure;
    
    formulaAssociationList := Eval(formulaHelper);
    
    If(!? List?(formulaAssociationList))
    {
        ExceptionThrow("", "The first argument must be an association list.");
    }
    
    finalFormula := formulaAssociationList["FinalFormula"];
    
    /*
    conversions := [
        //1~a_ <- a,
        -a_~b_ <- (-a)~b   
      ];
    */
      
    exp3 := ExceptionCatch(NM(Eval(finalFormula[2]) /*/: conversions*/), "", ExceptionThrow("", "Exception during evaluation: " + ExceptionGet()["message"]));
    
    LocalSymbols(exp2)
    { 
        If(PositionsPattern2(exp3, (a_ + b_)::({exp2 := b; Type(a) =? "∠" &? Type(b) !=? "∠";} |? {exp2 := a; Type(b) =? "∠" &? Type(a) !=? "∠";})) !=? [])
        {
            ExceptionThrow("", "The expression (" + exp2 + ") does not have an angle (∠) specified.");
        }
    }
    
    LocalSymbols(exp2)
    { 
        If(PositionsPattern2(exp3, (a_ - b_)::({exp2 := b; Type(a) =? "∠" &? Type(b) !=? "∠";} |? {exp2 := a; Type(b) =? "∠" &? Type(a) !=? "∠";})) !=? [])
        {
            ExceptionThrow("", "The expression (" + exp2 + ") does not have an angle (∠) specified.");
        }
    }
    
    LocalSymbols(exp2)
    { 
        If(PositionsPattern2(exp3, (a_ * b_)::({exp2 := b; Type(a) =? "∠" &? Type(b) !=? "∠";} |? {exp2 := a; Type(b) =? "∠" &? Type(a) !=? "∠";})) !=? [])
        {
            ExceptionThrow("", "The expression (" + exp2 + ") does not have an angle (∠) specified.");
        }
    }
    
    LocalSymbols(exp2)
    {
        If(PositionsPattern2(exp3, (a_ / b_)::({exp2 := b; Type(a) =? "∠" &? Type(b) !=? "∠";} |? {exp2 := a; Type(b) =? "∠" &? Type(a) !=? "∠";})) !=? [])
        {
            ExceptionThrow("", "The expression (" + exp2 + ") does not have an angle (∠) specified.");
        }
    }
 
    If(unit =? None)
    {
         finalFormula[2] := exp3;
    }
    Else
    {
        finalFormula[2] := ExceptionCatch(NM(exp3 # unit), "", ExceptionThrow("", "There was a problem converting to unit (" + unit + ")."));
    }
    
    //exp := Substitute('(==), '(:=)) exp;

    result := Eval(finalFormula[2]);
    
    result /: [a_~b_ <- NM(Expand(a)) ~ b];
    
    If(PositionsPattern2(result, a_ ~ b_) =? [] &? Type(result) !=? "List")
    {
        result := result ~ 1;
    }
    
    result;
}

%/mathpiper







%mathpiper_docs,name="EvaluateFormula",categories="Programming Procedures,Miscellaneous"
*CMD EvaluateFormula --- evaluate an expression inside of a ProblemSolution body

*CALL
        EvaluateFormula(exp)
        EvaluateFormula(exp, unit)

*PARMS

{exp} -- an expression

{unit} -- A unit to convert the result to

*DESC

evaluate an expression inside of a ProblemSolution bod.

*EXAMPLES

*SEE ProblemSolution, Formulas, Formula, Givens

%/mathpiper_docs





%mathpiper,name="EvaluateFormula",subtype="in_prompts"

formulaName := Formula(z == x + y, Label: "Example Formula") -> Formula(z == x + y, Label: "Example Formula")

x := 1~MHz -> 1~MHz

y := 2~MHz -> 2~MHz

EvaluateFormula(formulaName) -> 3~MHz

EvaluateFormula(formulaName, kHz) -> 3000~kHz

%/mathpiper