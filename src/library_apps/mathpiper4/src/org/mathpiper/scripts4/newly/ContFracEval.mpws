%mathpiper,def="ContFracEval"

//////////////////////////////////////////////////
/// ContFracEval: evaluate continued fraction from the list of coefficients
//////////////////////////////////////////////////
/// Each coefficient is either a single expression or a list of 2 expressions, giving the term and the numerator of the current level in the fraction.
/// ContFracEval([[a0, b0], [a1, b1], ...]) = a0+b0/(a1+b1/(...))
/// ContFracEval([a0, a1, ...]) = a0+1/(a1+1/(...))

10 ## ContFracEval([], rest_) <-- rest;
// finish recursion here
10 ## ContFracEval([[n_, m_]], rest_) <-- n+m+rest;
15 ## ContFracEval([n_], rest_) <-- n+rest;
/// Continued fractions with nontrivial numerators
20 ## ContFracEval(list_List?, rest_)::(List?(First(list))) <-- First(First(list)) + Rest(First(list)) / ContFracEval(Rest(list), rest);
/// Continued fractions with unit numerators
30 ## ContFracEval(list_List?, rest_) <-- First(list) + 1 / ContFracEval(Rest(list), rest);

/// evaluate continued fraction: main interface
ContFracEval(list_List?) <-- ContFracEval(list, 0);

%/mathpiper



%mathpiper_docs,name="ContFracEval",categories="Mathematics Procedures,Numbers (Operations)"
*CMD ContFracList --- manipulate continued fractions
*CMD ContFracEval --- manipulate continued fractions
*STD
*CALL
        ContFracList(frac)
        ContFracList(frac, depth)
        ContFracEval(list)
        ContFracEval(list, rest)

*PARMS

{frac} -- a number to be expanded

{depth} -- desired number of terms

{list} -- a list of coefficients

{rest} -- expression to put at the end of the continued fraction

*DESC

The procedure {ContFracList} computes terms of the continued fraction
representation of a rational number {frac}.  It returns a list of terms 
of length {depth}. If {depth} is not specified, it returns all terms.

The procedure {ContFracEval} converts a list of coefficients into a 
continued fraction expression. The optional parameter {rest} specifies 
the symbol to put at the end of the expansion. If it is not given, the 
result is the same as if {rest=0}.

*E.G.

In> A:=ContFracList(33/7 + 0.000001)
Result: [4,1,2,1,1,20393,40,35]

In> ContFracEval(Take(A, 5))
Result: 33/7;

In> ContFracEval(Take(A,3),_remainder)
Result: 4+1/(1/(_remainder+2)+1);
        
*SEE ContFrac, GuessRational
%/mathpiper_docs