%mathpiper,def="TestEquivalent;TestTwoLists"

Macro("TestEquivalent",["left", "right"])
{
    Local(leftEval,rightEval,diff,vars,isEquiv);
    Decide(Verbose?(),{Tell(TestEquivalent,[left,right]);});
    leftEval  := @left;
    rightEval := @right;
    Decide(Verbose?(),
      { NewLine(); Tell("    ",leftEval); Tell("   ",rightEval); });
    Decide( List?(leftEval),
      {
          Decide( List?(rightEval),
            {
                // both are lists
                Decide(Verbose?(),Tell("     both are lists "));
                isEquiv := TestTwoLists(leftEval,rightEval);
            },
            isEquiv := False
          );
      },
      {
          Decide( List?(rightEval), 
            isEquiv := False,
            {
                // neither is a list, so check equality of diff
                Decide(Verbose?(),Tell("     neither is list "));
                Decide(Equation?(leftEval),
                  {
                      Decide(Equation?(rightEval),
                        {
                            Decide(Verbose?(),Tell("      both are equations"));
                            Local(dLHs,dRHS);
                            dLHS := Simplify(EquationLeft(leftEval) - EquationLeft(rightEval));
                            dRHS := Simplify(EquationRight(leftEval) - EquationRight(rightEval));
                            Decide(Verbose?(),Tell("      ",[dLHS,dRHS]));
                            isEquiv := dLHS=?0 &? dRHS=?0;
                        },
                        isEquiv := False
                      );
                  },
                  {
                     Decide(Equation?(rightEval),
                        isEquiv := False,
                        {
                            Decide(Verbose?(),Tell("      neither is equation"));
                            diff := Simplify(leftEval - rightEval);
                            vars := VarList(diff);
                            Decide(Verbose?(),
                              {
                                 Tell("    ",[leftEval,rightEval]);
                                 Tell("    ",vars);
                                 Tell("    ",diff);
                              }
                            );
                            isEquiv   := ( Zero?(diff) |? ZeroVector?(diff) );
                        }
                      );
                   }
                );
            }
          );
      }
    );
    Decide(Verbose?(),Tell("     Equivalence = ",isEquiv));
    Decide( !? isEquiv,
      {
                  WriteString("******************");          NewLine();
                  WriteString("L.H.S. evaluates to: ");
                  Write(leftEval);                            NewLine();
                  WriteString("which differs from   ");
                  Write(rightEval);                           NewLine();
                  WriteString(" by                  "); 
                  Write(diff);                                NewLine();
                  WriteString("******************");          NewLine();
      }
    );
    isEquiv;
}


10 ## TestTwoLists( L1_List?, L2_List? ) <--
{
    Decide(Verbose?(),{Tell("   TestTwoLists");Tell("     ",L1);Tell("     ",L2);});
    Decide(Length(L1)=?1 &? Length(L2)=?1,
      {
          TestEquivalent(L1[1],L2[1]);
      },
      {
          EqualAsSets(L1,L2);
      }
    );
}

%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output


