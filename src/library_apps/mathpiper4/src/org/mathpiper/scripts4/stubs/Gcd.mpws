%mathpiper,def="Gcd"

0 ## Gcd(0,0) <-- 1;
1 ## Gcd(0, m_) <-- Abs(m);
1 ## Gcd(n_,0) <-- Abs(n);
1 ## Gcd(m_,m_) <-- Abs(m);
2 ## Gcd(n_,1) <-- 1;
2 ## Gcd(1,m_) <-- 1;
2 ## Gcd(n_Integer?,m_Integer?) <-- GcdN(n,m);
3 ## Gcd(n_,m_)::(GaussianInteger?(m) &? GaussianInteger?(n) )<-- GaussianGcd(n,m);

4 ## Gcd(-(n_), (m_)) <-- Gcd(n,m);
4 ## Gcd( (n_),-(m_)) <-- Gcd(n,m);
4 ## Gcd(Sqrt(n_Integer?),Sqrt(m_Integer?)) <-- Sqrt(Gcd(n,m));
4 ## Gcd(Sqrt(n_Integer?),m_Integer?) <-- Sqrt(Gcd(n,m^2));
4 ## Gcd(n_Integer?,Sqrt(m_Integer?)) <-- Sqrt(Gcd(n^2,m));

5 ## Gcd(n_Rational?,m_Rational?) <--
{
  Gcd(Numerator(n),Numerator(m))/Lcm(Denominator(n),Denominator(m));
}


10 ## Gcd(list_List?)::(Length(list)>?2) <--
    {
      Local(first);
      first:=Gcd(list[1],list[2]);
      Gcd(Concat([first], Rest(Rest(list))));
    }
14 ## Gcd([0]) <-- 1;
15 ## Gcd([head_]) <-- head;

20 ## Gcd(list_List?)::(Length(list)=?2) <-- Gcd(list[1],list[2]);

/*
30 ## Gcd(n_CanBeUni,m_CanBeUni)::(Length(VarList(n*m))=?1) <--
{
  Local(vars);
  vars:=VarList(n+m);
  NormalForm(Gcd(MakeUni(n,vars),MakeUni(m,vars)));
}
*/

100 ## Gcd(n_Constant?,m_Constant?) <-- 1;
110 ## Gcd(m_, n_) <--
{
  Echo("Not simplified");
}


/*
//Note:tk:moved here from univar.rep.
0 ## Gcd(n_UniVar?,m_UniVar?)::
    (n[1] =? m[1] &? Degree(n) <? Degree(m)) <-- Gcd(m,n);

1 ## Gcd(nn_UniVar?,mm_UniVar?)::
    (nn[1] =? mm[1] &? Degree(nn) >=? Degree(mm)) <--
{
   UniVariate(nn[1],0,
                UniGcd(Concat(ZeroVector(nn[2]),nn[3]),
                       Concat(ZeroVector(mm[2]),mm[3])));
}
*/
%/mathpiper



%mathpiper_docs,name="Gcd",categories="Mathematics Procedures,Numbers (Operations)"
*CMD Gcd --- greatest common divisor
*STD
*CALL
        Gcd(n,m)
        Gcd(list)

*PARMS

{n}, {m} -- integers or Gaussian integers or univariate polynomials

{list} -- a list of all integers or all univariate polynomials

*DESC

This procedure returns the greatest common divisor of "n" and "m".
The gcd is the largest number that divides "n" and "m".  It is
also known as the highest common factor (hcf).  The library code calls
{MathGcd}, which is an internal procedure.  This
procedure implements the "binary Euclidean algorithm" for determining the
greatest common divisor:

*HEAD        Routine for calculating {Gcd(n,m)}
        
*        1. if $n = m$ then return $n$
*        2. if both $n$ and $m$ are even then return $2*Gcd(n/2,m/2)$
*        3. if exactly one of $n$ or $m$ (say $n$) is even then return $Gcd(n/2,m)$
*        4. if both $n$ and $m$ are odd and, say, $n>m$ then return $Gcd((n-m)/2,m)$

This is a rather fast algorithm on computers that can efficiently shift
integers. When factoring Gaussian integers, a slower recursive algorithm is used.

If the second calling form is used, {Gcd} will
return the greatest common divisor of all the integers or polynomials
in "list". It uses the identity
$$Gcd(a,b,c) = Gcd(Gcd(a,b),c)$$.

*E.G.

In> Gcd(55,10)
Result: 5;

In> Gcd([60,24,120])
Result: 12;

In> Gcd( 7300 + 12*I, 2700 + 100*I)
Result: Complex(-4,4);


*SEE Lcm
%/mathpiper_docs





%mathpiper,name="Gcd",subtype="automatic_test"

Verify( Gcd( 324 + 1608*I, -11800 + 7900*I ),Complex(-52,16) );
// I changed from Complex(-4,4) to Complex(4,4) as the GaussianGcd algorithm suddenly returned this instead.
// However, as it turned out it was a bug in FloorN, introduced when
// we moved to the new number classes (so the numbers did not get converted
// to string and back any more). The number got prematurely truncated with
// this test case (regression test added to regress.yts also).
//TODO we can expand this with randomized tests
Verify( Gcd( 7300 + 12*I, 2700 + 100*I), Complex(-4,4) );


// Bug was found: gcd sometimes returned 0! Very bad, since the
// value returned by gcd is usually used to divide out greatest
// common divisors, and dividing by zero is not a good idea.
Verify(Gcd(0,0),1);
Verify(Gcd([0]),1);



VerifyGaussianGcd(x,y):=
{
  Local(gcd);
  gcd:=Gcd(x,y);
//  Echo(x/gcd);
//  Echo(y/gcd);
  Verify(GaussianInteger?(x/gcd) &? GaussianInteger?(y/gcd),True);
}
VerifyGaussianGcd(324 + 1608*I, -11800 + 7900*I);
VerifyGaussianGcd(7300 + 12*I, 2700 + 100*I);
VerifyGaussianGcd(120-I*200,-336+50*I);

/* Bug #3 */
KnownFailure(Gcd(10,3.3) !=? 3.3 &? Gcd(10,3.3) !=? 1);
/* I don't know what the answer should be, but buth 1 and 3.3 seem */
/* certainly wrong. */
Verify(Gcd(-10, 0), 10);
Verify(Gcd(0, -10), 10);

Verify(Gcd([7,21,28]), 7);

%/mathpiper