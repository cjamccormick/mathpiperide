%mathpiper,def="Abs"

RulebaseHoldArguments("Abs", ["a", "b"]);

10 ## Abs(Infinity) <-- Infinity; //Note:tk:moved here from stdfuncts.

10 ## Abs(n_Number?) <-- AbsN(n);
10 ## Abs(n_PositiveNumber?/m_PositiveNumber?) <-- n/m;
10 ## Abs(n_NegativeNumber?/m_PositiveNumber?) <-- (-n)/m;
10 ## Abs(n_PositiveNumber?/m_NegativeNumber?) <-- n/(-m);
10 ## Abs( Sqrt(x_)) <-- Sqrt(x);
10 ## Abs(-Sqrt(x_)) <-- Sqrt(x);
10 ## Abs(Complex(r_, i_)) <-- Sqrt(r^2 + i^2);
10 ## Abs(n_Infinity?) <-- Infinity;
10 ## Abs(Undefined) <-- Undefined;
20 ## Abs(n_List?) <-- MapSingle("Abs",n);

100 ## Abs(a_^n_) <-- Abs(a)^n;

110 ## Abs(a_ ~ b_) <-- Abs(a) ~ b;
110 ## Abs(a_RecognizedUnit?) <-- a;

%/mathpiper



%mathpiper_docs,name="Abs",categories="Mathematics Procedures,Calculus Related (Symbolic)"
*CMD Abs --- absolute value or modulus of complex number
*STD
*CALL
        Abs(x)

*PARMS

{x} -- argument to the procedure

*DESC

This procedure returns the absolute value (also called the modulus) of
"x". If "x" is positive, the absolute value is "x" itself; if
"x" is negative, the absolute value is "-x". For complex "x",
the modulus is the "r" in the polar decomposition
$x = r *Exp(I*phi)$.

This procedure is connected to the {Sign} procedure by
the identity "Abs(x) * Sign(x) = x" for real "x".

This procedure is threaded, meaning that if the argument {x} is a
list, the procedure is applied to all entries in the list.

*E.G.

In> Abs(2);
Result: 2;

In> Abs(-1/2);
Result: 1/2;

In> Abs(3+4*I);
Result: 5;

*SEE Sign, Arg
%/mathpiper_docs




%mathpiper,name="Abs",subtype="automatic_test"

Verify(Abs(Undefined),Undefined);

%/mathpiper