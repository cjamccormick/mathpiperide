%mathpiper,def="Combinations;BinomialCoefficient"

/* Binomials -- now using partial factorial for speed */
// BinomialCoefficient(n,m) = BinomialCoefficient(n, n-m)
10 ## BinomialCoefficient(0,0)                 <-- 1;
10 ## BinomialCoefficient(n_PositiveInteger?,m_NonNegativeInteger?)::(2*m <=? n) <-- ((n-m+1) *** n) / Factorial(m);
15 ## BinomialCoefficient(n_PositiveInteger?,m_NonNegativeInteger?)::(2*m >? n &? m <=? n) <-- BinomialCoefficient(n, n-m);
20 ## BinomialCoefficient(n_Integer?,m_Integer?) <-- 0;

Combinations(n,m) := BinomialCoefficient(n,m);

%/mathpiper



%mathpiper_docs,name="Combinations;BinomialCoefficient",categories="Mathematics Procedures,Combinatorics"
*CMD Combinations/BinomialCoefficient --- combinations/ binomial coefficient
*STD
*CALL
        Combinations(n, r)
        BinomialCoefficient(n, r)

*PARMS

{n} -- integer - total number of objects
{r} -- integer - number of objects chosen

*DESC

These procedures are actually two names for a single function.

In combinatorics, the procedure is thought of as being the number of ways
to choose "r" objects out of a total of "n" objects if order is
not taken into account.

In mathematics the procedure is called the binomial coefficient function
and it is thought of as the coefficient of the x^r term in the polynomial expansion
of the binomial power (1 + x)^n.

The binomial coefficient is defined to be zero
if "r" is negative or greater than "n"; {BinomialCoefficient(0,0)}=1.


*EXAMPLES


*SEE CombinationsList, Permutations, PermutationsList, Eulerian
%/mathpiper_docs




%mathpiper,name="Combinations;BinomialCoefficient",subtype="in_prompts"

Combinations(10, 4) -> 210

BinomialCoefficient(10, 4) -> 210

%/mathpiper



%mathpiper,name="BinomialCoefficient",subtype="automatic_test"

Verify(BinomialCoefficient(0,0),1 );

%/mathpiper

