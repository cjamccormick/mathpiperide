%mathpiper,def="PermutationsList"

Procedure("PermutationsList",["result", "list"])
{
  Decide(Length(list) =? 0,
  {
    result;
  },
  {
    Local(head);
    Local(newresult);
    Local(i);
    head:=list[1];
    newresult:=[];
    ForEach(item,result)
    {
      For(i:=Length(item)+1,i>?0,i--)
      {
        Insert!(newresult,1,Insert(item,i,head));
      }
    }
    newresult:=Reverse!(newresult);
    PermutationsList(newresult,Rest(list));
  });
}


Procedure("PermutationsList",["list"])
{
  PermutationsList([[]],list);
}

%/mathpiper



%mathpiper_docs,name="PermutationsList",categories="Mathematics Procedures,Combinatorics"
*CMD PermutationsList --- return all permutations of a list
*STD
*CALL
        PermutationsList(list)

*PARMS

{list} -- a list of elements

*DESC

PermutationsList returns a list which contains all the permutations of
the elements in the original list.

*E.G.

In> PermutationsList([_a,_b,_c])
Result: [[_a,_b,_c],[_a,_c,_b],[_c,_a,_b],[_b,_a,_c],
        [_b,_c,_a],[_c,_b,_a]];

*SEE Permutations, Combinations, CombinationsList
%/mathpiper_docs

*SEE LeviCivita