%mathpiper,def=""

//todo:tk:not defined in the scripts.

%/mathpiper



%mathpiper_docs,name="ProcedureListSome",categories="Programming Procedures,Lists (Operations)"
*CMD ProcedureList --- list of procedures used in an expression
*CMD ProcedureListArithmetic --- list of procedures used in an expression
*CMD ProcedureListSome --- list of procedures used in an expression
*STD
*CALL
        ProcedureList(expr)
        ProcedureListArithmetic(expr)
        ProcedureListSome(expr, list)

*PARMS

{expr} -- an expression

{list} -- list of procedure atoms to be considered "transparent"

*DESC

The command {ProcedureList(expr)} returns a list of all procedure atoms that appear
in the expression {expr}. The expression is recursively traversed.

The command {ProcedureListSome(expr, list)} does the same, except it only looks at 
arguments of a given {list} of procedures. All other procedures become "opaque" 
(as if they do not contain any other procedures).
For example, {ProcedureListSome(a + Sin(b-c))} will see that the expression has a 
"{-}" operation and return {[+,Sin,-]}, but {ProcedureListSome(a + Sin(b-c), [+])} 
will not look at arguments of {Sin()} and will return {[+,Sin]}.

{ProcedureListArithmetic} is defined through {ProcedureListSome} to look only at arithmetic 
operations {+}, {-}, {*}, {/}.

Note that since the operators "{+}" and "{-}" are prefix as well as infix 
operators, it is currently required to use {ToAtom("+")} to obtain the 
unevaluated atom "{+}".

*E.G. notest

In> ProcedureList(_x+_y*Cos(Ln(_x)/_x))
Result: [+,*,Cos,/,Ln];

In> ProcedureListArithmetic(_x+y*Cos(Ln(_x)/_x))
Result: [+,*,Cos];

In> ProcedureListSome([_a+_b*2,_c/_d],[List])
Result: [List,+,/];

*SEE VarList, HasExpression?, HasProcedure?
%/mathpiper_docs