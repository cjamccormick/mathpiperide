%mathpiper,def="VarListSome"

/// VarListSome is just like ProcedureList(x,y)

10 ## VarListSome([], looklist_) <-- [];

// an atom should be a variable to qualify
10 ## VarListSome(expr_Variable?, looklist_) <-- [expr];
15 ## VarListSome(expr_Atom?, looklist_) <-- [];

// a procedure not in the looking list - return it whole
20 ## VarListSome(expr_Procedure?, looklist_List?)::(!? Contains?(looklist, ToString(Type(expr)))) <-- [expr];

// a procedure in the looking list - traverse its arguments
30 ## VarListSome(expr_Procedure?, looklist_List?) <--
RemoveDuplicates(
                {        // obtain a list of procedures, considering only procedures in looklist
                        Local(item, result);
                        result := [];
                        ForEach(item, expr) result := Concat(result, VarListSome(item, looklist));
                        result;
                }
);

%/mathpiper



%mathpiper_docs,name="VarListSome",categories="Programming Procedures,Lists (Operations)"
*CMD VarListSome --- list of variables appearing in an expression (without duplicates)
*STD
*CALL
        VarListSome(expr, list)

*PARMS

{expr} -- an expression

{list} -- a list of procedure atoms

*DESC

The command {VarList(expr)} returns a list of variables (without duplicates) that appear in the
expression {expr}. The command {VarListSome} looks only at arguments of procedures in the {list}. 
All other procedures are  considered "opaque" (as if they do not contain any variables) and their 
arguments are not checked. For example, {VarListSome(a + Sin(b-c))} will return {[a, b, c]}, 
but [VarListSome(a*Sin(b-c), [*])]  will not look at arguments of [Sin()] and will return 
{[a,Sin(b-c)]}. Here {Sin(b-c)} is considered a "variable" because the procedure {Sin} does not 
belong to {list}.

Note that since the operators "{+}" and "{-}" are prefix as well as infix operators, it is currently 
required to use {ToAtom("+")} to obtain the unevaluated atom "{+}".

*E.G.

In> VarListSome(x+a*y, [ToAtom("+")])
Result: [x,a*y];


*SEE VarList, VarListArith, VarListAll, FreeOf?, Variable?, ProcedureList, HasExpression?, HasProcedure?
%/mathpiper_docs