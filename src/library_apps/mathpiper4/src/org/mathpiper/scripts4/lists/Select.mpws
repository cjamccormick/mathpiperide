%mathpiper,def="Select"

LocalSymbols(predicate,list,result,item)
{
  Procedure("Select",["list", "predicate"])
  {
    Local(result);
    result:=[];
    ForEach(item,list)
    {
      Decide(Apply(predicate,[item]),Append!(result,item));
    }
    result;
  }
  HoldArgument("Select","predicate");
  UnFence("Select",2);
}

%/mathpiper



%mathpiper_docs,name="Select",categories="Programming Procedures,Lists (Operations)"
*CMD Select --- select entries satisfying some predicate
*STD
*CALL
        Select(list, pred)

*PARMS

{pred} -- a predicate

{list} -- a list of elements to select from

*DESC

{Select} returns a sublist of "list" which contains all
the entries for which the predicate "pred" returns
{True} when applied to this entry.

The {Lambda} procedure can be used in place of a predicate procedure
if desired.

*E.G.

In> Select([_a,_b,2,_c,3,_d,4,_e,_f], "Integer?")
Result: [2,3,4];


/%mathpiper

list := [1,-3,2,-6,-4,3];

Select(list, Lambda([i], i >? 0 ));

/%/mathpiper

    /%output,preserve="false"
      Result: [1,2,3]
.   /%/output

*SEE Length, Find, Count, Lambda
%/mathpiper_docs