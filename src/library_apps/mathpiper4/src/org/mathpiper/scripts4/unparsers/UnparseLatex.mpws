%mathpiper,def="UnparseLatex;UnparseLatex2;UnparseLatexFinishList;UnparseLatexMatrixBracketIf;UnparseLatexMaxPrec"

/* def file definitions
UnparseLatex
UnparseLatexMaxPrec
TexForm
*/

/* UnparseLatex: convert MathPiper objects to Latex math mode strings */

/* version 0.4 */

/* Changelog
        0.1        basic functionality
        0.2 fixed bracketing of Exp, added all infix ops and math procedures
        0.3 fixed bracketing of lists, changed bracketing of math procedures, modified Latex representation of user-defined procedures (up to two-letter procedures are in italics), added Latex Greek letters
        0.4 added nth roots, Sum, Limit, Integrate, hyperbolics, set operations, Abs, Max, Min, "==", ":=", Infinity; support indexed expressions A[i] and matrices.
        0.4.1 bugfixes for [] operator, support for multiple indices a[1][2][3]
        0.4.2 fix for variable names ending on digits "a2" represented as $a_2$
        0.4.3 bugfixes: complex I, indeterminate integration; relaxed bracketing of Sin()-like procedures; implemented $TeX$ and $LaLatex$ correctly now (using \textrm{})
        0.4.4 use ordinary instead of partial derivative if expression has only one variable
        0.4.5 fixes for bracketing of Sum(); added <> to render as \sim and <=> to render as \approx; added BinomialCoefficient()
        0.4.6 moved the <> and <=> operators to initialization.rep/stdopers.mpi
        0.4.7 added Product() i.e. Product()
        0.4.8 added Differentiate(x,n), Deriv(x,n),  Implies? , and fixed errors with ArcSinh, ArcCosh, ArcTanh
        0.4.9 fixed omission: (fraction)^n was not put in brackets
        0.4.10 cosmetic change: insert \cdot between numbers in cases like 2*10^n
        0.4.11 added DumpErrors() to TexForm for the benefit of TeXmacs notebooks
        0.4.12 implement the % operation as Mod
        0.4.13 added Bessel{I,J,K,Y}, Ortho{H,P,T,U}, with a general framework for usual two-argument procedures of the form $A_n(x)$; fix for Max, Min
        0.4.14 added mathematical notation for Floor(), Ceil()
        0.4.15 added Block() represented by ( )
        0.4.16 added Zeta()
*/

/* To do:
        0. Find and fix bugs.
        1. The current bracketing approach has limitations: can't omit extra brackets sometimes. " sin a b" is ambiguous, so need to do either "sin a sin b" or "(sin a) b" Hold((a*b)*Sqrt(x)). The current approach is *not* to bracket procedures unless the enveloping operation is more binding than multiplication. This produces "sin a b" for both Sin(a*b) and Sin(a)*b but this is the current mathematical practice.
        2. Need to figure out how to deal with variable names such as "alpha3"
*/

/// TeXmacs unparser
//TexForm(expr_) <-- {DumpErrors();WriteString(UnparseLatex2(expr));NewLine();}


/*
Retract("UnparseLatex", All);
Retract("UnparseLatex2", All);
Retract("UnparseLatexFinishList", All);
Retract("UnparseLatexMatrixBracketIf", All);
Retract("UnparseLatexMaxPrec", All);
*/

LocalSymbols(fullParentheses?, objectLevel?)
{

RulebaseListedHoldArguments("UnparseLatex",["x", "optionsList"]);
RulebaseHoldArguments("UnparseLatex2",["expression", "precedence"]);

/* Boolean predicate */


/* this procedure will put TeX brackets around the string if predicate holds */

Procedure("UnparseLatexBracketIf", ["predicate", "string"])
{
        Check(Boolean?(predicate) &? String?(string), "UnparseLatex internal error: non-boolean and/or non-string argument of UnparseLatexBracketIf");
        
        Decide(predicate |? fullParentheses?, ConcatStrings("( ", string, ") "), string);
}




Procedure("UnparseLatexMatrixBracketIf", ["predicate", "string"])
{
        Check(Boolean?(predicate) &? String?(string), "UnparseLatex internal error: non-boolean and/or non-string argument of UnparseLatexMatrixBracketIf");
        Decide(predicate, ConcatStrings("\\left[ ", string, "\\right]"), string);
}



/* First, we convert UnparseLatex2(x) to UnparseLatex2(x, precedence). The enveloping precedence will determine whether we need to bracket the results. So UnparseLatex2(x, UnparseLatexMaxPrec()) will always print "x", while UnparseLatex2(x,-UnparseLatexMaxPrec()) will always print "(x)".
*/

UnparseLatexMaxPrec() := 60000;         /* This precedence will never be bracketed. It is equal to KMaxPrec */

//Handle no options call.
5 ## UnparseLatex(x_) <-- UnparseLatex(x, []);

/// main front-end
//100 ## UnparseLatex(x_) <-- ConcatStrings("$", UnparseLatex2(RemoveDollarSigns(x), UnparseLatexMaxPrec()), "$");

//Main routine.  It will automatically accept two or more option calls because the
//options come in a list.
10 ## UnparseLatex(x_, optionsList_List?) <--
{

    Local(options, resultString);
        
    options := OptionsToAssociationList(optionsList);

    fullParentheses? := Decide(Boolean?(options["FullParentheses"]), options["FullParentheses"], False);
    
    objectLevel? := Decide(Boolean?(options["ObjectLevel?"]), options["ObjectLevel?"], False);
    
    resultString := UnparseLatex2(RemoveDollarSigns(x), UnparseLatexMaxPrec());

    If(fullParentheses?)
    {
        resultString := StringSubstring(resultString, 2, Length(resultString) - 2);
    }

    ConcatStrings("$", resultString, "$");
}

//Handle a single option call because the option does not come in a list for some reason.
20 ## UnparseLatex(x_, singleOption_) <-- UnparseLatex(x, [singleOption]);



/* Replace numbers and variables -- never bracketed except explicitly */

110 ## UnparseLatex2(x_Number?, p_)::(MetaGet(x,"HighlightColor") !=? None) <-- "\\textcolor{" + MetaGet(x,"HighlightColor") + "}{" + ToString(x) + "}";
111 ## UnparseLatex2(x_Number?, p_) <-- ToString(x);
/* Variables */


200 ## UnparseLatex2(x_Atom?, p_)::(MetaGet(x,"HighlightColor") !=? None) <-- "\\textcolor{" + MetaGet(x,"HighlightColor") + "}{" + UnparseLatexLatexify(ToString(Decide(objectLevel?, x, MetaToObject(x)))) + "}";
201 ## UnparseLatex2(x_Atom?, p_) <-- UnparseLatexLatexify(ToString(Decide(objectLevel?, x, MetaToObject(x))));


/* Strings must be quoted but not bracketed */
100 ## UnparseLatex2(x_String?, p_) <--
{
    Local(characterList);

    characterList := [];
    ForEach(character, x)
    {
        Decide(character !=? " ", Append!(characterList, character), Append!(characterList, "\\hspace{2 mm}"));
    }
    ConcatStrings("\\mathrm{''", ListToString(characterList), "''}");
}



/* ProcedureToList(...) can generate lists with atoms that would otherwise result in unparsable expressions. */
100 ## UnparseLatex2(x_Atom?, p_)::(Infix?(ToString(x))) <-- ConcatStrings("\\mathrm{", ToString(x), "}");


/* Lists: make sure to have matrices processed before them. Enveloping precedence is irrelevant because lists are always bracketed. List items are never bracketed. Note that UnparseLatexFinishList({a,b}) generates ",a,b" */

100 ## UnparseLatex2(x_List?, p_)::(Length(x) =? 0) <-- UnparseLatexBracketIf(True, "");
110 ## UnparseLatex2(x_List?, p_) <-- UnparseLatexBracketIf(True, ConcatStrings(UnparseLatex2(First(x), UnparseLatexMaxPrec()), UnparseLatexFinishList(Rest(x)) ) );
100 ## UnparseLatexFinishList(x_List?)::(Length(x) =? 0) <-- "";
110 ## UnparseLatexFinishList(x_List?) <-- ConcatStrings(", ", UnparseLatex2(First(x), UnparseLatexMaxPrec()), UnparseLatexFinishList(Rest(x)));

/* Replace operations */


        /* Template for "regular" binary infix operators:
100 ## UnparseLatex2(x_ + y_, p_) <-- UnparseLatexBracketIf(p<?PrecedenceGet("+"), ConcatStrings(UnparseLatex2(x, LeftPrecedenceGet("+")), " + ", UnparseLatex2(y, RightPrecedenceGet("+")) ) );
        */
        
// special cases: things like x*2 and 2*10^x look ugly without a multiplication symbol cases like x*2
115 ## UnparseLatex2(expr_ * n_Number?, p_)::(Assigned?(??expression) &? MetaGet(??expression, "HighlightColor") !=? None) <-- UnparseLatexBracketIf(p <? PrecedenceGet("*"), ConcatStrings(UnparseLatex2(expr, LeftPrecedenceGet("*")), "\\textcolor{",MetaGet(??expression, "HighlightColor"),"}{ * }", UnparseLatex2(n, RightPrecedenceGet("*")) ) );
116 ## UnparseLatex2(expr_ * n_Number?, p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("*"), ConcatStrings(UnparseLatex2(expr, LeftPrecedenceGet("*")), " * ", UnparseLatex2(n, RightPrecedenceGet("*")) ) );



// cases like a*20! and a*10^x
117 ## UnparseLatex2(n_ * expr_, p_):: (Procedure?(expr) &? Contains?(["^"], Type(expr)) &? Number?(ProcedureToList(expr)[2]) &? (Assigned?(??expression) &? MetaGet(??expression,"HighlightColor") !=? None)) <-- UnparseLatexBracketIf(p <? PrecedenceGet("*"), ConcatStrings(UnparseLatex2(n, LeftPrecedenceGet("*")), "\\textcolor{",MetaGet(??expression, "HighlightColor"),"}{ * }", UnparseLatex2(expr, RightPrecedenceGet("*")) ) );
118 ## UnparseLatex2(n_ * expr_, p_):: (Procedure?(expr) &? Contains?(["^"], Type(expr)) &? Number?(ProcedureToList(expr)[2])) <-- UnparseLatexBracketIf(p <? PrecedenceGet("*"), ConcatStrings(UnparseLatex2(n, LeftPrecedenceGet("*")), " * ", UnparseLatex2(expr, RightPrecedenceGet("*")) ) );





        /* generic binary ops here */
119 ## UnparseLatex2(expr_Procedure?, p_)::(ArgumentsCount(expr) =? 2 &? Infix?(Type(expr)) &? !? Type(expr) =? "^" &? MetaGet(expr[0],"HighlightColor") !=? None) <-- UnparseLatexBracketIf(p <? PrecedenceGet(Type(expr)), ConcatStrings(UnparseLatex2(ProcedureToList(expr)[2], LeftPrecedenceGet(Type(expr))), "\\textcolor{",MetaGet(expr[0],"HighlightColor"),"}{", UnparseLatexLatexify(Type(expr)), "}", UnparseLatex2(ProcedureToList(expr)[3], RightPrecedenceGet(Type(expr))) ) );
120 ## UnparseLatex2(expr_Procedure?, p_)::(ArgumentsCount(expr) =? 2 &? Infix?(Type(expr)) ) <-- UnparseLatexBracketIf(p <? PrecedenceGet(Type(expr)), ConcatStrings(UnparseLatex2(ProcedureToList(expr)[2], LeftPrecedenceGet(Type(expr))), UnparseLatexLatexify(Type(expr)), UnparseLatex2(ProcedureToList(expr)[3], RightPrecedenceGet(Type(expr))) ) );

        /* Template for "regular" unary prefix operators:
100 ## UnparseLatex2(+ y_, p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("+"), ConcatStrings(" + ", UnparseLatex2(y, RightPrecedenceGet("+")) ) );
        Note that RightPrecedenceGet needs to be defined for prefix ops or Else we won't be able to tell combined prefix/infix ops like "-" from strictly prefix ops.
        */

        /* generic unary ops here */
        /* prefix */
119 ## UnparseLatex2(expr_Procedure?, p_)::(ArgumentsCount(expr) =? 1 &? Prefix?(Type(expr)) &? MetaGet(expr[0],"HighlightColor") !=? None) <-- UnparseLatexBracketIf(p <? PrecedenceGet(Type(expr)), ConcatStrings("\\textcolor{",MetaGet(expr[0],"HighlightColor"),"}{", UnparseLatexLatexify(Type(expr)), "}", UnparseLatex2(ProcedureToList(expr)[2], RightPrecedenceGet(Type(expr)))) );
120 ## UnparseLatex2(expr_Procedure?, p_)::(ArgumentsCount(expr) =? 1 &? Prefix?(Type(expr))) <-- UnparseLatexBracketIf(p <? PrecedenceGet(Type(expr)), ConcatStrings(UnparseLatexLatexify(Type(expr)), UnparseLatex2(ProcedureToList(expr)[2], RightPrecedenceGet(Type(expr)))) );


        /* postfix */
120 ## UnparseLatex2(expr_Procedure?, p_)::(ArgumentsCount(expr) =? 1 &? Postfix?(Type(expr))) <-- UnparseLatexBracketIf(p <? LeftPrecedenceGet(Type(expr)), ConcatStrings(
        UnparseLatex2(ProcedureToList(expr)[2], LeftPrecedenceGet(Type(expr))),
        UnparseLatexLatexify(Type(expr))
) );

        /* fraction and its operands are never bracketed except when they are under power */
98 ## UnparseLatex2(x_ / y_, p_)::(Assigned?(??expression) &? MetaGet(??expression,"HighlightColor") !=? None) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings("\\textcolor{",MetaGet(??expression,"HighlightColor"),"}{\\frac \\textcolor{Black}{", UnparseLatex2(x, UnparseLatexMaxPrec()), "} \\textcolor{Black}{", UnparseLatex2(y, UnparseLatexMaxPrec()), "}} ") );
100 ## UnparseLatex2(x_ / y_, p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings("\\frac{", UnparseLatex2(x, UnparseLatexMaxPrec()), "}{", UnparseLatex2(y, UnparseLatexMaxPrec()), "} ") );

        /* power's argument is never bracketed but it must be put in braces. Chained powers must be bracketed. Powers of 1/n are displayed as roots. */
100 ## UnparseLatex2(x_ ^ (1/2), p_) <-- ConcatStrings("\\sqrt{", UnparseLatex2(x, UnparseLatexMaxPrec()), "}");
101 ## UnparseLatex2(x_ ^ (1/y_), p_) <-- ConcatStrings("\\sqrt[", UnparseLatex2(y, UnparseLatexMaxPrec()), "]{", UnparseLatex2(x, UnparseLatexMaxPrec()), "}");


120 ## UnparseLatex2(x_ ^ y_, p_) <-- UnparseLatexBracketIf(p <=? PrecedenceGet("^"), ConcatStrings(UnparseLatex2(x, PrecedenceGet("^")), " ^{", UnparseLatex2(y, UnparseLatexMaxPrec()), "}" ) );

100 ## UnparseLatex2(If(pred_) body_, p_) <-- "\\textrm{if }(" + UnparseLatex2(pred,60000) + ") " + UnparseLatex2(body,60000);
100 ## UnparseLatex2(left_ Else right_, p_) <-- UnparseLatex2(left,60000) + "\\textrm{ Else }" + UnparseLatex2(right,60000);


/* procedures */


LocalSymbols(UnparseLatexRegularOps, UnparseLatexRegularPrefixOps, UnparseLatexGreekLetters, UnparseLatexSpecialNames) {

        /* addition, subtraction, multiplication, all comparison and logical operations are "regular" */

  UnparseLatexRegularOps :=
  [
    ["+"," + "],
    ["-"," - "],
    ["*"," * "], // " \\times "
    [":="," := "], //\\equiv "],
    ["=="," = "],
    ["=?"," = "],
    ["!=?","\\neq "],
    ["<=?","\\leq "],
    [">=?","\\geq "],
    ["<?"," < "],
    [">?"," > "],
    ["&?","\\wedge "],
    ["|?", "\\vee "],
    ["<>", "\\sim "],
    ["<=>", "\\approx "],
    ["->?", "\\Rightarrow "],
    ["==?", "\\equiv "],
    ["%", "\\bmod "],
    ["<-", "\\leftarrow "],
    ["<--", "\\longleftarrow "],
    ["~", "\\text{~}"]
  ];

  UnparseLatexRegularPrefixOps := [ ["+"," + "], ["-"," - "], ["!?"," \\neg "] ];



    /* Unknown procedure: precedence 200. Leave as is, never bracket the procedure itself and bracket the argumentPointer(s) automatically since it's a list. Other procedures are precedence 100 */

  UnparseLatexGreekLetters := ["Gamma", "Delta", "Theta", "Lambda", "Xi", "Pi", "Sigma", "Upsilon", "Phi", "Psi", "Omega", "alpha", "beta", "gamma", "delta", "epsilon", "zeta", "eta", "theta", "iota", "kappa", "lambda", "mu", "nu", "xi", "pi", "rho", "sigma", "tau", "upsilon", "phi", "chi", "psi", "omega", "varpi", "varrho", "varsigma", "varphi", "varepsilon"];
  UnparseLatexSpecialNames := [
    ["I", "\\imath "],        // this prevents a real uppercase I, use BesselI instead
    ["Pi", "\\pi "],        // this makes it impossible to have an uppercase Pi... hopefully it's not needed
    ["Infinity", "\\infty "],
    ["TeX", "\\textrm{\\TeX\\/}"],
    ["LaTeX", "\\textrm{\\LaTeX\\/}"],
    ["Maximum", "\\max "],        // this replaces these procedure names
    ["Minimum", "\\min "],
    ["Sequence", " "],
    ["Zeta", "\\zeta "],
  ];


  /* this procedure will take a user-defined variable or procedure name and output either this name unmodified if it's only 2 characters long, or the name in normal text if it's longer, or a Latex Greek letter code */
  Procedure("UnparseLatexLatexify", ["string"])
  {
    Local(subscriptSearchStartIndex, subscriptIndex, stringLength);
    
    Check(String?(string), "UnparseLatex internal error: non-string argument of UnparseLatexLatexify");

    subscriptSearchStartIndex := stringLength := Length(string);
    
    If(objectLevel?)
    {
        If(StringEndsWith?(string, "_")) subscriptSearchStartIndex--;
        
        string := StringSubstring(string, 1, subscriptSearchStartIndex);
        
        subscriptIndex := StringLastIndexOf(string, "_", subscriptSearchStartIndex);
    }
    Else
    {
        If(StringEndsWith?(string, "?")) subscriptSearchStartIndex--;
        
        subscriptIndex := StringLastIndexOf(string, "?", subscriptSearchStartIndex);
    }
    
    If(subscriptSearchStartIndex >? 2 &? subscriptIndex !=? -1)
    {
        StringSubstring(string, 1, subscriptIndex - 1) + "_{" + StringSubstring(string, subscriptIndex + 1, stringLength) + "}";
    }
    Else
    {
        /* Check if it's a greek letter or a special name */
        Decide(Contains?(AssociationIndices(UnparseLatexSpecialNames), string), UnparseLatexSpecialNames[string],
        Decide(Contains?(UnparseLatexGreekLetters, string), ConcatStrings("\\", string, " "),
        Decide(Contains?(AssociationIndices(UnparseLatexRegularOps), string), UnparseLatexRegularOps[string],
        Decide(Contains?(AssociationIndices(UnparseLatexRegularPrefixOps), string), UnparseLatexRegularPrefixOps[string],
        //Decide(Length(string) >=? 2 &? Number?(ToAtom(StringMidGet(2, Length(string)-1, string))), ConcatStrings(StringMidGet(1,1,string), "_{", StringMidGet(2, Length(string)-1, string), "}"),
        //Decide(Length(string) >? 2, ConcatStrings("\\text{ ", string, " }"),
        string
        ))));
    }
  }

}

/* */

/* Unknown bodied procedure */

200 ## UnparseLatex2(x_Procedure?, p_):: (Bodied?(Type(x))) <-- {
        Local(func, args, lastarg);
        func := Type(MetaToObject(x));
        args := Rest(ProcedureToList(x));
        lastarg := PopBack(args);
        UnparseLatexBracketIf(p <? PrecedenceGet(func), ConcatStrings(
          UnparseLatexLatexify(func), UnparseLatex2(args, UnparseLatexMaxPrec()),  UnparseLatex2(lastarg, PrecedenceGet(func))
        ));
}

/* Unknown infix procedure : already done above
210 ## UnparseLatex2(x_Procedure?, p_)::(Infix?(Type(x))) <-- ConcatStrings(UnparseLatexLatexify(Type(x)), UnparseLatex2(Rest(ProcedureToList(x)), UnparseLatexMaxPrec()) );
*/
/* Unknown procedure that is not prefix, infix or postfix */
220 ## UnparseLatex2(x_Procedure?, p_) <-- ConcatStrings(UnparseLatexLatexify(Type(MetaToObject(x))), UnparseLatex2(Rest(ProcedureToList(x)), UnparseLatexMaxPrec()) );

        /* Never bracket Sqrt or its arguments */
100 ## UnparseLatex2(Sqrt(x_), p_) <-- ConcatStrings("\\sqrt{", UnparseLatex2(x, UnparseLatexMaxPrec()), "}");

        /* Always bracket Exp's arguments */
100 ## UnparseLatex2(Exp(x_), p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings("\\exp ", UnparseLatexBracketIf(True, UnparseLatex2(x, UnparseLatexMaxPrec())) ) );


LocalSymbols(UnparseLatexMathFunctions, UnparseLatexMathFunctions2) {

  /* Sin, Cos, etc. and their argument is bracketed when it's a sum or product but not bracketed when it's a power. */
  /// supported MathPiper procedures: "mathematical" procedures of one argument which sometimes do not require parentheses (e.g. $\sin x$ )
  UnparseLatexMathFunctions := [ ["Cos","\\cos "], ["Sin","\\sin "], ["Tan","\\tan "], ["Cosh","\\cosh "], ["Sinh","\\sinh "], ["Tanh","\\tanh "], ["Ln","\\ln "], ["ArcCos","\\arccos "], ["ArcSin","\\arcsin "], ["ArcTan","\\arctan "], ["ArcCosh","\\mathrm{arccosh}\\, "], ["ArcSinh","\\mathrm{arcsinh}\\, "], ["ArcTanh","\\mathrm{arctanh}\\, "],
  ["Erf", "\\mathrm{erf}\\, "], ["Erfc", "\\mathrm{erfc}\\, "],
  ];

  /// supported MathPiper procedures: procedures of two arguments of the form $A_n(x)$
  UnparseLatexMathFunctions2 := [
  ["BesselI", "I "], ["BesselJ", "J "],
  ["BesselK", "K "], ["BesselY", "Y "],
  ["OrthoH", "H "], ["OrthoP", "P "],
  ["OrthoT", "T "], ["OrthoU", "U "],
  ];

  // generic two-argument procedures of the form $A(x,y)$ where just the name has to be changed: handle this using the usual naming conversion scheme (UnparseLatexSpecialNames)

  /* Precedence of 120 because we'd like to process other procedures like sqrt or exp first */

  // generic math procedures of one argument
  120 ## UnparseLatex2(expr_Procedure?, p_):: (ArgumentsCount(expr) =? 1 &? Contains?(AssociationIndices(UnparseLatexMathFunctions), Type(expr)) ) <-- UnparseLatexBracketIf(p <? PrecedenceGet("*"), ConcatStrings(UnparseLatexMathFunctions[Type(expr)], UnparseLatex2( ProcedureToList(expr)[2], PrecedenceGet("*")) ) );

  /// math procedures two arguments of the form $A_n(x)$
  120 ## UnparseLatex2(expr_Procedure?, p_):: (ArgumentsCount(expr) =? 2 &? Contains?(AssociationIndices(UnparseLatexMathFunctions2), Type(expr)) ) <-- UnparseLatexBracketIf(p <? PrecedenceGet("*"),
    ConcatStrings(
    UnparseLatexMathFunctions2[Type(expr)],
    "_{",
    UnparseLatex2( ProcedureToList(expr)[2], UnparseLatexMaxPrec()),        // never bracket the subscript
    "}",
    UnparseLatexBracketIf(True, UnparseLatex2(ProcedureToList(expr)[3], UnparseLatexMaxPrec()) ) // always bracket the procedure argument
    )
  );

} // LocalSymbols(UnparseLatexMathFunctions, UnparseLatexMathFunctions2)


/* Complex numbers */
100 ## UnparseLatex2(Complex(0, 1), p_) <-- UnparseLatex2(Hold(I), p);
100 ## UnparseLatex2(Complex(x_, 0), p_) <-- UnparseLatex2(x, p);
110 ## UnparseLatex2(Complex(x_, 1), p_) <-- UnparseLatex2(x + Hold(I), p);
110 ## UnparseLatex2(Complex(0, y_), p_) <-- UnparseLatex2(Hold(I)*y, p);
120 ## UnparseLatex2(Complex(x_, y_), p_) <-- UnparseLatex2(x + Hold(I)*y, p);

/* Abs(), Floor(), Ceil() are displayed as special brackets */

100 ## UnparseLatex2(Abs(x_), p_) <-- ConcatStrings("\\left| ", UnparseLatex2(x, UnparseLatexMaxPrec()), "\\right| ");
100 ## UnparseLatex2(Floor(x_), p_) <-- ConcatStrings("\\left\\lfloor ", UnparseLatex2(x, UnparseLatexMaxPrec()), "\\right\\rfloor ");
100 ## UnparseLatex2(Ceil(x_), p_) <-- ConcatStrings("\\left\\lceil ", UnparseLatex2(x, UnparseLatexMaxPrec()), "\\right\\rceil ");

/* Some procedures which are displayed as infix: Mod, Union, Intersection, Difference, Contains? */

100 ## UnparseLatex2(Modulo(x_, y_), p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings(UnparseLatex2(x, PrecedenceGet("/")), "\\bmod ", UnparseLatex2(y, PrecedenceGet("/")) ) );

100 ## UnparseLatex2(Union(x_, y_), p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings(UnparseLatex2(x, PrecedenceGet("/")), "\\cup ", UnparseLatex2(y, PrecedenceGet("/")) ) );

100 ## UnparseLatex2(Intersection(x_, y_), p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings(UnparseLatex2(x, PrecedenceGet("/")), "\\cap ", UnparseLatex2(y, PrecedenceGet("/")) ) );

100 ## UnparseLatex2(Difference(x_, y_), p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings(UnparseLatex2(x, PrecedenceGet("/")), "\\setminus ", UnparseLatex2(y, PrecedenceGet("/")) ) );

/* "Contains?" is displayed right to left */

100 ## UnparseLatex2(Contains?(x_, y_), p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings(UnparseLatex2(y, PrecedenceGet("/")), "\\in ", UnparseLatex2(x, PrecedenceGet("/")) ) );

/// Binomial coefficients: always bracketed
100 ## UnparseLatex2(BinomialCoefficient(n_, m_), p_) <-- UnparseLatexBracketIf(False, ConcatStrings("{", UnparseLatex2(n, UnparseLatexMaxPrec()), " \\choose ", UnparseLatex2(m, UnparseLatexMaxPrec()), "}" )
);

/* Some procedures with limits: Lim, Sum, Integrate */
/* todo:tk:commenting out for the minimal version of the scripts.
100 ## UnparseLatex2(Sum(x_, x1_, x2_, expr_), p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings("\\sum _{", UnparseLatex2(x, UnparseLatexMaxPrec()), " = ", UnparseLatex2(x1, UnparseLatexMaxPrec()), "} ^{", UnparseLatex2(x2, UnparseLatexMaxPrec()), "} ", UnparseLatex2(expr, PrecedenceGet("*")) ) );

100 ## UnparseLatex2(Sum(expr_), p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings("\\sum ", UnparseLatex2(expr, PrecedenceGet("*")) ) );


100 ## UnparseLatex2(Product(x_, x1_, x2_, expr_), p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings("\\prod _{", UnparseLatex2(x, UnparseLatexMaxPrec()), " = ", UnparseLatex2(x1, UnparseLatexMaxPrec()), "} ^{", UnparseLatex2(x2, UnparseLatexMaxPrec()), "} ", UnparseLatex2(expr, PrecedenceGet("*")) ) );

100 ## UnparseLatex2(Integrate(x_, x1_, x2_) expr_, p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings(
"\\int _{", UnparseLatex2(x1, UnparseLatexMaxPrec()), "} ^{", UnparseLatex2(x2, UnparseLatexMaxPrec()), " } ", UnparseLatex2(expr, PrecedenceGet("*")), " d", UnparseLatex2(x, UnparseLatexMaxPrec())
) );
*/


/* indeterminate integration */
/* todo:tk:commenting out for the minimal version of the scripts.
100 ## UnparseLatex2(Integrate(x_) expr_, p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings(
"\\int ", UnparseLatex2(expr, PrecedenceGet("*")), " d", UnparseLatex2(x, UnparseLatexMaxPrec())
) );

100 ## UnparseLatex2(Limit(x_, x1_) expr_, p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings("\\lim _{", UnparseLatex2(x, UnparseLatexMaxPrec()), "\\rightarrow ", UnparseLatex2(x1, UnparseLatexMaxPrec()), "} ", UnparseLatex2(expr, PrecedenceGet("/")) ) );
*/

// todo:tk:adding support for MPReduce-based integration.
100 ## UnparseLatex2(Integrate(expr_, x_, x1_, x2_), p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings(
"\\int _{", UnparseLatex2(x1, UnparseLatexMaxPrec()), "} ^{", UnparseLatex2(x2, UnparseLatexMaxPrec()), " } ", UnparseLatex2(expr, PrecedenceGet("*")), " d", UnparseLatex2(x, UnparseLatexMaxPrec())
) );

100 ## UnparseLatex2(Integrate(expr_, x_), p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("/"), ConcatStrings(
"\\int ", UnparseLatex2(expr, PrecedenceGet("*")), " d", UnparseLatex2(x, UnparseLatexMaxPrec())
) );



/* Derivatives */

/* Use partial derivative only when the expression has several variables */
/* todo:tk:commenting out for the minimal version of the scripts.
100 ## UnparseLatex2(Deriv(x_) y_, p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("-"), ConcatStrings(
        Decide(Length(VarList(y))>?1, "\\frac{\\partial}{\\partial ", "\\frac{d}{d "
        ), UnparseLatex2(x, PrecedenceGet("^")), "}", UnparseLatex2(y, PrecedenceGet("/")) ) );

100 ## UnparseLatex2(Deriv(x_, n_) y_, p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("-"), ConcatStrings(
        Decide(
                Length(VarList(y))>?1,
                "\\frac{\\partial^" + UnparseLatex2(n, UnparseLatexMaxPrec()) + "}{\\partial ",
                "\\frac{d^" + UnparseLatex2(n, UnparseLatexMaxPrec()) + "}{d "
        ), UnparseLatex2(x, PrecedenceGet("^")), " ^", UnparseLatex2(n, UnparseLatexMaxPrec()), "}", UnparseLatex2(y, PrecedenceGet("/")) ) );

100 ## UnparseLatex2(Differentiate(x_) y_, p_) <-- UnparseLatex2(Deriv(x) y, p);

100 ## UnparseLatex2(Differentiate(x_, n_) y_, p_) <-- UnparseLatex2(Deriv(x, n) y, p);
*/

// todo:tk:adding support for MPReduce-based differentiation.
100 ## UnparseLatex2(Differentiate(y_, x_) , p_) <-- UnparseLatexBracketIf(p <? PrecedenceGet("-"), ConcatStrings(
        Decide(Length(VarList(y))>?1, "\\frac{\\partial}{\\partial ", "\\frac{d}{d "
        ), UnparseLatex2(x, PrecedenceGet("^")), "}", UnparseLatex2(y, PrecedenceGet("/")) ) );



/* Indexed expressions */

/* This seems not to work because x[i] is replaced by Nth(x,i) */
/*
100 ## UnparseLatex2(x_ [ i_ ], p_) <-- ConcatStrings(UnparseLatex2(x, UnparseLatexMaxPrec()), " _{", UnparseLatex2(i, UnparseLatexMaxPrec()), "}");
*/
/* Need to introduce auxiliary procedure, or Else have trouble with arguments of Nth being lists */
RulebaseHoldArguments("UnparseLatexNth",["x", "y"]);
100 ## UnparseLatex2(Nth(Nth(x_, i_List?), j_), p_) <-- UnparseLatex2(UnparseLatexNth(x, Append(i,j)), p);
100 ## UnparseLatex2(UnparseLatexNth(Nth(x_, i_List?), j_), p_) <-- UnparseLatex2(UnparseLatexNth(x, Append(i,j)), p);
110 ## UnparseLatex2(Nth(Nth(x_, i_), j_), p_) <-- UnparseLatex2(UnparseLatexNth(x, List(i,j)), p);
120 ## UnparseLatex2(Nth(x_, i_), p_) <-- ConcatStrings(UnparseLatex2(x, UnparseLatexMaxPrec()), " _{", UnparseLatex2(i, UnparseLatexMaxPrec()), "}");
120 ## UnparseLatex2(UnparseLatexNth(x_, i_), p_) <-- ConcatStrings(UnparseLatex2(x, UnparseLatexMaxPrec()), " _{", UnparseLatex2(i, UnparseLatexMaxPrec()), "}");

/* Matrices are always bracketed. Precedence 80 because lists are at 100. */

80 ## UnparseLatex2(M_Matrix?, p_) <-- UnparseLatexMatrixBracketIf(True, UnparseLatexPrintMatrix(M));

Procedure("UnparseLatexPrintMatrix", ["M"])
{
/*
        Want something like "\begin{array}{cc} a & b \\ c & d \\ e & f \end{array}"
        here, "cc" is alignment and must be given for each column
*/
        Local(row, col, result, ncol);
        result := "\\begin{array}{";
        ForEach(col, M[1]) result:=ConcatStrings(result, "c");
        result := ConcatStrings(result, "}");

        ForEach(row, 1 .. Length(M)) {
                ForEach(col, 1 .. Length(M[row])) {
                        result := ConcatStrings( result, " ", UnparseLatex2(M[row][col], UnparseLatexMaxPrec()), Decide(col =? Length(M[row]), Decide(row =? Length(M), "", " \\\\"), " &"));
                }
        }

        ConcatStrings(result, " \\end{array} ");
}

} //LocalSymbols.

%/mathpiper





%mathpiper_docs,name="UnparseLatex",categories="Programming Procedures,Input/Output"
*CMD UnparseLatex --- export expressions to $LaTeX$
*STD
*CALL
        UnparseLatex(expr)

*PARMS

{expr} -- an expression to be exported

*DESC

{UnparseLatex} returns a string containing a $LaTeX$ representation 
of the MathPiper expression {expr}. Currently the exporter handles 
most expression types but not all.

*E.G.

In> UnparseLatex(Sin(a1)+2*Cos(b1))
Result: "\$\sin a_{1} + 2 \cos b_{1}\$";

*SEE UnparseMath2D, UnparseC
%/mathpiper_docs





%mathpiper,name="UnparseLatex",subtype="automatic_test"

/* it worketh no more...
Testing("Realistic example");
f:=Exp(I*lambda*eta)*w(T*(k+k1+lambda));
g:=Simplify(Substitute(lambda,0) f+(k+k1)*(Differentiate(lambda)f)+k*k1*Differentiate(lambda)Differentiate(lambda)f );
Verify(UnparseLatex(g), ...);
*/

/*
Verify(
UnparseLatex(Hold(Cos(A-B)*Sqrt(C+D)-(a+b)*c^d+2*I+Complex(a+b,a-b)/Complex(0,1)))
,"$\\cos ( A - B)  \\times \\sqrt{C + D} - ( a + b)  \\times c ^{d} + 2 \\times \\imath  + \\frac{a + b + \\imath  \\times ( a - b) }{\\imath } $"
);
*/

Verify(
UnparseLatex(Hold(Exp(A*B)/C/D/(E+F)*G-(-(a+b)-(c-d))-b^(c^d) -(a^b)^c))
,"$\\frac{\\frac{\\frac{\\exp ( A * B) }{C} }{D} }{E + F}  * G - (  - ( a + b)  - ( c - d) )  - b ^{c ^{d}} - ( a ^{b})  ^{c}$"

);

/*
RulebaseHoldArguments("f",["x", "y", "z"]);
RulebaseHoldArguments("g",["x", "y"]);
Verify(
UnparseLatex(ObjectToMeta(Hold(Cos(A-B)*Sin(a)*f(b,c,d*(e+1))*Sqrt(C+D)-(g(a+b)^(c+d))^(c+d))))
,"$\\cos ( A - B)  \\times \\sin a \\times f( b, c, d \\times ( e + 1) )  \\times \\sqrt{C + D} - ( g( a + b)  ^{c + d})  ^{c + d}$"
);
Retract("f",All);
Retract("g",All);
*/

/* This test is commented out because it throws an exception when orthopoly.mpws is removed from the build process.
// testing latest features: \\times, %, (a/b)^n, BinomialCoefficient(), BesselI, OrthoH
Verify(
UnparseLatex(3*2^n+Hold(x*10!) + (x/y)^2 + BinomialCoefficient(x,y) + BesselI(n,x) + Maximum(a,b) + OrthoH(n,x))
, "$3\\times 2 ^{n} + x\\times 10! + ( \\frac{x}{y} )  ^{2} + {x \\choose y} + I _{n}( x)  + \\max ( a, b)  + H _{n}( x) $"
);
*/

/* this fails because of a bug that Differentiate(x) f(y) does not go to 0 */ /*
Verify(
UnparseLatex(3*Differentiate(x)f(x,y,z)*Cos(Omega)*Modulo(Sin(a)*4,5/a^b))
,"$3 ( \\frac{\\partial}{\\partial x}f( x, y, z) )  ( \\cos \\Omega )  ( 4 ( \\sin a) ) \\bmod \\frac{5}{a ^{b}} $"
);
*/


Verify(
UnparseLatex(Hold(!? (c <? 0) &? (a+b)*c>=? -d^e &? (c <=? 0 |? b+1 >? 0) |? a !=? 0 &? !? (p =? q)))
,"$ \\neg c < 0\\wedge ( a + b)  * c\\geq  - d ^{e}\\wedge ( c\\leq 0\\vee b + 1 > 0) \\vee a\\neq 0\\wedge  \\neg p = q$"
);


/* todo:tk:commenting out for the minimal version of the scripts.
RulebaseHoldArguments("f",["x"]);
Verify(
UnparseLatex(Hold(Differentiate(x)f(x)))
,"$\\frac{d}{d x}f( x) $");
Retract("f",All);


/*
RulebaseHoldArguments("f",["x", "y", "z"]);
Verify(
UnparseLatex((Differentiate(x)f(x,y,z))*Cos(Omega)*Modulo(Sin(a)*4,5/a^b))
,"$( \\frac{\\partial}{\\partial x}f( x, y, z) )  \\times \\cos \\Omega  \\times ( 4 \\times \\sin a) \\bmod \\frac{5}{a ^{b}} $"
);
Retract("f",All);
*/


/*
RulebaseHoldArguments("g",["x"]);
RulebaseHoldArguments("theta",["x"]);
Verify(
UnparseLatex(Pi+Exp(1)-Theta-Integrate(x,x1,3/g(Pi))2*theta(x)*Exp(1/x))
,"$\\pi  + \\exp ( 1)  - \\Theta  - \\int _{x_{1}} ^{\\frac{3}{g( \\pi ) }  } 2 \\times \\theta ( x)  \\times \\exp ( \\frac{1}{x} )  dx$"
);
Retract("g",All);
Retract("theta",All);
*/

/*
Verify(
UnparseLatex(ObjectToMeta('([a[3]*b[5]-c[1][2],[a,b,c,d]])))
,"$( a _{3} * b _{5} - c _{( 1, 2) }, ( a, b, c, d) ) $"
);


//Note: this is the only code in the test suite that currently creates new rulebases.
RulebaseHoldArguments("aa",["x", "y", "z"]);
Bodied("aa", 200);
RulebaseHoldArguments("bar", ["x", "y"]);
Infix("bar", 100);
Verify(
UnparseLatex(ObjectToMeta('(aa(x,y) z + 1 bar y!)))
,"$aa( x, y) z + 1\\mathrm{ bar }y!$"
);
Retract("aa",All);
Retract("bar",All);
*/


Verify(
UnparseLatex('(x^(1/3)+x^(1/2)))
, "$\\sqrt[3]{x} + \\sqrt{x}$"
);

/*
Verify(
UnparseLatex()
,""
);
*/

/* Bug report from Michael Borcherds. The brackets were missing. */
Verify(UnparseLatex(Hold(2*x*(-2))), "$2 * x * (  - 2) $");


%/mathpiper
