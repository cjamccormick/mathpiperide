%mathpiper,def="BinomialDistribution"

/* Guard against distribution objects with senseless parameters
   Anti-nominalism */

BinomialDistribution(p_, n_)::
        (Decide(RationalOrNumber?(p),p<?0 |? p>?1, False)
         |? (Constant?(n) &? !? PositiveInteger?(n)) )
        <-- Undefined;

%/mathpiper



%mathpiper_docs,name="BinomialDistribution",categories="Mathematics Procedures,Statistics & Probability"
*CMD BinomialDistribution --- binomial distribution
*STD
*CALL
        BinomialDistribution(p,n)

*PARMS
{p} -- number, probability to observe an event in single trial

{n} -- number of trials

*DESC
Suppose we repeat a trial {n} times, the probability to observe an
event in a single trial is {p} and outcomes in all trials are mutually
independent. Then the number of trials when the event occurred
is distributed according to the binomial distribution. The probability
of that is {BinomialDistribution}{(p,n)}.

Describes the number of successes for draws with replacement.

Numerical value of {p} must satisfy $0<p<1$. Numerical value
of {n} must be a positive integer. 


*EXAMPLES

*SEE BernoulliDistribution, ChiSquareDistribution, DiscreteUniformDistribution, ExponentialDistribution, GeometricDistribution, NormalDistribution, PoissonDistribution, tDistribution
%/mathpiper_docs





%mathpiper,name="BinomialDistribution",subtype="in_prompts"

NM(PMF(BinomialDistribution(.1,15),0)) -> 0.2058911321;

NM(CDF(BinomialDistribution(.1,15),2)) -> 0.8159389308;

%/mathpiper
