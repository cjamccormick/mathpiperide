%mathpiper,def="PMF"

/* Evaluates distribution dst at point x
   known distributions are:
   1. Discrete distributions
   -- BernoulliDistribution(p)
   -- BinomialDistribution(p,n)
   -- DiscreteUniformDistribution(a,b)
   -- PoissonDistribution(l)
   
   2. Continuous distributions
   -- ExponentialDistribution(l)
   -- NormalDistrobution(a,s)
   -- ContinuousUniformDistribution(a,b)
   -- tDistribution(m)
   -- GammaDistribution(m)
   -- ChiSquareDistribution(m)

  DiscreteDistribution(domain,probabilities) represent arbitrary
  distribution with finite number of possible values; domain list
  contains possible values such that
  Pr(X=domain[i])=probabilities[i].
  TODO: Should domain contain numbers only?
*/

10 ## PMF(BernoulliDistribution(p_),0) <-- p;
10 ## PMF(BernoulliDistribution(p_),1) <-- 1-p;
10 ## PMF(BernoulliDistribution(p_),x_Number?)::(x !=? 0 &? x !=? 1) <-- 0;
10 ## PMF(BernoulliDistribution(p_), x_) <-- Decide(x =? 0,p, Decide(x =? 1, 1-p, 0));

10 ## PMF(BinomialDistribution(p_, n_), k_) <-- BinomialCoefficient(n,k)*p^k*(1-p)^(n-k);

10 ## PMF(DiscreteUniformDistribution(a_, b_), x_Number?) <-- Decide(x<?a |? x>?b, 0 ,1/(b-a+1));
11 ## PMF(DiscreteUniformDistribution(a_, b_), x_) <-- Hold(Decide(x<?a |? x>?b, 0 ,1/(b-a+1)));

10 ## PMF(PoissonDistribution(l_), n_Number?) <-- Decide(n<?0,0,Exp(-l)*l^n/Factorial(n));
11 ## PMF(PoissonDistribution(l_), n_) <-- Exp(-l)*l^n/Factorial(n);

10 ## PMF(GeometricDistribution(p_), n_) <--Decide(n<?0,0,p*(1-p)^n);



10 ## PMF(DiscreteDistribution(dom_List?, prob_List?), x_)::( Length(dom)=?Length(prob) &? Simplify(Add(prob))=?1) <--
    {
      Local(i);
      i:=Find(dom,x);
      Decide(i =? -1,0,prob[i]);
    }
    


10 ## PMF(HypergeometricDistribution( N_Number?, M_Number?, n_Number?), x_Number?)::(M <=? N &? n <=? N) <-- (BinomialCoefficient(M,x) * BinomialCoefficient(N-M, n-x))/BinomialCoefficient(N,n);

%/mathpiper




%mathpiper_docs,name="PMF",categories="Mathematics Procedures,Statistics & Probability"
*CMD PMF --- probability mass procedure
*STD
*CALL
        PMF(dist,x)

*PARMS
{dist} -- a distribution type

{x} -- a value of random variable

*DESC
{PMF} returns the
probability for a random variable with distribution {dist} to take a
value of {x}.


*SEE CDF, PDF, Expectation
%/mathpiper_docs
