%mathpiper,def="Tan"

RulebaseHoldArguments("Tan",["x"]);

5 ## Tan(x_Number?)::NumericMode?() <-- TanD(x);

10 ## Tan(x_Number? ~ rad)::NumericMode?() <-- Tan(x);
10 ## Tan(x_Number? ~ radian)::NumericMode?() <-- Tan(x);

15 ## Tan(x_Number? ~ deg)::NumericMode?() <-- Tan(x * 180/Pi);
15 ## Tan(x_Number? ~ degree)::NumericMode?() <-- Tan(x * 180/Pi);

%/mathpiper



%mathpiper_docs,name="Tan",categories="Mathematics Procedures,Trigonometry (Symbolic)"

*CMD Tan --- trigonometric function tangent

*CALL
        Tan(x)

*PARMS

{x} -- argument to the procedure

*DESC

This procedure represents the trigonometric function tangent.

*EXAMPLES

*SEE Sin, Cos, Cot, Sec, Csc, ArcSin, ArcCos, ArcTan, ArcCot, ArcSec, ArcCsc

%/mathpiper_docs





%mathpiper,name="Tan",subtype="automatic_test"

Verify(NM(Tan(1)), 1.557407725);

%/mathpiper





%mathpiper,name="Tan",subtype="in_prompts"

Tan(3) -> Tan(3)

NM(Tan(4~rad)) -> 1.157821282

NM(Tan(5~deg)) -> 0.6753151257

%/mathpiper